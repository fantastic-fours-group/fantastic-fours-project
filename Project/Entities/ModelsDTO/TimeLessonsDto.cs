﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Entities.ModelsDTO
{
    public class TimeLessonsDto
    {
        [Required(ErrorMessage = "Id is a required field for entity TimeLessons")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Start is a required field for entity TimeLessons")]
        public TimeSpan StartLessons { get; set; }

        [Required(ErrorMessage = "Finish is a required field for entity TimeLessons")]
        public TimeSpan FinishLessons { get; set; }
    }
}
