﻿using System.Linq;
using Entities.ModelsDB;

namespace DataAccessLayer.Predicates
{
    internal static class SchedulePredicate
    {
        public static Schedules WithId(this IQueryable<Schedules> schedules, int id)
        {
            return schedules.First(s => s.Id == id);
        }

        public static IQueryable<Schedules> WithGroupId(this IQueryable<Schedules> schedules, int groupId)
        {
            return schedules.Where(s => s.IdGroup == groupId);
        }

        public static IQueryable<Schedules> WithTimeLessonId(this IQueryable<Schedules> schedules, int timeLessonId)
        {
            return schedules.Where(s => s.IdTimeLesson == timeLessonId);
        }

        public static IQueryable<Schedules> WithDayOfTheWeek(this IQueryable<Schedules> schedules, string dayOfWeek)
        {
            return schedules.Where(s => s.DayOfTheWeek == dayOfWeek);
        }

        public static IQueryable<Schedules> WithTransactionId(this IQueryable<Schedules> schedules, int transactionId)
        {
            return schedules.Where(s => s.IdTransaction == transactionId);
        }
    }
}