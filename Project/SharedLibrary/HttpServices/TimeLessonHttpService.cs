﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Entities.ModelsDTO;
using Newtonsoft.Json;
using SharedLibrary.Interfaces;

namespace SharedLibrary.HttpServices
{
    public class TimeLessonHttpService:ITimeLessonHttpService
    {
        #region Rest API Urls
        private readonly string createTimeLessonUrl = "api/TimeLesson/CreateTimeLesson";
        private readonly string editTimeLessonUrl = "api/TimeLesson/EditTimeLesson";
        private readonly string deleteTimeLessonUrl = "api/TimeLesson/DeleteTimeLesson?timeLessonId=";
        private readonly string getTimeLessonByIdUrl = "api/TimeLesson/GetTimeLessonById?timeLessonId=";
        private readonly string getAllTimeLessonUrl = "api/TimeLesson/GetAllTimeLessons";
        private readonly string searchTimeLessonUrl = "api/TimeLessonSearch/SearchTimeLesson?start=";
        #endregion

        private readonly HttpService _httpService;

        public TimeLessonHttpService()
        {
            _httpService = new HttpService();
        }

        public void CreateTimeLesson(TimeLessonsDto timeLesson)
        {
            var _timeLesson = JsonConvert.SerializeObject(timeLesson);
            if (string.IsNullOrEmpty(_timeLesson)) throw new JsonSerializationException("Object timeLesson is empty");
            var buffer = Encoding.UTF8.GetBytes(_timeLesson);
            var content = new ByteArrayContent(buffer);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            _httpService.Post(createTimeLessonUrl, content);
        }

        public void EditTimeLesson(TimeLessonsDto timeLesson)
        {
            var _timeLesson = JsonConvert.SerializeObject(timeLesson);
            if (string.IsNullOrEmpty(_timeLesson)) throw new JsonSerializationException("Object timeLesson is empty");
            var buffer = Encoding.UTF8.GetBytes(_timeLesson);
            var content = new ByteArrayContent(buffer);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            _httpService.Put(editTimeLessonUrl, content);
        }

        public void DeleteTimeLesson(int idTimeLesson)
        {
            _httpService.Delete($"{deleteTimeLessonUrl}{idTimeLesson}");
        }

        public TimeLessonsDto SearchTimeLessons(TimeSpan start, TimeSpan finish)
        {
            return JsonConvert.DeserializeObject<TimeLessonsDto>(_httpService.GetItem($"{searchTimeLessonUrl}{start}&finish={finish}"));
        }

        public TimeLessonsDto GetTimeLessonsById(int idTimeLesson)
        {
            return JsonConvert.DeserializeObject<TimeLessonsDto>(_httpService.GetItem($"{getTimeLessonByIdUrl}{idTimeLesson}"));
        }

        public IList<TimeLessonsDto> GetAllTimeLessons()
        {
            return JsonConvert.DeserializeObject<List<TimeLessonsDto>>(_httpService.GetList(getAllTimeLessonUrl));
        }
    }
}
