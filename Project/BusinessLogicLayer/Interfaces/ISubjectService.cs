﻿using System.Collections.Generic;
using Entities.ModelsDTO;

namespace BusinessLogicLayer.Interfaces
{
    public interface ISubjectService
    {
        void CreateSubject(SubjectsDto subject);
        void EditSubject(SubjectsDto subject);
        void DeleteSubject(int idSubject);
        IList<SubjectsDto> GetSubjectByName(string name);
        SubjectsDto GetSubjectById(int idSubject);
        IList<SubjectsDto> GetAll();
    }
}
