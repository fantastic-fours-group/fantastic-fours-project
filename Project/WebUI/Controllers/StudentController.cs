﻿using System.Web.Mvc;
using Entities.ModelsDTO;
using SharedLibrary.HttpServices;

namespace WebUI.Controllers
{
    public class StudentController : Controller
    {
        private readonly StudentHttpService _studentService;
        private readonly GroupHttpService _groupService;
        private readonly UserHttpService _userService;

        public StudentController()
        {
            _studentService = new StudentHttpService();
            _groupService = new GroupHttpService();
            _userService = new UserHttpService();
        }

        [HttpGet]
        public ActionResult CreateStudent()
        {
            ViewBag.Groups = _groupService.GetAllGroups();
            ViewBag.Users = _userService.GetAllUsers();
            return PartialView();
        }

        [HttpPost]
        public ActionResult CreateStudent(StudentsDto model)
        {
            ViewBag.Success = false;
            if (ModelState.IsValid)
            {
                _studentService.CreateStudent(model);
                ViewBag.Success = true;
            }

            ViewBag.Groups = _groupService.GetAllGroups();
            ViewBag.Users = _userService.GetAllUsers();
            return PartialView(model);
        }

        [HttpGet]
        public ActionResult EditStudent(int id)
        {
            if (id > 0)
            {
                var student = _studentService.GetStudentById(id);
                ViewBag.Groups = _groupService.GetAllGroups();
                ViewBag.Users = _userService.GetAllUsers();
                return PartialView(student);
            }
            return RedirectToAction("Students", "Student");
        }

        [HttpPost]
        public ActionResult EditStudent(StudentsDto model)
        {
            ViewBag.Success = false;
            if (ModelState.IsValid)
            {
                _studentService.EditStudent(model);
                ViewBag.Success = true;
            }
            ViewBag.Groups = _groupService.GetAllGroups();
            ViewBag.Users = _userService.GetAllUsers();
            return PartialView(model);
        }
       
        [HttpGet]
        public ActionResult DeleteStudent(int id)
        {
            if (id > 0)
            {
                var student = _studentService.GetStudentById(id);
                ViewBag.Group = _groupService.GetGroupById(student.IdGroup);
                ViewBag.User = _userService.GetUserById(student.IdUser);
                return PartialView(student);
            }
            return RedirectToAction("Students","Student");
        }

        [HttpPost]
        public ActionResult DeleteStudentById(int id)
        {
            if (id > 0)
            {
                _studentService.DeleteStudent(id);
            }
            return RedirectToAction("Students", "Student");
        }

        [HttpGet]
        public ActionResult Students()
        {
            ViewBag.Students = _studentService.GetAllStudents();
            ViewBag.Groups = _groupService.GetAllGroups();
            return View();
        }

        [HttpPost]
        public ActionResult Students(string search, int id)
        {
            ViewBag.Groups = _groupService.GetAllGroups();
            if (!string.IsNullOrEmpty(search) && id > 0)
            {
                var group = _groupService.GetGroupById(id);
                ViewBag.Students = _studentService.SearchStudent($"{search} {group.Name}");
            }
            else if (string.IsNullOrEmpty(search) && id > 0)
            {
                ViewBag.Students = _studentService.GetByGroupId(id);
            }
            return View();
        }
    }
}