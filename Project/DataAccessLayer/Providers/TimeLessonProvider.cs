﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Mappers;
using DataAccessLayer.Predicates;
using Entities.ModelsDB;
using Entities.ModelsDTO;

namespace DataAccessLayer.Providers
{
    public class TimeLessonProvider : BaseProvider<TimeLessons>, ITimeLessonProvider
    {
        private readonly TimeLessonsMapper _timeLessonsMapper;

        public TimeLessonProvider()
        {
            _timeLessonsMapper = new TimeLessonsMapper();
        }

        protected override DbSet<TimeLessons> GetDbSet()
        {
            return Context.TimeLessons;
        }

        public IList<TimeLessonsDto> GetByStart(TimeSpan start)
        {
            return GetDbSet().WithStart(start).ToList().Select(timeLesson => _timeLessonsMapper.MapFrom(timeLesson)).ToList();
        }

        public IList<TimeLessonsDto> GetByFinish(TimeSpan finish)
        {
            return GetDbSet().WithFinish(finish).ToList().Select(timeLesson => _timeLessonsMapper.MapFrom(timeLesson)).ToList();
        }

        public TimeLessonsDto GetTimeLessonsById(int idTimeLesson)
        {
            return _timeLessonsMapper.MapFrom(GetDbSet().WithId(idTimeLesson));
        }
    }
}
