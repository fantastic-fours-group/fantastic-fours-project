﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Mappers;
using DataAccessLayer.Predicates;
using Entities.ModelsDB;
using Entities.ModelsDTO;

namespace DataAccessLayer.Providers
{
	public class ScheduleProvider : BaseProvider<Schedules>, IScheduleProvider
	{
		private readonly ScheduleMapper _scheduleMapper;

		public ScheduleProvider()
		{
			_scheduleMapper = new ScheduleMapper();
		}

		protected override DbSet<Schedules> GetDbSet()
		{
			return Context.Schedules;
		}

		public IList<SchedulesDto> GetByGroupId(int groupId)
		{
		    return GetDbSet().WithGroupId(groupId).ToList().Select(schedule => _scheduleMapper.MapFrom(schedule)).ToList();
		}

		public IList<SchedulesDto> GetByTimeLessonId(int timeLessonId)
		{
		    return GetDbSet().WithTimeLessonId(timeLessonId).ToList().Select(schedule => _scheduleMapper.MapFrom(schedule)).ToList();
		}

		public IList<SchedulesDto> GetByDayOfTheWeek(string dayOfTheWeek)
		{
		    return GetDbSet().WithDayOfTheWeek(dayOfTheWeek).ToList().Select(schedule => _scheduleMapper.MapFrom(schedule)).ToList();
		}

		public IList<SchedulesDto> GetByTransactionId(int transactionId)
		{
		    return GetDbSet().WithTransactionId(transactionId).ToList().Select(schedule => _scheduleMapper.MapFrom(schedule)).ToList();
		}

		public SchedulesDto GetScheduleById(int idSchedule)
		{
			return _scheduleMapper.MapFrom(GetDbSet().WithId(idSchedule));
		}
	}
}
