USE [master]
GO

DECLARE @DatabaseName NVARCHAR(50)
SET @DatabaseName = N'Schedule'

DECLARE @SQL VARCHAR( MAX )

SELECT @SQL = COALESCE(@SQL, '') + 'Kill ' + Convert(VARCHAR, SPId) + ';'
FROM master..SysProcesses
WHERE dbid = DB_ID(@DatabaseName) AND SPId <> @@SPId

EXEC (@SQL)
GO

--use [Movie Database Lite]
--go
DROP DATABASE Schedule
GO

