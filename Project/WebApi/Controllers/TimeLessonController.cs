﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Web.Http;
using BusinessLogicLayer.Services;
using Entities.ModelsDTO;

namespace WebApi.Controllers
{
    public class TimeLessonController : ApiController
    {
        private readonly TimeLessonService _timeLessonService;

        public TimeLessonController()
        {
            _timeLessonService = new TimeLessonService();
        }

        // GET: api/TimeLesson/GetAllTimeLessons
        [HttpGet]
        public IHttpActionResult GetAllTimeLessons()
        {
            try
            {
                var timeLessons = _timeLessonService.GetAllTimeLessons();
                return Ok(timeLessons);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // GET: api/TimeLesson/GetTimeLessonById?timeLessonId=1
        [HttpGet]
        public IHttpActionResult GetTimeLessonById(int timeLessonId)
        {
            try
            {
                var timeLesson = _timeLessonService.GetTimeLessonsById(timeLessonId);
                return Ok(timeLesson);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // POST: api/TimeLesson/CreateTimeLesson
        [HttpPost]
        public IHttpActionResult CreateTimeLesson(TimeLessonsDto timeLesson)
        {
            try
            {
                _timeLessonService.CreateTimeLesson(timeLesson);
                return Content(HttpStatusCode.Created, timeLesson);
            }
            catch (ValidationException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // PUT: api/TimeLesson/EditTimeLesson
        [HttpPut]
        public IHttpActionResult EditTimeLesson(TimeLessonsDto timeLesson)
        {
            try
            {
                _timeLessonService.EditTimeLesson(timeLesson);
                return Content(HttpStatusCode.Created, timeLesson);
            }
            catch (ValidationException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // DELETE: api/TimeLesson/DeleteTimeLesson?timeLessonId=1
        [HttpDelete]
        public IHttpActionResult DeleteTimeLesson(int timeLessonId)
        {
            try
            {
                _timeLessonService.DeleteTimeLesson(timeLessonId);
                return Content(HttpStatusCode.OK, timeLessonId);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
