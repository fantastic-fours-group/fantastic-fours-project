﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Validators;
using DataAccessLayer.Mappers;
using DataAccessLayer.Providers;
using Entities.ModelsDTO;

namespace BusinessLogicLayer.Services
{
    public class ScheduleService : IScheduleService
    {
        private readonly ScheduleMapper _scheduleMapper;
        private readonly ScheduleProvider _scheduleProvider;
        private readonly AttributeValidator _validator;

        public ScheduleService()
        {
            _scheduleMapper = new ScheduleMapper();
            _scheduleProvider = new ScheduleProvider();
            _validator = new AttributeValidator();
        }

        private void ValidateId(int id)
        {
            if (id < 1)
                throw new ArgumentException("Id must be positive number");
            if (id <= 0) throw new ArgumentOutOfRangeException(nameof(id));
        }

        public void CreateSchedule(SchedulesDto schedule)
        {
            _validator.Validate(schedule);
            _scheduleProvider.Create(_scheduleMapper.MapTo(schedule));
        }

        public void EditSchedule(SchedulesDto schedule)
        {
            _validator.Validate(schedule);
            _scheduleProvider.Update(_scheduleMapper.MapTo(schedule));
        }

        public void DeleteSchedule(int idSchedule)
        {
            ValidateId(idSchedule);
            _scheduleProvider.Delete(_scheduleMapper.MapTo(GetScheduleById(idSchedule)));
        }

        public IList<SchedulesDto> GetByGroupId(int groupId)
        {
            ValidateId(groupId);
            return _scheduleProvider.GetByGroupId(groupId);
        }

        public IList<SchedulesDto> GetByTimeLessonId(int timeLessonId)
        {
            ValidateId(timeLessonId);
            return _scheduleProvider.GetByTimeLessonId(timeLessonId);
        }

        public IList<SchedulesDto> GetByDayOfTheWeek(string dayOfTheWeek)
        {
            return _scheduleProvider.GetByDayOfTheWeek(dayOfTheWeek);
        }

        public IList<SchedulesDto> GetByTransactionId(int transactionId)
        {
            ValidateId(transactionId);
            return _scheduleProvider.GetByTransactionId(transactionId);
        }

        public SchedulesDto GetScheduleById(int idSchedule)
        {
            ValidateId(idSchedule);
            return _scheduleProvider.GetScheduleById(idSchedule);
        }

        public IList<SchedulesDto> GetAll()
        {
            return _scheduleProvider.GetAll().ToList().Select(schedule => _scheduleMapper.MapFrom(schedule)).ToList();
        }
    }
}
