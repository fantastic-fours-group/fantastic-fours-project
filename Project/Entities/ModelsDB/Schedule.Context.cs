﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace Entities.ModelsDB
{
    
    public partial class ScheduleEntities : DbContext
    {
        public ScheduleEntities()
            : base("name=ScheduleEntities")
        {
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Audiences> Audiences { get; set; }
        public virtual DbSet<Departments> Departments { get; set; }
        public virtual DbSet<Groups> Groups { get; set; }
        public virtual DbSet<Schedules> Schedules { get; set; }
        public virtual DbSet<Students> Students { get; set; }
        public virtual DbSet<Subjects> Subjects { get; set; }
        public virtual DbSet<Teachers> Teachers { get; set; }
        public virtual DbSet<TimeLessons> TimeLessons { get; set; }
        public virtual DbSet<Transactions> Transactions { get; set; }
        public virtual DbSet<Users> Users { get; set; }
    }
}
