using System.Collections.Generic;
using Entities.ModelsDTO;

namespace DataAccessLayer.Interfaces
{
	public interface ITeacherProvider
	{
		TeachersDto GetByUserId(int userId);
		IList<TeachersDto> GetByDepartmentId(int departmentId);
		IList<TeachersDto> GetByFirstName(string firstName);
		IList<TeachersDto> GetBySecondName(string secondName);
		IList<TeachersDto> GetByPosition(string position);
		TeachersDto GetTeacherById(int idTeacher);
	}
}