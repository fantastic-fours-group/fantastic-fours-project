﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Validators;
using DataAccessLayer.Mappers;
using DataAccessLayer.Providers;
using Entities.ModelsDTO;

namespace BusinessLogicLayer.Services
{
    public class TransactionService:ITransactionService
    {
        private readonly TransactionProvider _transactionProvider;
        private readonly TransactionMapper _transactionMapper;
        private readonly AttributeValidator _validator;

        public TransactionService()
        {
            _transactionMapper = new TransactionMapper();
            _transactionProvider = new TransactionProvider();
            _validator = new AttributeValidator();
        }

        private void ValidateId(int id)
        {
            if (id < 1)
                throw new ArgumentException("Id must be positive number");
            if (id <= 0) throw new ArgumentOutOfRangeException(nameof(id));
        }

        public void CreateTransaction(TransactionsDto transaction)
        {
            _validator.Validate(transaction);
            _transactionProvider.Create(_transactionMapper.MapTo(transaction));
        }

        public void EditTransaction(TransactionsDto transaction)
        {
            _validator.Validate(transaction);
            _transactionProvider.Update(_transactionMapper.MapTo(transaction));
        }

        public void DeleteTransaction(int idTransaction)
        {
            ValidateId(idTransaction);
            _transactionProvider.Delete(_transactionMapper.MapTo(GetTransactionsById(idTransaction)));
        }

        public IList<TransactionsDto> GetByTeacherId(int teacherId)
        {
            ValidateId(teacherId);
            return _transactionProvider.GetByTeacherId(teacherId);
        }

        public IList<TransactionsDto> GetBySubjectId(int subjectId)
        {
            ValidateId(subjectId);
            return _transactionProvider.GetBySubjectId(subjectId);
        }

        public IList<TransactionsDto> GetByAudienceId(int audienceId)
        {
            ValidateId(audienceId);
            return _transactionProvider.GetByAudienceId(audienceId);
        }

        public IList<TransactionsDto> GetByTypeOfLesson(string typeOfLesson)
        {
            return _transactionProvider.GetByTypeOfLesson(typeOfLesson);
        }

        public IList<TransactionsDto> GetByNumberOfWeek(string numberOfWeek)
        {
            return _transactionProvider.GetByNumberOfWeek(numberOfWeek);
        }

        public TransactionsDto GetTransactionsById(int idTransaction)
        {
            ValidateId(idTransaction);
            return _transactionProvider.GetTransactionsById(idTransaction);
        }

        public IList<TransactionsDto> GetAllTransactions()
        {
            return _transactionProvider.GetAll().ToList().Select(transaction => _transactionMapper.MapFrom(transaction)).ToList();
        }
    }
}
