﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Entities.ModelsDTO;
using Newtonsoft.Json;
using SharedLibrary.Interfaces;

namespace SharedLibrary.HttpServices
{
    public class GroupHttpService : IGroupHttpService
    {
        #region Rest API Urls
        private readonly string createGroupUrl = "api/Group/CreateGroup";
        private readonly string editGroupUrl = "api/Group/EditGroup";
        private readonly string deleteGroupUrl = "api/Group/DeleteGroup?groupId=";
        private readonly string getAllGroupsUrl = "api/Group/GetAllGroups";
        private readonly string getGroupByIdUrl = "api/Group/GetGroupById?groupId=";
        private readonly string getGroupByNameUrl = "api/GroupSearch/GetGroupByName?name=";
        private readonly string getGroupByDepartmentIdUrl = "api/GroupSearch/GetGroupByDepartmentId?departmentId=";
        private readonly string getGroupByCourse = "api/GroupSearch/GetGroupByCourse?course=";
        #endregion

        private readonly HttpService _httpService;

        public GroupHttpService()
        {
            _httpService = new HttpService();
        }

        public void CreateGroup(GroupsDto group)
        {
            var _group = JsonConvert.SerializeObject(group);
            if (string.IsNullOrEmpty(_group)) throw new JsonSerializationException("Object group is empty");
            var buffer = Encoding.UTF8.GetBytes(_group);
            var content = new ByteArrayContent(buffer);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            _httpService.Post(createGroupUrl, content);
        }

        public void DeleteGroup(int idGroup)
        {
            _httpService.Delete($"{deleteGroupUrl}{idGroup}");
        }

        public void EditGroup(GroupsDto group)
        {
            var _group = JsonConvert.SerializeObject(group);
            if (string.IsNullOrEmpty(_group)) throw new JsonSerializationException("Object group is empty");
            var buffer = Encoding.UTF8.GetBytes(_group);
            var content = new ByteArrayContent(buffer);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            _httpService.Put(editGroupUrl, content);
        }

        public IList<GroupsDto> GetAllGroups()
        {
            return JsonConvert.DeserializeObject<List<GroupsDto>>(_httpService.GetList(getAllGroupsUrl));
        }

        public IList<GroupsDto> GetGroupByCourse(string course)
        {
            return JsonConvert.DeserializeObject<List<GroupsDto>>(_httpService.GetList($"{getGroupByCourse}{course}"));
        }

        public IList<GroupsDto> GetGroupByDepartmentId(int departmentId)
        {
            return JsonConvert.DeserializeObject<List<GroupsDto>>(_httpService.GetList($"{getGroupByDepartmentIdUrl}{departmentId}"));
        }

        public GroupsDto GetGroupById(int idGroup)
        {
            return JsonConvert.DeserializeObject<GroupsDto>(_httpService.GetItem($"{getGroupByIdUrl}{idGroup}"));
        }

        public List<GroupsDto> GetGroupByName(string name)
        {
            return JsonConvert.DeserializeObject<List<GroupsDto>>(_httpService.GetList($"{getGroupByNameUrl}{name}"));
        }
    }
}
