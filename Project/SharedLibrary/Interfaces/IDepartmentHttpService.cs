﻿using Entities.ModelsDTO;
using System.Collections.Generic;

namespace SharedLibrary.Interfaces
{
    public interface IDepartmentHttpService
    {
        void CreateDepartment(DepartmentsDto department);
        void EditDepartment(DepartmentsDto department);
        void DeleteDepartment(int idDepartment);
        List<DepartmentsDto> GetDepartmentByName(string name);
        DepartmentsDto GetDepartmentById(int idDepartment);
        IList<DepartmentsDto> GetAllDepartments();
    }
}
