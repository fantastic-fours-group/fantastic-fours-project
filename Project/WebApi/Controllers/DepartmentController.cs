﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Web.Http;
using BusinessLogicLayer.Services;
using Entities.ModelsDTO;

namespace WebApi.Controllers
{
    public class DepartmentController : ApiController
    {
        private readonly DepartmentService _departmentService;

        public DepartmentController()
        {
            _departmentService = new DepartmentService();
        }

        // GET: api/Department/GetAllDepartments
        [HttpGet]
        public IHttpActionResult GetAllDepartments()
        {
            try
            {
                var departments = _departmentService.GetAllDepartments();
                return Ok(departments);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // GET: api/Department/GetDepartmentById?departmentId=1
        [HttpGet]
        public IHttpActionResult GetDepartmentById(int departmentId)
        {
            try
            {
                var department = _departmentService.GetDepartmentById(departmentId);
                return Ok(department);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // POST: api/Department/CreateDepartment
        [HttpPost]
        public IHttpActionResult CreateDepartment(DepartmentsDto department)
        {
            try
            {
                _departmentService.CreateDepartment(department);
                return Content(HttpStatusCode.Created, department);
            }
            catch (ValidationException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // PUT: api/Department/EditDepartment
        [HttpPut]
        public IHttpActionResult EditDepartment(DepartmentsDto department)
        {
            try
            {
                _departmentService.EditDepartment(department);
                return Content(HttpStatusCode.Created, department);
            }
            catch (ValidationException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // DELETE: api/Department/DeleteDepartment?departmentId=1
        [HttpDelete]
        public IHttpActionResult DeleteDepartment(int departmentId)
        {
            try
            {
                _departmentService.DeleteDepartment(departmentId);
                return Content(HttpStatusCode.OK, departmentId);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
