﻿using System.Collections.Generic;
using Entities.ModelsDTO;

namespace BusinessLogicLayer.Interfaces
{
    public interface ITeacherService
    {
        void CreateTeacher(TeachersDto teacher);
        void EditTeacher(TeachersDto teacher);
        void DeleteTeacher(int idTeacher);
        TeachersDto GetByUserId(int userId);
        IList<TeachersDto> GetByDepartmentId(int departmentId);
        IList<TeachersDto> SearchTeacher(string firstName, string secondName, string position, string department);
        IList<TeachersDto> GetByPosition(string position);
        TeachersDto GetTeacherById(int idTeacher);
        IList<TeachersDto> GetAll();
    }
}
