﻿using System.ComponentModel.DataAnnotations;
using Resourses.Enums;

namespace Entities.ModelsDTO
{
    public class AudiencesDto
    {
        [Required(ErrorMessage = "Id is a required field for entity Audience")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is a required field for entity Audience")]
        [StringLength(100, MinimumLength = 1, ErrorMessage = "Name size must be between 1 and 100 characters for entity Audience")]
        public string Name { get; set; }

        [Required(ErrorMessage = "TypeAudience is a required field for entity Audience")]
        public ScheduleEnums.AudienceType TypeAudience { get; set; }

        [Required(ErrorMessage = "Descriptions is a required field for entity Audience")]
        [StringLength(100, MinimumLength = 1, ErrorMessage = "Descriptions size must be between 1 and 20 characters for entity Audience")]
        public string Descriptions { get; set; }
    }
}
