﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Web.Http;
using BusinessLogicLayer.Services;
using Entities.ModelsDTO;

namespace WebApi.Controllers
{
    public class TeacherController : ApiController
    {
        private readonly TeacherService _teacherService;

        public TeacherController()
        {
            _teacherService = new TeacherService();
        }

        // GET: api/Teacher/GetAllTeachers
        [HttpGet]
        public IHttpActionResult GetAllTeachers()
        {
            try
            {
                var teachers = _teacherService.GetAll();
                return Ok(teachers);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // GET: api/Teacher/GetTeacherById?teacherId=1
        [HttpGet]
        public IHttpActionResult GetTeacherById(int teacherId)
        {
            try
            {
                var teacher = _teacherService.GetTeacherById(teacherId);
                return Ok(teacher);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // POST: api/Teacher/CreateTeacher
        [HttpPost]
        public IHttpActionResult CreateTeacher(TeachersDto teacher)
        {
            try
            {
                _teacherService.CreateTeacher(teacher);
                return Content(HttpStatusCode.Created, teacher);
            }
            catch (ValidationException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // PUT: api/Teacher/EditTeacher
        [HttpPut]
        public IHttpActionResult Put(TeachersDto teacher)
        {
            try
            {
                _teacherService.EditTeacher(teacher);
                return Content(HttpStatusCode.Created, teacher);
            }
            catch (ValidationException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // DELETE: api/Teacher/DeleteTeacher?teacherId=3
        [HttpDelete]
        public IHttpActionResult DeleteTeacher(int teacherId)
        {
            try
            {
                _teacherService.DeleteTeacher(teacherId);
                return Content(HttpStatusCode.OK, teacherId);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
