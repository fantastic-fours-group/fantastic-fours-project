using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using Entities.ModelsDB;

namespace DataAccessLayer.Providers
{
    public abstract class BaseProvider<T> where T : class
    {
        protected static readonly ScheduleEntities Context = new ScheduleEntities();

        protected BaseProvider()
        {
            Context.Configuration.ProxyCreationEnabled = false;
        }
        // TODO: should this be replaced with inline of Context.Set<T>()?
        protected abstract DbSet<T> GetDbSet();

        public IEnumerable<T> GetAll()
        {
            return GetDbSet();
        }

        public IEnumerable<T> Find(Func<T, bool> predicate)
        {
            return GetDbSet().Where(predicate);
        }

        public void Create(T entity)
        {
            Context.Set<T>().AddOrUpdate(entity);
            Context.SaveChanges();
        }

        public void Update(T entity)
        {
            Context.Set<T>().AddOrUpdate(entity);
            Context.SaveChanges();
        }

        public void Delete(T entity)
        {
            using (var _context = new ScheduleEntities())
            {
                _context.Set<T>().Add(entity);
                _context.Entry(entity).State = EntityState.Deleted;
                _context.SaveChanges();
            }
        }
    }
}