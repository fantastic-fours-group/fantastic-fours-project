﻿using System.Web.Mvc;
using Entities.ModelsDTO;
using SharedLibrary.HttpServices;

namespace WebUI.Controllers
{
    public class UserController : Controller
    {
        private readonly UserHttpService _userService;

        public UserController()
        {
            _userService = new UserHttpService();
        }
        [HttpGet]
        public ActionResult CreateUser()
        {
            return PartialView();
        }
        [HttpPost]
        public ActionResult CreateUser(UsersDto model)
        {
            if (ModelState.IsValid)
            {
                _userService.CreateUser(model);
            }
            return RedirectToAction("Users","User");
        }
        [HttpGet]
        public ActionResult EditUser(int id)
        {
            if (id > 0)
            {
                var user = _userService.GetUserById(id);
                return PartialView(user);
            }
            return RedirectToAction("Users", "User");
        }
        [HttpPost]
        public ActionResult EditUser(UsersDto model)
        {
            if (ModelState.IsValid)
            {
                _userService.EditUser(model);
            }
            return RedirectToAction("Users", "User");
        }
        [HttpGet]
        public ActionResult DeleteUser(int id)
        {
            if (id > 0)
            {
                var user = _userService.GetUserById(id);
                return PartialView(user);
            }
            return RedirectToAction("Users", "User");
        }
        [HttpPost]
        public ActionResult DeleteUserById(int id)
        {
            if (id > 0)
            {
                _userService.DeleteUser(id);
            }
            return RedirectToAction("Users", "User");
        }

        [HttpGet]
        public ActionResult Users(string role)
        {
            ViewBag.Users = _userService.GetAllUsers();
            if (!string.IsNullOrEmpty(role))
            {
                ViewBag.Users = _userService.GetUserByRole(role);
            }
            return View();
        }
    }
}