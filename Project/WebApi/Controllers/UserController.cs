﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Web.Http;
using BusinessLogicLayer.Services;
using Entities.ModelsDTO;

namespace WebApi.Controllers
{
    public class UserController : ApiController
    {
        private readonly UserService _userService;

        public UserController()
        {
            _userService = new UserService();
        }

        // GET: api/User/GetAllUsers
        [HttpGet]
        public IHttpActionResult GetAllUsers()
        {
            try
            {
                var users = _userService.GetAllUsers();
                return Ok(users);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // GET: api/User/GetUserById?userId=1
        [HttpGet]
        public IHttpActionResult GetUserById(int userId)
        {
            try
            {
                var user = _userService.GetUserById(userId);
                return Ok(user);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // POST: api/User/CreateUser
        [HttpPost]
        public IHttpActionResult CreateUser(UsersDto user)
        {
            try
            {
                _userService.CreateUser(user);
                return Content(HttpStatusCode.Created, user);
            }
            catch (ValidationException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // PUT: api/User/EditUser
        [HttpPut]
        public IHttpActionResult EditUser(UsersDto user)
        {
            try
            {
                _userService.EditUser(user);
                return Content(HttpStatusCode.Created, user);
            }
            catch (ValidationException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // DELETE: api/User/DeleteUser?userId=1
        [HttpDelete]
        public IHttpActionResult DeleteUser(int userId)
        {
            try
            {
                _userService.DeleteUser(userId);
                return Content(HttpStatusCode.OK, userId);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
