﻿using System.Linq;
using Entities.ModelsDB;

namespace DataAccessLayer.Predicates
{
    internal static class SubjectPredicates
    {
        public static Subjects WithId(this IQueryable<Subjects> subjects, int id)
        {
            return subjects.First(s => s.Id == id);
        }

        public static IQueryable<Subjects> WithName(this IQueryable<Subjects> subjects, string name)
        {
            return subjects.Where(s => s.Name.Contains(name));
        }
    }
}
