﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Validators;
using DataAccessLayer.Mappers;
using DataAccessLayer.Providers;
using Entities.ModelsDTO;

namespace BusinessLogicLayer.Services
{
    public class UserService:IUserService
    {
        private readonly UserMapper _userMapper;
        private readonly UserProvider _userProvider;
        private readonly HashingService _hashing;
        private readonly AttributeValidator _validator;

        public UserService()
        {
            _userMapper = new UserMapper();
            _userProvider = new UserProvider();
            _hashing = new HashingService();
            _validator = new AttributeValidator();
        }

        private void ValidateId(int id)
        {
            if (id < 1)
                throw new ArgumentException("Id must be positive number");
            if (id <= 0) throw new ArgumentOutOfRangeException(nameof(id));
        }

        private void HashPassword(UsersDto user)
        {
            var hashedPassword = _hashing.GetHash(user.UserPassword);
            user.UserPassword = hashedPassword;
        }

        public void CreateUser(UsersDto user)
        {
            _validator.Validate(user);
            HashPassword(user);
            _userProvider.Create(_userMapper.MapTo(user));
        }

        public void EditUser(UsersDto user)
        {
            _validator.Validate(user);
            HashPassword(user);
            _userProvider.Update(_userMapper.MapTo(user));
        }

        public void DeleteUser(int idUser)
        {
            ValidateId(idUser);
            _userProvider.Delete(_userMapper.MapTo(GetUserById(idUser)));
        }

        public UsersDto GetUserByEmail(string userEmail)
        {
            return _userProvider.GetByUserEmail(userEmail);
        }

        public UsersDto GetUserByCredentials(string userLogin, string userPassword)
        {
            return GetAllUsers()
                .First(user => user.UserLogin.Equals(userLogin) && user.UserPassword.Equals(_hashing.GetHash(userPassword)));
        }

        public void UpdateUserPassword(string userLogin, string oldPassword, string newPassword)
        {
            if (string.IsNullOrEmpty(newPassword))
                throw new ValidationException("Password cannot be empty");

            if (newPassword.Length >= 8 && newPassword.Length <= 50)
            {
                var user = GetUserByCredentials(userLogin, oldPassword);
                user.UserPassword = _hashing.GetHash(newPassword);
                EditUser(user);
            }
            else
            {
                throw new ValidationException("Password size must be between 8 and 50 characters");
            }
        }

        public IList<UsersDto> GetUserByRole(string userRole)
        {
            return _userProvider.GetByUserRole(userRole);
        }

        public UsersDto GetUserById(int idUser)
        {
            ValidateId(idUser);
            return _userProvider.GetUserById(idUser);
        }

        public IList<UsersDto> GetAllUsers()
        {
            return _userProvider.GetAll().Select(user => _userMapper.MapFrom(user)).ToList();
        }
    }
}
