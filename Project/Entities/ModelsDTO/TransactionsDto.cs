﻿using System.ComponentModel.DataAnnotations;
using Resourses.Enums;

namespace Entities.ModelsDTO
{
    public class TransactionsDto
    {
        [Required(ErrorMessage = "Id is a required field for entity Transaction")]
        public int Id { get; set; }

        [Required(ErrorMessage = "IdTeacher is a required field for entity Transaction")]
        public int IdTeacher { get; set; }

        [Required(ErrorMessage = "IdSubject is a required field for entity Transaction")]
        public int IdSubject { get; set; }

        [Required(ErrorMessage = "IdAudience is a required field for entity Transaction")]
        public int IdAudience { get; set; }

        [Required(ErrorMessage = "TypeLessons is a required field for entity Transaction")]
        public ScheduleEnums.TypeOfLessons TypeLessons { get; set; }

        [Required(ErrorMessage = "NumberOfWeek is a required field for entity Transaction")]
        public ScheduleEnums.NumberOfWeek NumberOfWeek { get; set; }
    }
}
