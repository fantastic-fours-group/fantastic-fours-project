﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Web.Http;
using BusinessLogicLayer.Services;
using Entities.ModelsDTO;

namespace WebApi.Controllers
{
    public class SubjectController : ApiController
    {
        private readonly SubjectService _subjectService;

        public SubjectController()
        {
            _subjectService = new SubjectService();
        }

        // GET api/Subject/GetAllSubjects
        [HttpGet]
        public IHttpActionResult GetAllSubjects()
        {
            try
            {
                var subjects = _subjectService.GetAll();
                return Ok(subjects);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // Get: api/Subject/GetSubjectById?subjectId=1
        [HttpGet]
        public IHttpActionResult GetSubjectById(int subjectId)
        {
            try
            {
                var subjects = _subjectService.GetSubjectById(subjectId);
                return Ok(subjects);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // POST api/Subject/CreateSubject
        [HttpPost]
        public IHttpActionResult CreateSubject(SubjectsDto subject)
        {
            try
            {
                _subjectService.CreateSubject(subject);
                return Content(HttpStatusCode.Created, subject);
            }
            catch (ValidationException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // PUT api/Subject/EditSubject
        [HttpPut]
        public IHttpActionResult EditSubject(SubjectsDto subject)
        {
            try
            {
                _subjectService.EditSubject(subject);
                return Content(HttpStatusCode.Created, subject);
            }
            catch (ValidationException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // DELETE api/Subject/DeleteSubject?subjectId=1
        [HttpDelete]
        public IHttpActionResult DeleteSubject(int subjectId)
        {
            try
            {
                _subjectService.DeleteSubject(subjectId);
                return Content(HttpStatusCode.OK, subjectId);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
