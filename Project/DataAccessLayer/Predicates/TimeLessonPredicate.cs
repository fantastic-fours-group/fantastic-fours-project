using System;
using System.Linq;
using Entities.ModelsDB;

namespace DataAccessLayer.Predicates
{
    internal static class TimeLessonPredicate
    {
        public static TimeLessons WithId(this IQueryable<TimeLessons> timeLessons, int id)
        {
            return timeLessons.First(tl => tl.Id == id);
        }

        public static IQueryable<TimeLessons> WithStart(this IQueryable<TimeLessons> timeLessons, TimeSpan start)
        {
            return timeLessons.Where(tl => tl.StartLessons == start);
        }

        public static IQueryable<TimeLessons> WithFinish(this IQueryable<TimeLessons> timeLessons, TimeSpan finish)
        {
            return timeLessons.Where(tl => tl.FinishLessons == finish);
        }
    }
}