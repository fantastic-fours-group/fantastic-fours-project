using System;
using System.Collections.Generic;
using Entities.ModelsDTO;

namespace DataAccessLayer.Interfaces
{
    public interface ITimeLessonProvider
    {
        IList<TimeLessonsDto> GetByStart(TimeSpan start);
        IList<TimeLessonsDto> GetByFinish(TimeSpan finish);
        TimeLessonsDto GetTimeLessonsById(int idTimeLesson);
    }
}