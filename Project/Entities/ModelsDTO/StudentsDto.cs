﻿using System.ComponentModel.DataAnnotations;

namespace Entities.ModelsDTO
{
    public class StudentsDto
    {
        [Required(ErrorMessage = "Id is a required field for entity Student")]
        public int Id { get; set; }

        [Required(ErrorMessage = "IdUser is a required field for entity Student")]
        public int IdUser { get; set; }

        [Required(ErrorMessage = "IdGroup is a required field for entity Student")]
        public int IdGroup { get; set; }

        [Required(ErrorMessage = "FirstName is a required field for entity Student")]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "FirstName size must be between 1 and 20 characters for entity Student")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "SecondName is a required field for entity Student")]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "SecondName size must be between 1 and 30 characters for entity Student")]
        public string SecondName { get; set; }
    }
}
