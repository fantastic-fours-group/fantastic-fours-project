using System.Linq;
using Entities.ModelsDB;

namespace DataAccessLayer.Predicates
{
    internal static class UserPredicate
    {
        public static Users WithId(this IQueryable<Users> users, int id)
        {
            return users.First(u => u.Id == id);
        }

        // TODO: Is User prefix if required for rest of all of this methods(and in database schema as well)
        public static Users WithUserEmail(this IQueryable<Users> users, string userEmail)
        {
            return users.First(u => u.UserEmail == userEmail);
        }

        public static IQueryable<Users> WithUserLogin(this IQueryable<Users> users, string userLogin)
        {
            return users.Where(u => u.UserLogin == userLogin);
        }

        public static IQueryable<Users> WithUserPassword(this IQueryable<Users> users, string userPassword)
        {
            return users.Where(u => u.UserPassword == userPassword);
        }

        public static IQueryable<Users> WithUserRole(this IQueryable<Users> users, string userRole)
        {
            return users.Where(u => u.UserRole == userRole);
        }
    }
}