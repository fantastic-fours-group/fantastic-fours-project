﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Entities.ModelsDTO;
using Newtonsoft.Json;
using SharedLibrary.Interfaces;

namespace SharedLibrary.HttpServices
{
    public class DepartmentHttpService : IDepartmentHttpService
    {
        #region Rest API Urls
        private readonly string createDepartmentUrl = "api/Department/CreateDepartment";
        private readonly string editDepartmentUrl = "api/Department/EditDepartment";
        private readonly string deleteDepartmentUrl = "api/Department/DeleteDepartment?departmentId=";
        private readonly string getAllDepartmentsUrl = "api/Department/GetAllDepartments";
        private readonly string getDepartmentById = "api/Department/GetDepartmentById?departmentId=";
        private readonly string getDepartmentByName = "api/DepartmentSearch/GetDepartmentByName?name=";
        #endregion

        private readonly HttpService _httpService;

        public DepartmentHttpService()
        {
            _httpService = new HttpService();
        }

        public void CreateDepartment(DepartmentsDto department)
        {
            var _department = JsonConvert.SerializeObject(department);
            if (string.IsNullOrEmpty(_department)) throw new JsonSerializationException("Object department is empty");
            var buffer = Encoding.UTF8.GetBytes(_department);
            var content = new ByteArrayContent(buffer);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            _httpService.Post(createDepartmentUrl, content);
        }

        public void DeleteDepartment(int idDepartment)
        {
            _httpService.Delete($"{deleteDepartmentUrl}{idDepartment}");
        }

        public void EditDepartment(DepartmentsDto department)
        {
            var _department = JsonConvert.SerializeObject(department);
            if (string.IsNullOrEmpty(_department)) throw new JsonSerializationException("Object department is empty");
            var buffer = Encoding.UTF8.GetBytes(_department);
            var content = new ByteArrayContent(buffer);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            _httpService.Put(editDepartmentUrl, content);
        }

        public IList<DepartmentsDto> GetAllDepartments()
        {
            return JsonConvert.DeserializeObject<List<DepartmentsDto>>(_httpService.GetList(getAllDepartmentsUrl));
        }

        public DepartmentsDto GetDepartmentById(int idDepartment)
        {
            return JsonConvert.DeserializeObject<DepartmentsDto>(_httpService.GetItem($"{getDepartmentById}{idDepartment}"));
        }

        public List<DepartmentsDto> GetDepartmentByName(string name)
        {
            return JsonConvert.DeserializeObject<List<DepartmentsDto>>(_httpService.GetList($"{getDepartmentByName}{name}"));
        }
    }
}
