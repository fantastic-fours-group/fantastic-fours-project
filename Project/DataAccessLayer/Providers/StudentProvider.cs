﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Mappers;
using DataAccessLayer.Predicates;
using Entities.ModelsDB;
using Entities.ModelsDTO;

namespace DataAccessLayer.Providers
{
	public class StudentProvider : BaseProvider<Students>, IStudentProvider
	{
		private readonly StudentMapper _studentMapper;

		public StudentProvider()
		{
			_studentMapper = new StudentMapper();
		}

		protected override DbSet<Students> GetDbSet()
		{
			return Context.Students;
		}

		public StudentsDto GetByUserId(int userId)
		{
		    return _studentMapper.MapFrom(GetDbSet().WithUserId(userId));
		}

		public IList<StudentsDto> GetByGroupId(int groupId)
		{
		    return GetDbSet().WithGroupId(groupId).ToList().Select(student => _studentMapper.MapFrom(student)).ToList();
		}

		public IList<StudentsDto> GetByFirstName(string firstName)
		{
		    return GetDbSet().WithFirstName(firstName).ToList().Select(student => _studentMapper.MapFrom(student)).ToList();
		}

		public IList<StudentsDto> GetBySecondName(string secondName)
		{
		    return GetDbSet().WithSecondName(secondName).ToList().Select(student => _studentMapper.MapFrom(student)).ToList();
		}

		public StudentsDto GetStudentById(int studentId)
		{
			return _studentMapper.MapFrom(GetDbSet().WithId(studentId));
		}
	}
}
