﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Entities.ModelsDTO;
using Newtonsoft.Json;
using SharedLibrary.Interfaces;

namespace SharedLibrary.HttpServices
{
    public class TransactionHttpService:ITransactionHttpService
    {
        #region Rest API Urls
        private readonly string createTransactionUrl = "api/Transaction/CreateTransaction";
        private readonly string editTransactionUrl = "api/Transaction/EditTransaction";
        private readonly string deleteTransactionUrl = "api/Transaction/DeleteTransaction?transactionId=";
        private readonly string getAllTransactionsUrl = "api/Transaction/GetAllTrancactions";
        private readonly string getTransactionByIdUrl = "api/Transaction/GetTransactionById?transactionId=";
        private readonly string getTransactionByTeacherIdUrl =
            "api/TransactionSearch/GetTransactionByTeahcerId?teacherId=";
        private readonly string getTransactionBySubjectIdUrl =
            "api/TransactionSearch/GetTransactionBySubjectId?subjectId=";
        private readonly string getTransactionByAudienceIdUrl =
            "api/TransactionSearch/GetTransactionByAudienceId?audienceId=";
        private readonly string getTransactionByTypeOfLessonUrl =
            "api/TransactionSearch/GetTransactionByTypeOfLesson?typeOfLesson=";
        private readonly string getTransactionByNumberOfWeekUrl =
            "api/TransactionSearch/GetTransactionByNumberOfWeek?numberOfWeek=";
        #endregion

        private readonly HttpService _httpService;

        public TransactionHttpService()
        {
            _httpService = new HttpService();
        }

        public void CreateTransaction(TransactionsDto transaction)
        {
            var _transaction = JsonConvert.SerializeObject(transaction);
            if (string.IsNullOrEmpty(_transaction)) throw new JsonSerializationException("Object transaction is empty");
            var buffer = Encoding.UTF8.GetBytes(_transaction);
            var content = new ByteArrayContent(buffer);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            _httpService.Post(createTransactionUrl, content);
        }

        public void EditTransaction(TransactionsDto transaction)
        {
            var _transaction = JsonConvert.SerializeObject(transaction);
            if (string.IsNullOrEmpty(_transaction)) throw new JsonSerializationException("Object transaction is empty");
            var buffer = Encoding.UTF8.GetBytes(_transaction);
            var content = new ByteArrayContent(buffer);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            _httpService.Put(editTransactionUrl, content);
        }

        public void DeleteTransaction(int idTransaction)
        {
            _httpService.Delete($"{deleteTransactionUrl}{idTransaction}");
        }

        public IList<TransactionsDto> GetByTeacherId(int teacherId)
        {
            return JsonConvert.DeserializeObject<List<TransactionsDto>>(
                _httpService.GetList($"{getTransactionByTeacherIdUrl}{teacherId}"));
        }

        public IList<TransactionsDto> GetBySubjectId(int subjectId)
        {
            return JsonConvert.DeserializeObject<List<TransactionsDto>>(
                _httpService.GetList($"{getTransactionBySubjectIdUrl}{subjectId}"));
        }

        public IList<TransactionsDto> GetByAudienceId(int audienceId)
        {
            return JsonConvert.DeserializeObject<List<TransactionsDto>>(
                _httpService.GetList($"{getTransactionByAudienceIdUrl}{audienceId}"));
        }

        public IList<TransactionsDto> GetByTypeOfLesson(string typeOfLesson)
        {
            return JsonConvert.DeserializeObject<List<TransactionsDto>>(
                _httpService.GetList($"{getTransactionByTypeOfLessonUrl}{typeOfLesson}"));
        }

        public IList<TransactionsDto> GetByNumberOfWeek(string numberOfWeek)
        {
            return JsonConvert.DeserializeObject<List<TransactionsDto>>(
                _httpService.GetList($"{getTransactionByNumberOfWeekUrl}{numberOfWeek}"));
        }

        public TransactionsDto GetTransactionsById(int idTransaction)
        {
            return JsonConvert.DeserializeObject<TransactionsDto>(
                _httpService.GetItem($"{getTransactionByIdUrl}{idTransaction}"));
        }

        public IList<TransactionsDto> GetAllTransactions()
        {
            return JsonConvert.DeserializeObject<List<TransactionsDto>>(
                _httpService.GetList(getAllTransactionsUrl));
        }
    }
}
