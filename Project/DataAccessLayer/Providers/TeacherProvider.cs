using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Mappers;
using DataAccessLayer.Predicates;
using Entities.ModelsDB;
using Entities.ModelsDTO;

namespace DataAccessLayer.Providers
{
public class TeacherProvider : BaseProvider<Teachers>, ITeacherProvider
{
	private readonly TeacherMapper _teacherMapper;

	public TeacherProvider()
	{
		_teacherMapper = new TeacherMapper();
	}

	protected override DbSet<Teachers> GetDbSet()
	{
		return Context.Teachers;
	}

	public TeachersDto GetByUserId(int userId)
	{
	    return _teacherMapper.MapFrom(GetDbSet().WithUserId(userId));
	}

	public IList<TeachersDto> GetByDepartmentId(int departmentId)
	{
	    return GetDbSet().WithDepartmentId(departmentId).ToList().Select(teacher => _teacherMapper.MapFrom(teacher)).ToList();
	}

	public IList<TeachersDto> GetByFirstName(string firstName)
	{
	    return GetDbSet().WithFirstName(firstName).ToList().Select(teacher => _teacherMapper.MapFrom(teacher)).ToList();
	}

	public IList<TeachersDto> GetBySecondName(string secondName)
	{
	    return GetDbSet().WithSecondName(secondName).ToList().Select(teacher => _teacherMapper.MapFrom(teacher)).ToList();
	}

	public IList<TeachersDto> GetByPosition(string position)
	{
	    return GetDbSet().WithPosition(position).ToList().Select(teacher => _teacherMapper.MapFrom(teacher)).ToList();
	}

	public TeachersDto GetTeacherById(int idTeacher)
	{
		return _teacherMapper.MapFrom(GetDbSet().WithId(idTeacher));
	}
}
}