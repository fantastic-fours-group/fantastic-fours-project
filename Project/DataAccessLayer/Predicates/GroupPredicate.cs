﻿using System.Linq;
using Entities.ModelsDB;

namespace DataAccessLayer.Predicates
{
    internal static class GroupPredicates
    {
        public static Groups WithId(this IQueryable<Groups> groups, int id)
        {
            return groups.First(g => g.Id == id);
        }

        public static IQueryable<Groups> WithDepartmentId(this IQueryable<Groups> groups, int departmentId)
        {
            return groups.Where(g => g.IdDepartment == departmentId);
        }

        public static IQueryable<Groups> WithName(this IQueryable<Groups> groups, string name)
        {
            return groups.Where(g => g.Name.Contains(name));
        }

        public static IQueryable<Groups> WithCourse(this IQueryable<Groups> groups, string course)
        {
            return groups.Where(g => g.Course == course);
        }
    }
}
