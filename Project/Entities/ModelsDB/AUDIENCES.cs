
namespace Entities.ModelsDB
{
    using System.Collections.Generic;
    
    public partial class Audiences
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Audiences()
        {
            this.Transactions = new HashSet<Transactions>();
        }
    
        public int Id { get; set; }
        public string Name { get; set; }
        public string TypeAudience { get; set; }
        public string Descriptions { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Transactions> Transactions { get; set; }
    }
}
