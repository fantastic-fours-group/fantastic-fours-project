﻿using System;
using System.Net;
using System.Web.Http;
using BusinessLogicLayer.Services;

namespace WebApi.Controllers
{
    public class AudienceSearchController : ApiController
    {
        private readonly AudienceService _audienceService;

        public AudienceSearchController()
        {
            _audienceService = new AudienceService();
        }

        // GET: api/AudienceSearch/GetAudienceByName?name=401
        [HttpGet]
        public IHttpActionResult GetAudienceByName(string name)
        {
            try
            {
                var audiences = _audienceService.GetAudienceByName(name);
                return Ok(audiences);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // GET: api/AudienceSearch/GetAudienceByType?type=Computer class
        [HttpGet]
        public IHttpActionResult GetAudienceByType(string type)
        {
            try
            {
                var audiences = _audienceService.GetByAudienceType(type);
                return Ok(audiences);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
