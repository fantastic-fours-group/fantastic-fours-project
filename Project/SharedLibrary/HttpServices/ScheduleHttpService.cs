﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Entities.ModelsDTO;
using Newtonsoft.Json;
using SharedLibrary.Interfaces;

namespace SharedLibrary.HttpServices
{
    public class ScheduleHttpService:IScheduleHttpService
    {
        #region Rest API Urls
        private readonly string createScheduleUrl = "api/Schedule/CreateSchedule";
        private readonly string editScheduleUrl = "api/Schedule/EditSchedule";
        private readonly string deleteScheduleUrl = "api/Schedule/DeleteSchedule?scheduleId=";
        private readonly string getAllSchedulesUrl = "api/Schedule/GetAllSchedule";
        private readonly string getScheduleByIdUrl = "api/Schedule/GetScheduleById?scheduleId=";
        private readonly string getScheduleByGroupIdUrl = "api/ScheduleSearch/GetScheduleByGroupId?groupId=";
        private readonly string getScheduleByTimeLessonIdUrl =
            "api/ScheduleSearch/GetScheduleByTimeLessonsId?timeLessonId=";
        private readonly string getScheduleByDayOfTheWeek =
            "api/ScheduleSearch/GetScheduleByDayOfTheWeek?dayOfTheWeek=";
        private readonly string getScheduleByTransactionId =
            "api/ScheduleSearch/GetScheduleByTransactionId?transactionId=";
        #endregion

        private readonly HttpService _httpService;

        public ScheduleHttpService()
        {
            _httpService = new HttpService();
        }

        public void CreateSchedule(SchedulesDto schedule)
        {
            var _schedule = JsonConvert.SerializeObject(schedule);
            if (string.IsNullOrEmpty(_schedule)) throw new JsonSerializationException("Object schedule is empty");
            var buffer = Encoding.UTF8.GetBytes(_schedule);
            var content = new ByteArrayContent(buffer);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            _httpService.Post(createScheduleUrl, content);
        }

        public void EditSchedule(SchedulesDto schedule)
        {
            var _schedule = JsonConvert.SerializeObject(schedule);
            if (string.IsNullOrEmpty(_schedule)) throw new JsonSerializationException("Object schedule is empty");
            var buffer = Encoding.UTF8.GetBytes(_schedule);
            var content = new ByteArrayContent(buffer);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            _httpService.Put(editScheduleUrl, content);
        }

        public void DeleteSchedule(int idSchedule)
        {
            _httpService.Delete($"{deleteScheduleUrl}{idSchedule}");
        }

        public IList<SchedulesDto> GetByGroupId(int groupId)
        {
            return JsonConvert.DeserializeObject<List<SchedulesDto>>(
                _httpService.GetList($"{getScheduleByGroupIdUrl}{groupId}"));
        }

        public IList<SchedulesDto> GetByTimeLessonId(int timeLessonId)
        {
            return JsonConvert.DeserializeObject<List<SchedulesDto>>(
                _httpService.GetList($"{getScheduleByTimeLessonIdUrl}{timeLessonId}"));
        }

        public IList<SchedulesDto> GetByDayOfTheWeek(string dayOfTheWeek)
        {
            return JsonConvert.DeserializeObject<List<SchedulesDto>>(
                _httpService.GetList($"{getScheduleByDayOfTheWeek}{dayOfTheWeek}"));
        }

        public IList<SchedulesDto> GetByTransactionId(int transactionId)
        {
            return JsonConvert.DeserializeObject<List<SchedulesDto>>(
                _httpService.GetList($"{getScheduleByTransactionId}{transactionId}"));
        }

        public SchedulesDto GetScheduleById(int idSchedule)
        {
            return JsonConvert.DeserializeObject<SchedulesDto>(
                _httpService.GetList($"{getScheduleByIdUrl}{idSchedule}"));
        }

        public IList<SchedulesDto> GetAllSchedules()
        {
            return JsonConvert.DeserializeObject<List<SchedulesDto>>(
                _httpService.GetList(getAllSchedulesUrl));
        }
    }
}
