﻿using System;
using System.Linq;
using System.Web.Mvc;
using Entities.ModelsDTO;
using Resourses.Enums;
using SharedLibrary.HttpServices;

namespace WebUI.Controllers
{
    public class ScheduleController : Controller
    {
        private readonly TransactionHttpService _transactionHttpService;
        private readonly TimeLessonHttpService _timeLessonHttpService;
        private readonly GroupHttpService _groupLessonHttpService;
        private readonly ScheduleHttpService _scheduleHttpService;
        private readonly TeacherHttpService _teacherHttpService;
        private readonly SubjectHttpService _subjectHttpService;
        private readonly AudienceHttpService _audienceHttpService;

        public ScheduleController()
        {
            _transactionHttpService = new TransactionHttpService();
            _timeLessonHttpService = new TimeLessonHttpService();
            _groupLessonHttpService = new GroupHttpService();
            _scheduleHttpService = new ScheduleHttpService();
            _teacherHttpService = new TeacherHttpService();
            _subjectHttpService = new SubjectHttpService();
            _audienceHttpService = new AudienceHttpService();
        }

        [HttpGet]
        public ActionResult CreateSchedule()
        {
            ViewBag.Groups = _groupLessonHttpService.GetAllGroups();
            ViewBag.TimeLessons = _timeLessonHttpService.GetAllTimeLessons();
            ViewBag.Teachers = _teacherHttpService.GetAllTeachers();
            ViewBag.Subjects = _subjectHttpService.GetAll();
            ViewBag.Audiences = _audienceHttpService.GetAllAudienceses();
            return PartialView();
        }
        [HttpPost]
        public ActionResult CreateSchedule(FormCollection form)
        {
            var idTeacher = int.Parse(form["IdTeacher"]);
            var idSubject = int.Parse(form["IdSubject"]);
            var idAudience = int.Parse(form["IdAudience"]);
            var idGroup = int.Parse(form["IdGroup"]);
            var typeOfLesson = form["TypeOfLesson"];
            var numberOfWeek = form["NumberOfWeek"];
            var idTimeLesson = int.Parse(form["IdTimeLesson"]);
            var dayOfWeek = form["DayOfTheWeek"];

            TransactionsDto transactions = new TransactionsDto();
            transactions.IdTeacher = idTeacher;
            transactions.IdAudience = idAudience;
            transactions.IdSubject = idSubject;
            transactions.TypeLessons = (ScheduleEnums.TypeOfLessons)Enum.Parse(typeof(ScheduleEnums.TypeOfLessons),typeOfLesson);
            transactions.NumberOfWeek = (ScheduleEnums.NumberOfWeek)Enum.Parse(typeof(ScheduleEnums.NumberOfWeek),numberOfWeek);
            _transactionHttpService.CreateTransaction(transactions);
            var lastTransaction = _transactionHttpService.GetAllTransactions().LastOrDefault();

            if (lastTransaction != null)
            {
                SchedulesDto schedule = new SchedulesDto();
                schedule.IdGroup = idGroup;
                schedule.TransactionId = lastTransaction.Id;
                schedule.IdTimeLessons = idTimeLesson;
                schedule.DayOfTheWeek = (ScheduleEnums.DayOfWeek)Enum.Parse(typeof(ScheduleEnums.DayOfWeek), dayOfWeek);
                _scheduleHttpService.CreateSchedule(schedule);
            }
            return RedirectToAction("Schedules","Schedule");
        }
        [HttpGet]
        public ActionResult EditSchedule(int id) 
        {
            if (id > 0)
            {
                ViewBag.Groups = _groupLessonHttpService.GetAllGroups();
                ViewBag.TimeLessons = _timeLessonHttpService.GetAllTimeLessons();
                ViewBag.Teachers = _teacherHttpService.GetAllTeachers();
                ViewBag.Subjects = _subjectHttpService.GetAll();
                ViewBag.Audiences = _audienceHttpService.GetAllAudienceses();

                var schedule = _scheduleHttpService.GetScheduleById(id);
                var transaction = _transactionHttpService.GetTransactionsById(schedule.TransactionId);

                ViewBag.Group = _groupLessonHttpService.GetGroupById(schedule.IdGroup);
                ViewBag.TimeLesson = _timeLessonHttpService.GetTimeLessonsById(schedule.IdTimeLessons);
                ViewBag.DayOfWeek = schedule.DayOfTheWeek;
                ViewBag.Teacher = _teacherHttpService.GetTeacherById(transaction.IdTeacher);
                ViewBag.Subject = _subjectHttpService.GetSubjectById(transaction.IdSubject);
                ViewBag.Audience = _audienceHttpService.GetAudienceById(transaction.IdAudience);
                ViewBag.TypeOfLesson = transaction.TypeLessons;
                ViewBag.NumberOfWeek = transaction.NumberOfWeek;
                ViewBag.TransactionId = transaction.Id;
                ViewBag.Id = id;
                return PartialView();
            }
            return RedirectToAction("Schedules", "Schedule");
        }
        [HttpPost]
        public ActionResult EditSchedule(FormCollection form) 
        {
            var scheduleId = int.Parse(form["Id"]);
            var transactionId = int.Parse(form["TransactionId"]);
            var idTeacher = int.Parse(form["IdTeacher"]);
            var idSubject = int.Parse(form["IdSubject"]);
            var idAudience = int.Parse(form["IdAudience"]);
            var idGroup = int.Parse(form["IdGroup"]);
            var typeOfLesson = form["TypeOfLesson"];
            var numberOfWeek = form["NumberOfWeek"];
            var idTimeLesson = int.Parse(form["IdTimeLesson"]);
            var dayOfWeek = form["DayOfTheWeek"];

            TransactionsDto transaction = new TransactionsDto();
            transaction.Id = transactionId;
            transaction.IdTeacher = idTeacher;
            transaction.IdAudience = idAudience;
            transaction.IdSubject = idSubject;
            transaction.TypeLessons = (ScheduleEnums.TypeOfLessons)Enum.Parse(typeof(ScheduleEnums.TypeOfLessons), typeOfLesson);
            transaction.NumberOfWeek = (ScheduleEnums.NumberOfWeek)Enum.Parse(typeof(ScheduleEnums.NumberOfWeek), numberOfWeek);
            _transactionHttpService.EditTransaction(transaction);

            SchedulesDto schedule = new SchedulesDto();
            schedule.Id = scheduleId;
            schedule.IdGroup = idGroup;
            schedule.TransactionId = transaction.Id;
            schedule.IdTimeLessons = idTimeLesson;
            schedule.DayOfTheWeek = (ScheduleEnums.DayOfWeek)Enum.Parse(typeof(ScheduleEnums.DayOfWeek), dayOfWeek);
            _scheduleHttpService.EditSchedule(schedule);

            return RedirectToAction("Schedules", "Schedule");
        }
        [HttpGet]
        public ActionResult DeleteSchedule(int id)
        {
            if (id > 0)
            {
                var schedule = _scheduleHttpService.GetScheduleById(id);
                var transaction = _transactionHttpService.GetTransactionsById(schedule.TransactionId);

                ViewBag.Group = _groupLessonHttpService.GetGroupById(schedule.IdGroup);
                ViewBag.TimeLesson = _timeLessonHttpService.GetTimeLessonsById(schedule.IdTimeLessons);
                ViewBag.DayOfWeek = schedule.DayOfTheWeek;
                ViewBag.Teacher = _teacherHttpService.GetTeacherById(transaction.IdTeacher);
                ViewBag.Subject = _subjectHttpService.GetSubjectById(transaction.IdSubject);
                ViewBag.Audience = _audienceHttpService.GetAudienceById(transaction.IdAudience);
                ViewBag.TypeOfLesson = transaction.TypeLessons;
                ViewBag.NumberOfWeek = transaction.NumberOfWeek;
                ViewBag.TransactionId = transaction.Id;
                ViewBag.Id = id;
                return PartialView();
            }
            return RedirectToAction("Schedules", "Schedule");
        }
        [HttpPost]
        public ActionResult DeleteScheduleById(FormCollection form)
        {
            var scheduleId = int.Parse(form["Id"]);
            var transactionId = int.Parse(form["TransactionId"]);
            if (scheduleId > 0 && transactionId > 0)
            {
                _scheduleHttpService.DeleteSchedule(scheduleId);
                _transactionHttpService.DeleteTransaction(transactionId);
            }
            return RedirectToAction("Schedules", "Schedule");
        }

        [HttpGet]
        public ActionResult Schedules(int id = 1)
        {
            if (id > 0)
            {
                ViewBag.Groups = _groupLessonHttpService.GetAllGroups();
                ViewBag.Schedules = _scheduleHttpService.GetByGroupId(id);
                ViewBag.Transactions = _transactionHttpService.GetAllTransactions();
                ViewBag.TimeLessons = _timeLessonHttpService.GetAllTimeLessons();
                ViewBag.Teachers = _teacherHttpService.GetAllTeachers();
                ViewBag.Subjects = _subjectHttpService.GetAll();
                ViewBag.Audience = _audienceHttpService.GetAllAudienceses();
            }
            return View();
        }
    }
}