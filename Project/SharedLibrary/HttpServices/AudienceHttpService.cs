﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Entities.ModelsDTO;
using Newtonsoft.Json;
using SharedLibrary.Interfaces;

namespace SharedLibrary.HttpServices
{
    public class AudienceHttpService : IAudienceHttpService
    {
        #region Rest API Urls
        private readonly string createAudieceUrl = "api/Audience/CreateAudience";
        private readonly string editAudienceUrl = "api/Audience/EditAudience";
        private readonly string deleteAudienceUrl = "api/Audience/DeleteAudience?audienceId=";
        private readonly string getAllAudienceUrl = "api/Audience/GetAllAudiences";
        private readonly string getAudienceByIdUrl = "api/Audience/GetAudienceById?audienceId=";
        private readonly string getAudienceByNameUrl = "api/AudienceSearch/GetAudienceByName?name=";
        private readonly string getAudienceByType = "api/AudienceSearch/GetAudienceByType?type=";
        #endregion

        private readonly HttpService _httpService;

        public AudienceHttpService()
        {
            _httpService = new HttpService();
        }

        public void CreateAudience(AudiencesDto audience)
        {
            var _audience = JsonConvert.SerializeObject(audience);
            if (string.IsNullOrEmpty(_audience)) throw new JsonSerializationException("Object audience is empty");
            var buffer = Encoding.UTF8.GetBytes(_audience);
            var content = new ByteArrayContent(buffer);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            _httpService.Post(createAudieceUrl, content);
        }

        public void DeleteAudience(int idAudience)
        {
            _httpService.Delete($"{deleteAudienceUrl}{idAudience}");
        }

        public void EditAudience(AudiencesDto audience)
        {
            var _audience = JsonConvert.SerializeObject(audience);
            if (string.IsNullOrEmpty(_audience)) throw new JsonSerializationException("Object audience is empty");
            var buffer = Encoding.UTF8.GetBytes(_audience);
            var content = new ByteArrayContent(buffer);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            _httpService.Put(editAudienceUrl, content);
        }

        public IList<AudiencesDto> GetAllAudienceses()
        {
            return JsonConvert.DeserializeObject<List<AudiencesDto>>(_httpService.GetList(getAllAudienceUrl));
        }

        public AudiencesDto GetAudienceById(int audienceId)
        {
            return JsonConvert.DeserializeObject<AudiencesDto>(_httpService.GetItem($"{getAudienceByIdUrl}{audienceId}"));
        }

        public IList<AudiencesDto> GetAudienceByName(string name)
        {
            return JsonConvert.DeserializeObject<List<AudiencesDto>>(_httpService.GetList($"{getAudienceByNameUrl}{name}"));
        }

        public IList<AudiencesDto> GetByAudienceType(string audienceType)
        {
            return JsonConvert.DeserializeObject<List<AudiencesDto>>(_httpService.GetList($"{getAudienceByType}{audienceType}"));
        }
    }
}
