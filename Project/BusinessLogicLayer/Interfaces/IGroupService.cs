﻿using System.Collections.Generic;
using Entities.ModelsDTO;

namespace BusinessLogicLayer.Interfaces
{
    public interface IGroupService
    {
        void CreateGroup(GroupsDto group);
        void EditGroup(GroupsDto group);
        void DeleteGroup(int idGroup);
        IList<GroupsDto> GetGroupByDepartmentId(int departmentId);
        IList<GroupsDto> GetGroupByName(string name);
        IList<GroupsDto> GetGroupByCourse(string course);
        GroupsDto GetGroupById(int idGroup);
        IList<GroupsDto> GetAll();
    }
}
