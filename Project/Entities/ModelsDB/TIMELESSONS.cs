namespace Entities.ModelsDB
{
    using System;
    using System.Collections.Generic;
    
    public partial class TimeLessons
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TimeLessons()
        {
            this.Schedules = new HashSet<Schedules>();
        }
    
        public int Id { get; set; }
        public System.TimeSpan StartLessons { get; set; }
        public System.TimeSpan FinishLessons { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Schedules> Schedules { get; set; }
    }
}
