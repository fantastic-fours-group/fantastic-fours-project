﻿using System;
using Entities.ModelsDB;
using Entities.ModelsDTO;
using Resourses.Enums;
using Resourses.Parsers;

namespace DataAccessLayer.Mappers
{
    public class AudienceMapper
    {
        public AudiencesDto MapFrom(Audiences audiences)
        {
            return new AudiencesDto
            {
                Id = audiences.Id,
                Name = audiences.Name,
                TypeAudience =
                    (ScheduleEnums.AudienceType)Enum.Parse(typeof(ScheduleEnums.AudienceType), audiences.TypeAudience.Replace(" ", ""),
                        true),
                Descriptions = audiences.Descriptions
            };
        }

        public Audiences MapTo(AudiencesDto audiences)
        {
            return new Audiences
            {
                Id = audiences.Id,
                Name = audiences.Name,
                TypeAudience = audiences.TypeAudience.GetStringValue(),
                Descriptions = audiences.Descriptions
            };
        }
    }
}
