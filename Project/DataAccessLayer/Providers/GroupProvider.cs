using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Mappers;
using DataAccessLayer.Predicates;
using Entities.ModelsDB;
using Entities.ModelsDTO;

namespace DataAccessLayer.Providers
{
    public class GroupProvider : BaseProvider<Groups>, IGroupProvider
    {
        private readonly GroupMapper _groupMapper;

        public GroupProvider()
        {
            _groupMapper = new GroupMapper();
        }

        protected override DbSet<Groups> GetDbSet()
        {
            return Context.Groups;
        }

        public IList<GroupsDto> GetByDepartmentId(int departmentId)
        {
            return GetDbSet().WithDepartmentId(departmentId).ToList().Select(@group => _groupMapper.MapFrom(@group)).ToList();
        }

        public IList<GroupsDto> GetByName(string name)
        {
            return GetDbSet().WithName(name).ToList().Select(group=>_groupMapper.MapFrom(group)).ToList();
        }

        public IList<GroupsDto> GetByCourse(string course)
        {
            return GetDbSet().WithCourse(course).ToList().Select(@group => _groupMapper.MapFrom(@group)).ToList();
        }

        public GroupsDto GetGroupById(int idGroup)
        {
            return _groupMapper.MapFrom(GetDbSet().WithId(idGroup));
        }
    }
}