namespace Entities.ModelsDB
{
    using System;
    using System.Collections.Generic;
    
    public partial class Students
    {
        public int Id { get; set; }
        public int IdUser { get; set; }
        public int IdGroup { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
    
        public virtual Groups Groups { get; set; }
        public virtual Users Users { get; set; }
    }
}
