using System.Collections.Generic;
using Entities.ModelsDTO;

namespace DataAccessLayer.Interfaces
{
public interface IStudentProvider
{
	StudentsDto GetByUserId(int userId);
	IList<StudentsDto> GetByGroupId(int groupId);
	IList<StudentsDto> GetByFirstName(string firstName);
	IList<StudentsDto> GetBySecondName(string secondName);
	StudentsDto GetStudentById(int studentId);
}
}