using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Mappers;
using DataAccessLayer.Predicates;
using Entities.ModelsDB;
using Entities.ModelsDTO;

namespace DataAccessLayer.Providers
{
    public class DepartmentProvider : BaseProvider<Departments>, IDepartmentProvider
    {
        private readonly DepartmentMapper _departmentMapper;

        public DepartmentProvider()
        {
            _departmentMapper = new DepartmentMapper();
        }

        protected override DbSet<Departments> GetDbSet()
        {
            return Context.Departments;
        }

        public IList<DepartmentsDto> GetDepartmentByName(string name)
        {
            return GetDbSet().WithName(name).ToList().Select(department => _departmentMapper.MapFrom(department)).ToList();
        }

        public DepartmentsDto GetDepartmentById(int idDepartment)
        {
            return _departmentMapper.MapFrom(GetDbSet().WithId(idDepartment));
        }
    }
}