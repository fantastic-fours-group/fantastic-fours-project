﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Validators;
using DataAccessLayer.Mappers;
using DataAccessLayer.Providers;
using Entities.ModelsDTO;
using Resourses.Enums;

namespace BusinessLogicLayer.Services
{
    public class TeacherService:ITeacherService
    {
        private readonly TeacherMapper _teacherMapper;
        private readonly DepartmentProvider _departmentProvider;
        private readonly TeacherProvider _teacherProvider;
        private readonly AttributeValidator _validator;

        public TeacherService()
        {
            _teacherMapper = new TeacherMapper();
            _departmentProvider = new DepartmentProvider();
            _teacherProvider = new TeacherProvider();
            _validator = new AttributeValidator();
        }

        private void ValidateId(int id)
        {
            if (id < 1)
                throw new ArgumentException("Id must be positive number");
            if (id <= 0) throw new ArgumentOutOfRangeException(nameof(id));
        }

        public void CreateTeacher(TeachersDto teacher)
        {
            _validator.Validate(teacher);
            _teacherProvider.Create(_teacherMapper.MapTo(teacher));
        }

        public void EditTeacher(TeachersDto teacher)
        {
            _validator.Validate(teacher);
            _teacherProvider.Update(_teacherMapper.MapTo(teacher));
        }

        public void DeleteTeacher(int idTeacher)
        {
            ValidateId(idTeacher);
            _teacherProvider.Delete(_teacherMapper.MapTo(GetTeacherById(idTeacher)));
        }

        public TeachersDto GetByUserId(int userId)
        {
            ValidateId(userId);
            return _teacherProvider.GetByUserId(userId);
        }

        public IList<TeachersDto> GetByDepartmentId(int departmentId)
        {
            ValidateId(departmentId);
            return _teacherProvider.GetByDepartmentId(departmentId);
        }

        public IList<TeachersDto> SearchTeacher(string firstName, string secondName, string position, string department)
        {
            var positionEnum =
                (ScheduleEnums.TeacherPosition)Enum.Parse(typeof(ScheduleEnums.TeacherPosition),
                    position.Replace(" ", ""));
            return GetAll().Where(teacher =>
                        teacher.FirstName.Contains(firstName) && teacher.SecondName.Contains(secondName) &&
                        teacher.Position.Equals(positionEnum) &&
                        teacher.IdDepartment.Equals(_departmentProvider.GetDepartmentByName(department).First().Id))
                    .ToList();
        }

        public IList<TeachersDto> GetByPosition(string position)
        {
            return _teacherProvider.GetByPosition(position);
        }

        public TeachersDto GetTeacherById(int idTeacher)
        {
            ValidateId(idTeacher);
            return _teacherProvider.GetTeacherById(idTeacher);
        }

        public IList<TeachersDto> GetAll()
        {
            return _teacherProvider.GetAll().ToList().Select(teacher => _teacherMapper.MapFrom(teacher)).ToList();
        }
    }
}
