﻿using System.Collections.Generic;
using Entities.ModelsDTO;

namespace BusinessLogicLayer.Interfaces
{
    public interface IStudentService
    {
        void CreateStudent(StudentsDto student);
        void EditStudent(StudentsDto student);
        void DeleteStudent(int idStudent);
        StudentsDto GetByUserId(int userId);
        IList<StudentsDto> GetByGroupId(int groupId);
        IList<StudentsDto> SearchStudent(string firstName, string secondName, string @group);
        StudentsDto GetStudentById(int studentId);
        IList<StudentsDto> GetAll();
    }
}
