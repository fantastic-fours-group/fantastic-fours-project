﻿using System;
using Entities.ModelsDB;
using Entities.ModelsDTO;
using Resourses.Enums;
using Resourses.Parsers;

namespace DataAccessLayer.Mappers
{
    public class UserMapper
    {
        public UsersDto MapFrom(Users users)
        {
            return new UsersDto
            {
                Id = users.Id,
                UserEmail = users.UserEmail,
                UserLogin = users.UserLogin,
                UserPassword = users.UserPassword,
                UserRole = (ScheduleEnums.UserRoles) Enum.Parse(typeof(ScheduleEnums.UserRoles),
                    users.UserRole.Replace(" ", ""),
                    true)
            };
        }

        public Users MapTo(UsersDto users)
        {
            return new Users
            {
                Id = users.Id,
                UserEmail = users.UserEmail,
                UserLogin = users.UserLogin,
                UserPassword = users.UserPassword,
                UserRole = users.UserRole.GetStringValue()
            };
        }
    }
}
