﻿using Resourses.Attributes;

namespace Resourses.Enums
{
    public class ScheduleEnums
    {
        public enum AudienceType
        {
            [StringValue("Computer class")]
            ComputerClass,
            [StringValue("Lecture class")]
            LectureClass
        }

        public enum Course
        {
            [StringValue("1")]
            First = 1,
            [StringValue("2")]
            Second,
            [StringValue("3")]
            Third,
            [StringValue("4")]
            Fourth,
            [StringValue("5")]
            Fifth,
            [StringValue("6")]
            Sixth
        }

        public enum UserRoles
        {
            [StringValue("Student")]
            Student,
            [StringValue("Teacher")]
            Teacher,
            [StringValue("Admin")]
            Admin
        }

        public enum TeacherPosition
        {
            [StringValue("Professor")]
            Professor,
            [StringValue("Postgraduate")]
            Postgraduate,
            [StringValue("Docent")]
            Docent,
            [StringValue("Assistant")]
            Assistant,
            [StringValue("Senior Lecturer")]
            SeniorLecturer,
            [StringValue("Trainee")]
            Trainee,
            [StringValue("Teacher")]
            Teacher,
            [StringValue("Doctoral student")]
            Doctoralstudent,
            [StringValue("Researcher")]
            Researcher
        }

        public enum TypeOfLessons
        {
            [StringValue("Lecture")]
            Lecture,
            [StringValue("Practice")]
            Practice,
            [StringValue("Laboratory")]
            Laboratory
        }

        public enum NumberOfWeek
        {
            [StringValue("First")]
            First,
            [StringValue("Second")]
            Second,
            [StringValue("All")]
            All
        }

        public enum DayOfWeek
        {
            [StringValue("Monday")]
            Monday,
            [StringValue("Tuesday")]
            Tuesday,
            [StringValue("Wednesday")]
            Wednesday,
            [StringValue("Thursday")]
            Thursday,
            [StringValue("Friday")]
            Friday,
            [StringValue("Saturday")]
            Saturday,
            [StringValue("Sunday")]
            Sunday
        }
    }
}
