using System.Collections.Generic;
using Entities.ModelsDTO;

namespace DataAccessLayer.Interfaces
{
	public interface IScheduleProvider
	{
		IList<SchedulesDto> GetByGroupId(int groupId);
		IList<SchedulesDto> GetByTimeLessonId(int timeLessonId);
		IList<SchedulesDto> GetByDayOfTheWeek(string dayOfTheWeek);
		IList<SchedulesDto> GetByTransactionId(int transactionId);
		SchedulesDto GetScheduleById(int idSchedule);
	}
}