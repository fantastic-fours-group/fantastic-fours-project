using System.Linq;
using Entities.ModelsDB;

namespace DataAccessLayer.Predicates
{
    internal static class TransactionPredicate
    {
        public static Transactions WithId(this IQueryable<Transactions> transactions, int id)
        {
            return transactions.First(tr => tr.Id == id);
        }

        public static IQueryable<Transactions> WithTeacherId(this IQueryable<Transactions> transactions, int teacherId)
        {
            return transactions.Where(tr => tr.IdTeacher == teacherId);
        }

        public static IQueryable<Transactions> WithSubjectId(this IQueryable<Transactions> transactions, int subjectId)
        {
            return transactions.Where(tr => tr.IdSubject == subjectId);
        }

        public static IQueryable<Transactions> WithAudienceId(this IQueryable<Transactions> transactions,
            int audienceId)
        {
            return transactions.Where(tr => tr.IdAudience == audienceId);
        }

        public static IQueryable<Transactions> WithTypeOfLesson(this IQueryable<Transactions> transactions,
            string typeOfLesson)
        {
            return transactions.Where(tr => tr.TypeOfLesson == typeOfLesson);
        }

        public static IQueryable<Transactions> WithNumberOfWeek(this IQueryable<Transactions> transactions,
            string numberOfWeek)
        {
            return transactions.Where(tr => tr.NumberOfWeek == numberOfWeek);
        }
    }
}