﻿using System;
using System.Security.Cryptography;
using System.Text;
using BusinessLogicLayer.Interfaces;

namespace BusinessLogicLayer.Services
{
    public class HashingService : IHashingService
    {
        public string GetHash(string value)
        {
            var hash = new MD5CryptoServiceProvider();
            var data = hash.ComputeHash(Encoding.UTF8.GetBytes(value));
            var sBuilder = new StringBuilder();
            foreach (var t in data)
            {
                sBuilder.Append(t.ToString("x2"));
            }
            return sBuilder.ToString();
        }

        public bool VerifyHash(MD5 hash, string value, string hashValue)
        {
            var hashOfInput = GetHash(value);
            var comparer = StringComparer.OrdinalIgnoreCase;
            return comparer.Compare(hashOfInput, hashValue) == 0;
        }
    }
}
