using System.Linq;
using Entities.ModelsDB;

namespace DataAccessLayer.Predicates
{
	internal static class TeacherPredicate
	{
		public static Teachers WithId(this IQueryable<Teachers> teachers, int id)
		{
			return teachers.First(t => t.Id == id);
		}

		public static Teachers WithUserId(this IQueryable<Teachers> teachers, int userId)
		{
			return teachers.First(t => t.IdUser == userId);
		}

		public static IQueryable<Teachers> WithDepartmentId(this IQueryable<Teachers> teachers, int departmentId)
		{
			return teachers.Where(t => t.IdDepartment == departmentId);
		}

		public static IQueryable<Teachers> WithFirstName(this IQueryable<Teachers> teachers, string firstName)
		{
			return teachers.Where(t => t.FirstName == firstName);
		}

		public static IQueryable<Teachers> WithSecondName(this IQueryable<Teachers> teachers, string secondName)
		{
			return teachers.Where(t => t.SecondName == secondName);
		}

		public static IQueryable<Teachers> WithPosition(this IQueryable<Teachers> teachers, string position)
		{
			return teachers.Where(t => t.Position == position);
		}
	}
}