﻿using System.Collections.Generic;
using Entities.ModelsDTO;

namespace BusinessLogicLayer.Interfaces
{
    public interface IAudienceService
    {
        void CreateAudience(AudiencesDto audience);
        void EditAudience(AudiencesDto audience);
        void DeleteAudience(int idAudience);
        IList<AudiencesDto> GetAudienceByName(string name);
        IList<AudiencesDto> GetByAudienceType(string audienceType);
        AudiencesDto GetAudienceById(int audienceId);
        IList<AudiencesDto> GetAllAudienceses();
    }
}
