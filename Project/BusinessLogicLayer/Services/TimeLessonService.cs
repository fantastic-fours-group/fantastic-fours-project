﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Validators;
using DataAccessLayer.Mappers;
using DataAccessLayer.Providers;
using Entities.ModelsDTO;

namespace BusinessLogicLayer.Services
{
    public class TimeLessonService:ITimeLessonService
    {
        private readonly TimeLessonsMapper _timeLessonsMapper;
        private readonly TimeLessonProvider _timeLessonProvider;
        private readonly AttributeValidator _validator;

        public TimeLessonService()
        {
            _timeLessonsMapper = new TimeLessonsMapper();
            _timeLessonProvider = new TimeLessonProvider();
            _validator = new AttributeValidator();
        }

        private void ValidateId(int id)
        {
            if (id < 1)
                throw new ArgumentException("Id must be positive number");
            if (id <= 0) throw new ArgumentOutOfRangeException(nameof(id));
        }

        public void CreateTimeLesson(TimeLessonsDto timeLesson)
        {
            _validator.Validate(timeLesson);
            _timeLessonProvider.Create(_timeLessonsMapper.MapTo(timeLesson));
        }

        public void EditTimeLesson(TimeLessonsDto timeLesson)
        {
            _validator.Validate(timeLesson);
            _timeLessonProvider.Update(_timeLessonsMapper.MapTo(timeLesson));
        }

        public void DeleteTimeLesson(int idTimeLesson)
        {
            ValidateId(idTimeLesson);
            _timeLessonProvider.Delete(_timeLessonsMapper.MapTo(GetTimeLessonsById(idTimeLesson)));
        }

        public TimeLessonsDto SearchTimeLessons(TimeSpan start, TimeSpan finish)
        {
            return GetAllTimeLessons().First(timeLesson => timeLesson.StartLessons.Equals(start) && timeLesson.FinishLessons.Equals(finish));
        }

        public TimeLessonsDto GetTimeLessonsById(int idTimeLesson)
        {
            ValidateId(idTimeLesson);
            return _timeLessonProvider.GetTimeLessonsById(idTimeLesson);
        }

        public IList<TimeLessonsDto> GetAllTimeLessons()
        {
            return _timeLessonProvider.GetAll().ToList().Select(timeLesson => _timeLessonsMapper.MapFrom(timeLesson)).ToList();
        }
    }
}
