﻿using System;
using System.Net;
using System.Web.Http;
using BusinessLogicLayer.Services;

namespace WebApi.Controllers
{
    public class ScheduleSearchController : ApiController
    {
        private readonly ScheduleService _scheduleService;

        public ScheduleSearchController()
        {
            _scheduleService = new ScheduleService();
        }

        // GET: api/ScheduleSearch/GetScheduleByGroupId?groupId=2
        [HttpGet]
        public IHttpActionResult GetScheduleByGroupId(int groupId)
        {
            try
            {
                var schedules = _scheduleService.GetByGroupId(groupId);
                return Ok(schedules);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // GET: api/ScheduleSearch/GetScheduleByTimeLessonsId?timeLessonId=2
        [HttpGet]
        public IHttpActionResult GetScheduleByTimeLessonsId(int timeLessonId)
        {
            try
            {
                var schedules = _scheduleService.GetByTimeLessonId(timeLessonId);
                return Ok(schedules);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // Get: api/ScheduleSearch/GetScheduleByDayOfTheWeek?dayOfTheWeek=Monday
        [HttpGet]
        public IHttpActionResult GetScheduleByDayOfTheWeek(string dayOfTheWeek)
        {
            try
            {
                var schedules = _scheduleService.GetByDayOfTheWeek(dayOfTheWeek);
                return Ok(schedules);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // Get: api/ScheduleSearch/GetScheduleByTransactionId?transactionId=11
        [HttpGet]
        public IHttpActionResult GetScheduleByTransactionId(int transactionId)
        {
            try
            {
                var schedules = _scheduleService.GetByTransactionId(transactionId);
                return Ok(schedules);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
