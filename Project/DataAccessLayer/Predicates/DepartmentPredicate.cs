using System.Linq;
using Entities.ModelsDB;

namespace DataAccessLayer.Predicates
{
    internal static class DepartmentPredicates
    {
        public static Departments WithId(this IQueryable<Departments> departments, int id)
        {
            return departments.First(d => d.Id == id);
        }

        public static IQueryable<Departments> WithName(this IQueryable<Departments> departments, string name)
        {
            return departments.Where(d => d.Name.Contains(name));
        }
    }
}