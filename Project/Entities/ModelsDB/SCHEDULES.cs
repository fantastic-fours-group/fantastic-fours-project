namespace Entities.ModelsDB
{
    using System;
    using System.Collections.Generic;
    
    public partial class Schedules
    {
        public int Id { get; set; }
        public int IdGroup { get; set; }
        public int IdTimeLesson { get; set; }
        public string DayOfTheWeek { get; set; }
        public int IdTransaction { get; set; }
    
        public virtual Groups Groups { get; set; }
        public virtual TimeLessons TimeLessons { get; set; }
        public virtual Transactions Transactions { get; set; }
    }
}
