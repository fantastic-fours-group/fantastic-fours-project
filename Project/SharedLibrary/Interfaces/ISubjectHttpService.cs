﻿using System.Collections.Generic;
using Entities.ModelsDTO;

namespace SharedLibrary.Interfaces
{
    public interface ISubjectHttpService
    {
        void CreateSubject(SubjectsDto subject);
        void EditSubject(SubjectsDto subject);
        void DeleteSubject(int idSubject);
        List<SubjectsDto> GetSubjectByName(string name);
        SubjectsDto GetSubjectById(int idSubject);
        IList<SubjectsDto> GetAll();
    }
}
