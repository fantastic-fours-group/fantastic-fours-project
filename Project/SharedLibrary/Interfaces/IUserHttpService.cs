﻿using System.Collections.Generic;
using Entities.ModelsDTO;

namespace SharedLibrary.Interfaces
{
    public interface IUserHttpService
    {
        void CreateUser(UsersDto user);
        void EditUser(UsersDto user);
        void DeleteUser(int idUser);
        UsersDto GetUserByEmail(string userEmail); // can be delete
        UsersDto GetUserByCredentials(string userLogin, string userPassword);
        void UpdateUserPassword(string userLogin, string oldPassword, string newPassword);
        IList<UsersDto> GetUserByRole(string userRole);
        UsersDto GetUserById(int idUser);
        IList<UsersDto> GetAllUsers();
    }
}
