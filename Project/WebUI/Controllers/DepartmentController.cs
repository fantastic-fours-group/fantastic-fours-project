﻿using System;
using System.Web.Mvc;
using Entities.ModelsDTO;
using SharedLibrary.HttpServices;

namespace WebUI.Controllers
{
    public class DepartmentController : Controller
    {
        private readonly DepartmentHttpService _departmentService;

        public DepartmentController()
        {
            _departmentService = new DepartmentHttpService();
        }

        [HttpGet]
        public ActionResult CreateDepartment()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult CreateDepartment(DepartmentsDto model)
        {
            if (ModelState.IsValid)
            {
                _departmentService.CreateDepartment(model);
            }
            return RedirectToAction("Departments","Department");
        }

        [HttpGet]
        public ActionResult EditDepartment(int id)
        {
            if (id > 0) { 
                var department = _departmentService.GetDepartmentById(id); 
                return PartialView(department);
            }
            return RedirectToAction("Departments", "Department");
        }

        [HttpPost]
        public ActionResult EditDepartment(DepartmentsDto model)
        {
            if (ModelState.IsValid)
            {
                _departmentService.EditDepartment(model);
            }
            return RedirectToAction("Departments", "Department");
        }

        [HttpGet]
        public ActionResult DeleteDepartment(int id)
        {
            if (id > 0)
            {
                var department = _departmentService.GetDepartmentById(id);
                return PartialView(department);
            }
            return RedirectToAction("Departments", "Department");
        }

        [HttpPost]
        public ActionResult DeleteDepartmentById(int id)
        {
            if (id > 0)
            {
                _departmentService.DeleteDepartment(id);
            }
            return RedirectToAction("Departments", "Department");
        }

        [HttpGet]
        public ActionResult Departments(string name)
        {
            ViewBag.Departments = _departmentService.GetAllDepartments();
            if (!String.IsNullOrEmpty(name))
            {
                ViewBag.Departments = _departmentService.GetDepartmentByName(name);
            }
            return View();
        }
    }
}