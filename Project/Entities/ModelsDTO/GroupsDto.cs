﻿using System.ComponentModel.DataAnnotations;
using Resourses.Enums;

namespace Entities.ModelsDTO
{
    public class GroupsDto
    {
        [Required(ErrorMessage = "Name is a required field for entity Group")]
        public int Id { get; set; }

        [Required(ErrorMessage = "IdDepartment is a required field for entity Group")]
        public int IdDepartment { get; set; }

        [Required(ErrorMessage = "Name is a required field for entity Group")]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "Name size must be between 1 and 50 characters for entity Group")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Course is a required field for entity Group")]
        public ScheduleEnums.Course Course { get; set; }
    }
}
