﻿using System.Collections.Generic;
using Entities.ModelsDTO;

namespace SharedLibrary.Interfaces
{
    public interface IStudentHttpService
    {
        void CreateStudent(StudentsDto student);
        void EditStudent(StudentsDto student);
        void DeleteStudent(int idStudent);
        StudentsDto GetByUserId(int userId);
        IList<StudentsDto> GetByGroupId(int groupId);
        IList<StudentsDto> SearchStudent(string search);
        StudentsDto GetStudentById(int studentId);
        IList<StudentsDto> GetAllStudents();
    }
}
