﻿using System;
using System.Net;
using System.Web.Http;
using BusinessLogicLayer.Services;

namespace WebApi.Controllers
{
    public class SubjectSearchController : ApiController
    {
        private readonly SubjectService _subjectService;

        public SubjectSearchController()
        {
            _subjectService = new SubjectService();
        }

        // GET: api/SubjectSearch/GetSubjectByName?name=Modern DBMS
        [HttpGet]
        public IHttpActionResult GetSubjectByName(string name)
        {
            try
            {
                var subjects = _subjectService.GetSubjectByName(name);
                return Ok(subjects);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
