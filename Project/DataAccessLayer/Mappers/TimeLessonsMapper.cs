﻿using Entities.ModelsDB;
using Entities.ModelsDTO;

namespace DataAccessLayer.Mappers
{
    public class TimeLessonsMapper
    {
        public TimeLessonsDto MapFrom(TimeLessons timeLessons)
        {
            return new TimeLessonsDto
            {
                Id = timeLessons.Id,
                StartLessons = timeLessons.StartLessons,
                FinishLessons = timeLessons.FinishLessons
            };
        }

        public TimeLessons MapTo(TimeLessonsDto timeLessons)
        {
            return new TimeLessons
            {
                Id = timeLessons.Id,
                StartLessons = timeLessons.StartLessons,
                FinishLessons = timeLessons.FinishLessons
            };
        }
    }
}
