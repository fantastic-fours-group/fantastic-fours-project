﻿using Entities.ModelsDTO;
using System.Collections.Generic;

namespace SharedLibrary.Interfaces
{
    public interface IAudienceHttpService
    {
        void CreateAudience(AudiencesDto audience);
        void EditAudience(AudiencesDto audience);
        void DeleteAudience(int idAudience);
        IList<AudiencesDto> GetAudienceByName(string name);
        IList<AudiencesDto> GetByAudienceType(string audienceType);
        AudiencesDto GetAudienceById(int audienceId);
        IList<AudiencesDto> GetAllAudienceses();
    }
}
