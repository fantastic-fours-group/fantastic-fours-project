﻿using System;
using Entities.ModelsDB;
using Entities.ModelsDTO;
using Resourses.Enums;
using Resourses.Parsers;

namespace DataAccessLayer.Mappers
{
    public class GroupMapper
    {
        public GroupsDto MapFrom(Groups groups)
        {
            return new GroupsDto
            {
                Id = groups.Id,
                IdDepartment = groups.IdDepartment,
                Name = groups.Name,
                Course = (ScheduleEnums.Course) Enum.Parse(typeof(ScheduleEnums.Course), groups.Course.Replace(" ", ""),
                    true),
            };
        }

        public Groups MapTo(GroupsDto groups)
        {
            return new Groups
            {
                Id = groups.Id,
                IdDepartment = groups.IdDepartment,
                Name = groups.Name,
                Course = groups.Course.GetStringValue()
            };
        }
    }
}
