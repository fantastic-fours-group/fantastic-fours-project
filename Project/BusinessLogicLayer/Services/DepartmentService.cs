﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Validators;
using DataAccessLayer.Mappers;
using DataAccessLayer.Providers;
using Entities.ModelsDTO;

namespace BusinessLogicLayer.Services
{
    public class DepartmentService : IDepartmentService
    {
        private readonly DepartmentMapper _departmentMapper;
        private readonly DepartmentProvider _departmentProvider;
        private readonly AttributeValidator _validator;

        public DepartmentService()
        {
            _departmentMapper = new DepartmentMapper();
            _departmentProvider = new DepartmentProvider();
            _validator = new AttributeValidator();
        }

        private void ValidateId(int id)
        {
            if (id < 1)
                throw new ArgumentException("Id must be positive number");
            if (id <= 0) throw new ArgumentOutOfRangeException(nameof(id));
        }

        public void CreateDepartment(DepartmentsDto department)
        {
            _validator.Validate(department);
            _departmentProvider.Create(_departmentMapper.MapTo(department));
        }

        public void EditDepartment(DepartmentsDto department)
        {
            _validator.Validate(department);
            _departmentProvider.Update(_departmentMapper.MapTo(department));
        }

        public void DeleteDepartment(int idDepartment)
        {
            ValidateId(idDepartment);
            _departmentProvider.Delete(_departmentMapper.MapTo(GetDepartmentById(idDepartment)));
        }

        public IList<DepartmentsDto> GetDepartmentByName(string name)
        {
            return _departmentProvider.GetDepartmentByName(name);
        }

        public DepartmentsDto GetDepartmentById(int idDepartment)
        {
            ValidateId(idDepartment);
            return _departmentProvider.GetDepartmentById(idDepartment);
        }

        public IList<DepartmentsDto> GetAllDepartments()
        {
            return _departmentProvider.GetAll().ToList().Select(department => _departmentMapper.MapFrom(department)).ToList();
        }
    }
}
