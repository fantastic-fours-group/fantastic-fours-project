using System.Collections.Generic;
using Entities.ModelsDTO;

namespace DataAccessLayer.Interfaces
{
    public interface ITransactionProvider
    {
        IList<TransactionsDto> GetByTeacherId(int teacherId);
        IList<TransactionsDto> GetBySubjectId(int subjectId);
        IList<TransactionsDto> GetByAudienceId(int audienceId);
        IList<TransactionsDto> GetByTypeOfLesson(string typeOfLesson);
        IList<TransactionsDto> GetByNumberOfWeek(string numberOfWeek);
        TransactionsDto GetTransactionsById(int idTransaction);
    }
}