﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Validators;
using DataAccessLayer.Mappers;
using DataAccessLayer.Providers;
using Entities.ModelsDTO;

namespace BusinessLogicLayer.Services
{
    public class AudienceService : IAudienceService
    {
        private readonly AudienceProvider _audienceProvider;
        private readonly AudienceMapper _audienceMapper;
        private readonly AttributeValidator _validator;

        public AudienceService()
        {
            _audienceProvider = new AudienceProvider();
            _audienceMapper = new AudienceMapper();
            _validator = new AttributeValidator();
        }

        private void ValidateId(int id)
        {
            if (id < 1)
                throw new ArgumentException("Id must be positive number");
            if (id <= 0) throw new ArgumentOutOfRangeException(nameof(id));
        }

        public void CreateAudience(AudiencesDto audience)
        {
            _validator.Validate(audience);
            _audienceProvider.Create(_audienceMapper.MapTo(audience));
        }

        public void EditAudience(AudiencesDto audience)
        {
            _validator.Validate(audience);
            _audienceProvider.Update(_audienceMapper.MapTo(audience));
        }

        public void DeleteAudience(int idAudience)
        {
            ValidateId(idAudience);
            _audienceProvider.Delete(_audienceMapper.MapTo(GetAudienceById(idAudience)));
        }

        public IList<AudiencesDto> GetAudienceByName(string name)
        {
            return _audienceProvider.GetAudienceByName(name);
        }

        public IList<AudiencesDto> GetByAudienceType(string audienceType)
        {
            return _audienceProvider.GetByAudienceType(audienceType);
        }
        
        public AudiencesDto GetAudienceById(int audienceId)
        {
            ValidateId(audienceId);
            return _audienceProvider.GetById(audienceId);
        }

        public IList<AudiencesDto> GetAllAudienceses()
        {
            return _audienceProvider.GetAll().ToList().Select(audience => _audienceMapper.MapFrom(audience)).ToList();
        }
    }
}
