﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Validators;
using DataAccessLayer.Mappers;
using DataAccessLayer.Providers;
using Entities.ModelsDTO;

namespace BusinessLogicLayer.Services
{
    public class StudentService : IStudentService
    {
        private readonly StudentProvider _studentProvider;
        private readonly StudentMapper _studentMapper;
        private readonly AttributeValidator _validator;
        private readonly GroupProvider _groupProvider;

        public StudentService()
        {
            _studentProvider = new StudentProvider();
            _studentMapper = new StudentMapper();
            _validator = new AttributeValidator();
            _groupProvider = new GroupProvider();
        }

        private void ValidateId(int id)
        {
            if (id < 1)
                throw new ArgumentException("Id must be positive number");
            if (id <= 0) throw new ArgumentOutOfRangeException(nameof(id));
        }

        public void CreateStudent(StudentsDto student)
        {
            _validator.Validate(student);
            _studentProvider.Create(_studentMapper.MapTo(student));
        }

        public void EditStudent(StudentsDto student)
        {
            _validator.Validate(student);
            _studentProvider.Update(_studentMapper.MapTo(student));
        }

        public void DeleteStudent(int idStudent)
        {
            ValidateId(idStudent);
            _studentProvider.Delete(_studentMapper.MapTo(GetStudentById(idStudent)));
        }

        public StudentsDto GetByUserId(int userId)
        {
            ValidateId(userId);
            return _studentProvider.GetByUserId(userId);
        }

        public IList<StudentsDto> GetByGroupId(int groupId)
        {
            ValidateId(groupId);
            return _studentProvider.GetByGroupId(groupId);
        }

        public IList<StudentsDto> SearchStudent(string firstName, string secondName, string group)
        {
            return GetAll().Where(student =>
                    student.FirstName.Contains(firstName) && student.SecondName.Contains(secondName) &&
                    student.IdGroup == _groupProvider.GetByName(@group).First().Id).ToList();
        }

        public StudentsDto GetStudentById(int studentId)
        {
            ValidateId(studentId);
            return _studentProvider.GetStudentById(studentId);
        }

        public IList<StudentsDto> GetAll()
        {
            return _studentProvider.GetAll().ToList().Select(student => _studentMapper.MapFrom(student)).ToList();
        }
    }
}
