﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Web.Http;
using BusinessLogicLayer.Services;

namespace WebApi.Controllers
{
    public class UserSearchController : ApiController
    {
        private readonly UserService _userService;

        public UserSearchController()
        {
            _userService = new UserService();
        }

        // GET: api/UserSearch/GetUserByEmail?userEmail=user4@gmail.com
        [HttpGet]
        public IHttpActionResult GetUserByEmail(string userEmail)
        {
            try
            {
                var user = _userService.GetUserByEmail(userEmail);
                return Ok(user);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // GET: api/UserSearch/GetUserByCredentials?userLogin=admin1123&userPassword=1234
        [HttpGet]
        public IHttpActionResult GetUserByCredentials(string userLogin, string userPassword)
        {
            try
            {
                var user = _userService.GetUserByCredentials(userLogin, userPassword);
                return Ok(user);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // GET: api/UserSearch/GetUserByRole?userRole=Admin
        [HttpGet]
        public IHttpActionResult GetUserByRole(string userRole)
        {
            try
            {
                var user = _userService.GetUserByRole(userRole);
                return Ok(user);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // GET: api/UserSearch/UpdateUserPassword?userLogin=admin1123&oldPassword=1234&newPassword=12345
        [HttpGet]
        public IHttpActionResult UpdateUserPassword(string userLogin, string oldPassword, string newPassword)
        {
            try
            {
                _userService.UpdateUserPassword(userLogin, oldPassword, newPassword);
                return Ok();
            }
            catch (ValidationException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
