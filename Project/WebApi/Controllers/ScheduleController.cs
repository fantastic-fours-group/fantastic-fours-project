﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Web.Http;
using BusinessLogicLayer.Services;
using Entities.ModelsDTO;

namespace WebApi.Controllers
{
    public class ScheduleController : ApiController
    {
        private readonly ScheduleService _scheduleService;

        public ScheduleController()
        {
            _scheduleService = new ScheduleService();
        }

        // GET: api/Schedule/GetAllSchedule
        [HttpGet]
        public IHttpActionResult GetAllSchedule()
        {
            try
            {
                var schedules = _scheduleService.GetAll();
                return Ok(schedules);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // GET: api/Schedule/GetScheduleById?scheduleId=1
        [HttpGet]
        public IHttpActionResult GetScheduleById(int scheduleId)
        {
            try
            {
                var schedule = _scheduleService.GetScheduleById(scheduleId);
                return Ok(schedule);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // POST: api/Schedule/CreateSchedule
        [HttpPost]
        public IHttpActionResult CreateSchedule(SchedulesDto schedule)
        {
            try
            {
                _scheduleService.CreateSchedule(schedule);
                return Content(HttpStatusCode.Created, schedule);
            }
            catch (ValidationException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // PUT: api/Schedule/EditSchedule
        [HttpPut]
        public IHttpActionResult EditSchedule(SchedulesDto schedule)
        {
            try
            {
                _scheduleService.EditSchedule(schedule);
                return Content(HttpStatusCode.Created, schedule);
            }
            catch (ValidationException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // DELETE: api/Schedule/DeleteSchedule?scheduleId=3
        [HttpDelete]
        public IHttpActionResult DeleteSchedule(int scheduleId)
        {
            try
            {
                _scheduleService.DeleteSchedule(scheduleId);
                return Content(HttpStatusCode.OK, scheduleId);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
