IF NOT EXISTS(SELECT 1 FROM sys.databases s WHERE s.name = 'Schedule')
    CREATE DATABASE Schedule
GO
 
USE [Schedule]
GO
 
IF NOT EXISTS(SELECT 1 FROM sys.objects s WHERE s.name = 'Subjects' AND s.TYPE = 'U')
  CREATE TABLE Subjects
  (
    Id INT IDENTITY(1,1) NOT NULL
    ,Name VARCHAR(100) UNIQUE NOT NULL
    ,CONSTRAINT PK_subject PRIMARY KEY (Id)
  )
GO
 
IF NOT EXISTS(SELECT 1 FROM sys.objects s WHERE s.name = 'Audiences' AND s.TYPE = 'U')
  CREATE TABLE Audiences
  (
    Id INT IDENTITY(1,1) NOT NULL
    ,Name VARCHAR(100) NOT NULL  UNIQUE
	,TypeAudience VARCHAR(20)
	,Descriptions VARCHAR(100)
    ,CHECK(TypeAudience='Computer class' or TypeAudience='Lecture class')
    ,CONSTRAINT PK_audience PRIMARY KEY (Id)
  )
GO
 
IF NOT EXISTS(SELECT 1 FROM sys.objects s WHERE s.name = 'TimeLessons' AND s.TYPE = 'U')
  CREATE TABLE TimeLessons
  (
    Id INT IDENTITY(1,1) NOT NULL
    ,StartLessons TIME NOT NULL
    ,FinishLessons TIME NOT NULL
    ,CONSTRAINT PK_lesson PRIMARY KEY (Id)
  )
GO
 
IF NOT EXISTS(SELECT 1 FROM sys.objects s WHERE s.name = 'Departments' AND s.TYPE = 'U')
  CREATE TABLE Departments
  (
    Id INT IDENTITY(1,1) NOT NULL
    ,Name VARCHAR(100) NOT NULL  UNIQUE
    ,CONSTRAINT PK_departmentID PRIMARY KEY (Id)
  )
GO
 
IF NOT EXISTS(SELECT 1 FROM sys.objects s WHERE s.name = 'Groups' AND s.TYPE = 'U')
  CREATE TABLE Groups
  (
    Id INT IDENTITY(1,1) NOT NULL
    ,IdDepartment INT NOT NULL
    ,Name VARCHAR(50) NOT NULL
    ,Course nchar(1) NOT NULL
    ,CONSTRAINT PK_groupID PRIMARY KEY (Id)
    ,CONSTRAINT FK_departmentID FOREIGN KEY (IdDepartment) REFERENCES dbo.DEPARTMENTS(Id) ON DELETE CASCADE
    ,CHECK(Course='1' or Course='2' or Course='3' or Course='4' or Course='5' or Course='6')
  )
GO
 

IF NOT EXISTS(SELECT 1 FROM sys.objects s WHERE s.name = 'Users' AND s.type = 'U')
  CREATE TABLE Users
  (
    Id INT IDENTITY(1,1) NOT NULL
    ,UserEmail VARCHAR(100) NOT NULL UNIQUE
    ,UserLogin VARCHAR(50) NOT NULL
    ,UserPassword VARCHAR(50) NOT NULL
    ,UserRole nchar(10) NOT NULL 
    ,CONSTRAINT PK_userID PRIMARY KEY (Id)
    ,CONSTRAINT [email_validator]  check(UserEmail LIKE '%___@___%.__%')
    ,CHECK(UserRole='Student' or UserRole='Admin' or UserRole='Teacher')
  )
GO
 

IF NOT EXISTS(SELECT 1 FROM sys.objects s WHERE s.name = 'Teachers' AND s.TYPE = 'U')
  CREATE TABLE Teachers
  (
    Id INT IDENTITY(1,1) NOT NULL
    ,IdUser INT NOT NULL
    ,IdDepartment INT NOT NULL
    ,FirstName VARCHAR(50) NOT NULL
    ,SecondName VARCHAR(50) NOT NULL
    ,Position nchar(25) NOT NULL
    ,CONSTRAINT PK_teacherID PRIMARY KEY (Id)
	,CONSTRAINT FK_teacher FOREIGN KEY (IdUser) REFERENCES dbo.USERS(Id) ON DELETE CASCADE
    ,CONSTRAINT FK_department FOREIGN KEY (IdDepartment) REFERENCES dbo.DEPARTMENTS(Id) ON DELETE CASCADE
    ,CHECK(Position='Professor' or Position='Postgraduate' or Position='Docent' or Position='Assistant' or Position='Senior Lecturer' or Position='Trainee' or Position='Teacher' or Position='Doctoral student' or Position='Researcher')
  )
GO
 
IF NOT EXISTS(SELECT 1 FROM sys.objects s WHERE s.name = 'Students' AND s.TYPE = 'U')
  CREATE TABLE Students
  (
    Id INT IDENTITY(1,1) NOT NULL
    ,IdUser INT NOT NULL
    ,IdGroup INT NOT NULL
    ,FirstName VARCHAR(50) NOT NULL
    ,SecondName VARCHAR(50) NOT NULL
    ,CONSTRAINT PK_studentID PRIMARY KEY (Id)
    ,CONSTRAINT FK_student FOREIGN KEY (IdUser) REFERENCES dbo.USERS(Id) ON DELETE CASCADE
    ,CONSTRAINT FK_groupID FOREIGN KEY (IdGroup) REFERENCES dbo.GROUPS(Id) ON DELETE CASCADE
  )
GO
 
IF NOT EXISTS(SELECT 1 FROM sys.objects s WHERE s.name = 'Transactions' AND s.type = 'U')
  CREATE TABLE Transactions
  (
    Id INT IDENTITY(1,1) NOT NULL
    ,IdTeacher INT NOT NULL
    ,IdSubject INT NOT NULL
    ,IdAudience INT NOT NULL
    ,TypeOfLesson NCHAR(20) NOT NULL
    ,NumberOfWeek VARCHAR(10) NOT NULL
    ,CONSTRAINT PK_transactionID PRIMARY KEY (Id)
    ,CONSTRAINT Teacher_fk FOREIGN KEY (IdTeacher) REFERENCES dbo.TEACHERS(Id) ON DELETE CASCADE
    ,CONSTRAINT fk_subject FOREIGN KEY (IdSubject) REFERENCES dbo.SUBJECTS(Id) ON DELETE CASCADE
    ,CONSTRAINT FK_audience FOREIGN KEY (IdAudience) REFERENCES dbo.AUDIENCES(Id) ON DELETE CASCADE
    ,CHECK(NumberOfWeek='First' or NumberOfWeek='Second' or NumberOfWeek='All')
    ,CHECK(TypeOfLesson='Lecture' or TypeOfLesson='Practice' or TypeOfLesson='Laboratory')
  )
GO
 
IF NOT EXISTS(SELECT 1 FROM sys.objects s WHERE s.name = 'Schedules' AND s.type = 'U')
  CREATE TABLE Schedules
  (
    Id INT IDENTITY(1,1) NOT NULL
    ,IdGroup INT NOT NULL
    ,IdTimeLesson INT NOT NULL
    ,DayOfTheWeek nchar(10) NOT NULL
    ,IdTransaction INT NOT NULL
    ,CONSTRAINT PK_sheduleID PRIMARY KEY (Id)
    ,CONSTRAINT fk_group FOREIGN KEY (IdGroup) REFERENCES dbo.GROUPS(Id) ON DELETE CASCADE
    ,CONSTRAINT fk_timelesson FOREIGN KEY (IdTimeLesson) REFERENCES dbo.TIMELESSONS(Id) ON DELETE CASCADE
    ,CONSTRAINT fk_transactionId FOREIGN KEY (IdTransaction) REFERENCES dbo.TRANSACTIONS(Id)
    ,CHECK(DayOfTheWeek='Monday' or DayOfTheWeek='Tuesday' or DayOfTheWeek='Wednesday' or DayOfTheWeek='Thursday' or DayOfTheWeek='Friday' or DayOfTheWeek='Saturday' or DayOfTheWeek='Sunday')
  )
GO