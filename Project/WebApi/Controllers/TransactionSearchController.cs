﻿using System;
using System.Net;
using System.Web.Http;
using BusinessLogicLayer.Services;

namespace WebApi.Controllers
{
    public class TransactionSearchController : ApiController
    {
        private readonly TransactionService _transactionService;

        public TransactionSearchController()
        {
            _transactionService = new TransactionService();
        }

        // GET: api/TransactionSearch/GetTransactionByTeahcerId?teacherId=1
        [HttpGet]
        public IHttpActionResult GetTransactionByTeahcerId(int teacherId)
        {
            try
            {
                var transactions = _transactionService.GetByTeacherId(teacherId);
                return Ok(transactions);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // GET: api/TransactionSearch/GetTransactionBySubjectId?subjectId=1
        [HttpGet]
        public IHttpActionResult GetTransactionBySubjectId(int subjectId)
        {
            try
            {
                var transactions = _transactionService.GetBySubjectId(subjectId);
                return Ok(transactions);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // GET: api/TransactionSearch/GetTransactionByAudienceId?audienceId=1
        [HttpGet]
        public IHttpActionResult GetTransactionByAudienceId(int audienceId)
        {
            try
            {
                var transactions = _transactionService.GetByAudienceId(audienceId);
                return Ok(transactions);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // GET: api/TransactionSearch/GetTransactionByTypeOfLesson?typeOfLesson=Lecture
        [HttpGet]
        public IHttpActionResult GetTransactionByTypeOfLesson(string typeOfLesson)
        {
            try
            {
                var transactions = _transactionService.GetByTypeOfLesson(typeOfLesson);
                return Ok(transactions);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // GET: api/TransactionSearch/GetTransactionByNumberOfWeek?numberOfWeek=First
        [HttpGet]
        public IHttpActionResult GetTransactionByNumberOfWeek(string numberOfWeek)
        {
            try
            {
                var transactions = _transactionService.GetByNumberOfWeek(numberOfWeek);
                return Ok(transactions);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
