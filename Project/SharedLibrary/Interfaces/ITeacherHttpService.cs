﻿using System.Collections.Generic;
using Entities.ModelsDTO;

namespace SharedLibrary.Interfaces
{
    interface ITeacherHttpService
    {
        void CreateTeacher(TeachersDto teacher);
        void EditTeacher(TeachersDto teacher);
        void DeleteTeacher(int idTeacher);
        TeachersDto GetByUserId(int userId);
        IList<TeachersDto> GetByDepartmentId(int departmentId);
        IList<TeachersDto> SearchTeacher(string search, string position, string department);
        IList<TeachersDto> GetByPosition(string position);
        TeachersDto GetTeacherById(int idTeacher);
        IList<TeachersDto> GetAllTeachers();
    }
}
