USE [Schedule]
GO

DELETE FROM [dbo].[Schedules] 
IF exists(SELECT *
          FROM sys.identity_columns
          WHERE object_name(object_id) = 'Schedules' AND last_value IS NOT NULL)
  DBCC CHECKIDENT ([Schedules], RESEED, 0)
GO

DELETE FROM [dbo].[TimeLessons] 
IF exists(SELECT *
          FROM sys.identity_columns
          WHERE object_name(object_id) = 'TimeLessons' AND last_value IS NOT NULL)
  DBCC CHECKIDENT ([TimeLessons], RESEED, 0)
GO

DELETE FROM [dbo].[Transactions] 
IF exists(SELECT *
          FROM sys.identity_columns
          WHERE object_name(object_id) = 'Transactions' AND last_value IS NOT NULL)
  DBCC CHECKIDENT ([Transactions], RESEED, 0)
GO

DELETE FROM [dbo].[Students] 
IF exists(SELECT *
          FROM sys.identity_columns
          WHERE object_name(object_id) = 'Students' AND last_value IS NOT NULL)
  DBCC CHECKIDENT ([Students], RESEED, 0)
GO

DELETE FROM [dbo].[Teachers] 
IF exists(SELECT *
          FROM sys.identity_columns
          WHERE object_name(object_id) = 'Teachers' AND last_value IS NOT NULL)
  DBCC CHECKIDENT ([Teachers], RESEED, 0)
GO

DELETE FROM [dbo].[Users] 
IF exists(SELECT *
          FROM sys.identity_columns
          WHERE object_name(object_id) = 'Users' AND last_value IS NOT NULL)
  DBCC CHECKIDENT ([Users], RESEED, 0)
GO

DELETE FROM [dbo].[Groups] 
IF exists(SELECT *
          FROM sys.identity_columns
          WHERE object_name(object_id) = 'Groups' AND last_value IS NOT NULL)
  DBCC CHECKIDENT ([Groups], RESEED, 0)
GO

DELETE FROM [dbo].[Departments] 
IF exists(SELECT *
          FROM sys.identity_columns
          WHERE object_name(object_id) = 'DEPARTMENTS' AND last_value IS NOT NULL)
  DBCC CHECKIDENT ([Departments], RESEED, 0)
GO

DELETE FROM [dbo].[Audiences] 
IF exists(SELECT *
          FROM sys.identity_columns
          WHERE object_name(object_id) = 'Audiences' AND last_value IS NOT NULL)
  DBCC CHECKIDENT ([Audiences], RESEED, 0)
GO

DELETE FROM [dbo].[Subjects] 
IF exists(SELECT *
          FROM sys.identity_columns
          WHERE object_name(object_id) = 'Subjects' AND last_value IS NOT NULL)
  DBCC CHECKIDENT ([Subjects], RESEED, 0)
GO

INSERT INTO [dbo].[Audiences] ([Name], [TypeAudience], [Descriptions]) VALUES 
												(3, 'Computer class', '15 computers'),
												(15, 'Computer class', '15 computers'),
												(18, 'Lecture class', '15 places'),
												(19, 'Computer class', '15 computers'),
												(21, 'Lecture class', '30 places'),
												(24, 'Lecture class', '15 places'),
												(31, 'Computer class', '15 computers'),
												(33, 'Computer class', '15 computers'),
												(39, 'Lecture class', '16 places')
GO

INSERT INTO [dbo].[Subjects] ([Name]) VALUES ('Theory of programming'),
											 ('Designing programs.systems'),
											 ('Systems and methods of decision making'),
											 ('Information systems of accounting'),
											 ('Modern DBMS'),
											 ('Platforms of corporate information systems'),
											 ('Methods of optimization and operations research')
GO

INSERT INTO [dbo].[Departments] ([Name]) VALUES ('Algebra and computer science'),
												('Mathematical analysis'),
												('Differential equations'),
												('Applied Mathematics'),
												('Mathematical modeling')
GO

INSERT INTO [dbo].[Groups] ([IdDepartment],[Name],[Course]) VALUES (5,'401',4),
																   (5,'411',4),
																   (4,'402',4),
																   (3,'404',4),
																   (2,'405',4),
																   (5,'407',4)
GO

INSERT INTO [dbo].[Users] ([UserLogin],[UserPassword],[UserEmail],[UserRole]) VALUES 
																   ('admin','1234','admin@gmail.com','Admin'),
																   ('User1','abc1234','user1@gmail.com','Teacher'),
																   ('User2','abc1234','user2@gmail.com','Teacher'),
																   ('User3','abc1234','user3@gmail.com','Teacher'),
																   ('User4','abc1234','user4@gmail.com','Teacher'),
																   ('User5','abc1234','user5@gmail.com','Teacher'),
																   ('User6','abc1234','user6@gmail.com','Student'),
																   ('User7','abc1234','user7@gmail.com','Student'),
																   ('User8','abc1234','user8@gmail.com','Student'),
																   ('User9','abc1234','user9@gmail.com','Student'),
																   ('User10','abc1234','user10@gmail.com','Student')
GO
INSERT INTO [dbo].[Teachers] ([IdUser],[FirstName],[SecondName],[IdDepartment],[Position]) VALUES 
																   (2,'Andrew','Pertsov',5,'Assistant'),
																   (3,'Natalia','Romanenko',4,'Assistant'),
																   (4,'Igor','Scootar',4,'Assistant'),
																   (5,'Vasyl','Kushnirchuk',5,'Docent'),	
																   (6,'Nikolai','Gorbatenko',5,'Assistant')
																   
GO

INSERT INTO [dbo].[Students] ([IdUser],[FirstName],[SecondName],[IdGroup]) VALUES 
																   (7,'Alexey','Mitzkan',1),
																   (8,'Mykhailo','Mytskan',5),
																   (9,'Igor','Kosovich',3),
																   (10,'Yaroslav','Vyshyvanyuk',6),
																   (11,'Mykhailo','Lukan',5)
GO

INSERT INTO [dbo].[Transactions] ([IdTeacher],[IdSubject],[IdAudience],[TypeOfLesson],[NumberOfWeek]) VALUES 
																   (1,1,3,'Lecture','First'),
																   (2,2,4,'Practice','First'),
																   (3,2,2,'Practice','First'),
																   (1,1,4,'Practice','All'),
																   (4,3,1,'Lecture','First'),
																   (4,3,7,'Practice','First'),
																   (4,7,9,'Lecture','Second'),
																   (5,6,5,'Lecture','First'),
																   (5,6,9,'Practice','All'),
																   (2,2,8,'Lecture','First'),
																   (3,2,8,'Lecture','Second')
GO

INSERT INTO [dbo].[TimeLessons] ([StartLessons],[FinishLessons]) VALUES
												('08:20','09:40'),
												('09:50','11:10'),
												('11:30','12:50'),
												('13:00','14:20'),
												('14:40','16:00'),
												('16:10','17:30'),
												('17:40','19:00'),
												('19:10','20:30')
GO

INSERT INTO [dbo].[Schedules] ([IdGroup],[IdTransaction],[IdTimeLesson],[DayOfTheWeek]) VALUES
																   (1,1,1,'Monday'),
																   (2,1,1,'Monday'),
																   (2,2,2,'Monday'),
																   (1,3,2,'Monday'),
																   (1,5,3,'Monday'),
																   (2,4,1,'Tuesday'),
																   (2,6,2,'Tuesday'),
																   (1,4,1,'Wednesday'),
																   (2,8,1,'Wednesday'),
																   (2,9,2,'Wednesday'),
																   (1,7,1,'Thursday'),
																   (2,7,1,'Thursday'),
																   (1,10,2,'Friday'),
																   (2,10,2,'Friday'),
																   (3,10,2,'Friday'),
																   (1,11,2,'Friday'),
																   (2,11,2,'Friday'),
																   (3,11,2,'Friday')
GO

