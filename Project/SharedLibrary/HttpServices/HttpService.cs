﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Net.Http;
using SharedLibrary.Interfaces;

namespace SharedLibrary.HttpServices
{
    public class HttpService : IHttpService
    {
        private const string BaseUrl = "http://localhost:65226/";

        public string Put(string urlResponce, HttpContent content)
        {
            var returnValue = string.Empty;
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseUrl);
                var response = client.PutAsync(urlResponce, content).Result;
                if (response.IsSuccessStatusCode)
                {
                    returnValue = response.Content.ReadAsStringAsync().Result;
                }
                else if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    throw new ValidationException();
                }
                else if (response.StatusCode == HttpStatusCode.InternalServerError)
                {
                    throw new Exception();
                }

                return returnValue;
            }
        }

        public string Post(string urlResponce, HttpContent content)
        {
            var returnValue = string.Empty;
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseUrl);
                var response = client.PostAsync(urlResponce, content).Result;
                if (response.IsSuccessStatusCode)
                {
                    returnValue = response.Content.ReadAsStringAsync().Result;
                }
                else if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    throw new ValidationException();
                }
                else if (response.StatusCode == HttpStatusCode.InternalServerError)
                {
                    throw new Exception();
                }

                return returnValue;
            }
        }

        public string Delete(string urlResponce)
        {
            var returnValue = string.Empty;
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseUrl);
                var response = client.DeleteAsync(urlResponce).Result;
                if (response.IsSuccessStatusCode)
                {
                    returnValue = response.Content.ReadAsStringAsync().Result;
                }
                else if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    throw new ArgumentException();
                }
                else if (response.StatusCode == HttpStatusCode.InternalServerError)
                {
                    throw new Exception();
                }

                return returnValue;
            }
        }

        public string GetItem(string urlResponce)
        {
            var returnValue = string.Empty;
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseUrl);
                var response = client.GetAsync(urlResponce).Result;
                if (response.IsSuccessStatusCode)
                {
                    returnValue = response.Content.ReadAsStringAsync().Result;
                }
                else if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    throw new ArgumentException();
                }
                else if (response.StatusCode == HttpStatusCode.InternalServerError)
                {
                    throw new Exception();
                }

                return returnValue;
            }
        }

        public string GetList(string urlResponce)
        {
            var returnValue = string.Empty;
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseUrl);
                var response = client.GetAsync(urlResponce).Result;
                if (response.IsSuccessStatusCode)
                {
                    returnValue = response.Content.ReadAsStringAsync().Result;
                }
                else if (response.StatusCode == HttpStatusCode.InternalServerError)
                {
                    throw new Exception();
                }

                return returnValue;
            }

        }
    }
}
