﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Mappers;
using DataAccessLayer.Predicates;
using Entities.ModelsDB;
using Entities.ModelsDTO;

namespace DataAccessLayer.Providers
{
    public class TransactionProvider : BaseProvider<Transactions>, ITransactionProvider
    {
        private readonly TransactionMapper _transactionMapper;

        public TransactionProvider()
        {
            _transactionMapper = new TransactionMapper();
        }

        protected override DbSet<Transactions> GetDbSet()
        {
            return Context.Transactions;
        }

        public IList<TransactionsDto> GetByTeacherId(int teacherId)
        {
            return GetDbSet().WithTeacherId(teacherId).ToList().Select(transaction => _transactionMapper.MapFrom(transaction)).ToList();
        }

        public IList<TransactionsDto> GetBySubjectId(int subjectId)
        {
            return GetDbSet().WithSubjectId(subjectId).ToList().Select(transaction => _transactionMapper.MapFrom(transaction)).ToList();
        }

        public IList<TransactionsDto> GetByAudienceId(int audienceId)
        {
            return GetDbSet().WithAudienceId(audienceId).ToList().Select(transaction => _transactionMapper.MapFrom(transaction)).ToList();
        }

        public IList<TransactionsDto> GetByTypeOfLesson(string typeOfLesson)
        {
            return GetDbSet().WithTypeOfLesson(typeOfLesson).ToList().Select(transaction => _transactionMapper.MapFrom(transaction)).ToList();
        }

        public IList<TransactionsDto> GetByNumberOfWeek(string numberOfWeek)
        {
            return GetDbSet().WithNumberOfWeek(numberOfWeek).ToList().Select(transaction => _transactionMapper.MapFrom(transaction)).ToList();
        }

        public TransactionsDto GetTransactionsById(int idTransaction)
        {
            return _transactionMapper.MapFrom(GetDbSet().WithId(idTransaction));
        }
    }
}
