﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Entities.ModelsDTO;
using Newtonsoft.Json;
using SharedLibrary.Interfaces;

namespace SharedLibrary.HttpServices
{
    public class TeacherHttpService:ITeacherHttpService
    {
        #region Rest API Urls
        private readonly string createTeacherUrl = "api/Teacher/CreateTeacher";
        private readonly string editTeacherUrl = "api/Teacher/EditTeacher";
        private readonly string deleteTeacherUrl = "api/Teacher/DeleteTeacher?teacherId=";
        private readonly string getTeacherByIdUrl = "api/Teacher/GetTeacherById?teacherId=";
        private readonly string getAllTeachersUrl = "api/Teacher/GetAllTeachers";
        private readonly string getTeacherByUserIdUrl = "api/TeacherSearch/GetTeacherByUserId?userId=";
        private readonly string getTeacherByDepratmentIdUrl =
            "api/TeacherSearch/GetTeacherByDepartmentId?departmentId=";
        private readonly string searchTeacherUrl = "api/TeacherSearch/SearchTeacher?firstName=";
        private readonly string getTeacherByPositionUrl = "api/TeacherSearch/GetTeacherByPosition?position=";
        #endregion

        private readonly HttpService _httpService;

        public TeacherHttpService()
        {
            _httpService = new HttpService();
        }


        private List<string> ParseSearchString(string search)
        {
            var result = new List<string>();
            var sep = new[] { ' ' };
            string[] splited = search.Split(sep);
            if (search.Trim(sep).IndexOfAny(sep) < 0)
                result = new List<string> { splited.First() };
            else if (splited.Length == 2)
            {
                result = new List<string> { splited.First(), splited.Last() };
            }
            return result;
        }

        public void CreateTeacher(TeachersDto teacher)
        {
            var _teacher = JsonConvert.SerializeObject(teacher);
            if (string.IsNullOrEmpty(_teacher)) throw new JsonSerializationException("Object teacher is empty");
            var buffer = Encoding.UTF8.GetBytes(_teacher);
            var content = new ByteArrayContent(buffer);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            _httpService.Post(createTeacherUrl, content);
        }

        public void EditTeacher(TeachersDto teacher)
        {
            var _teacher = JsonConvert.SerializeObject(teacher);
            if (string.IsNullOrEmpty(_teacher)) throw new JsonSerializationException("Object teacher is empty");
            var buffer = Encoding.UTF8.GetBytes(_teacher);
            var content = new ByteArrayContent(buffer);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            _httpService.Put(editTeacherUrl, content);
        }

        public void DeleteTeacher(int idTeacher)
        {
            _httpService.Delete($"{deleteTeacherUrl}{idTeacher}");
        }

        public TeachersDto GetByUserId(int userId)
        {
            return JsonConvert.DeserializeObject<TeachersDto>(_httpService.GetItem($"{getTeacherByUserIdUrl}{userId}"));
        }

        public IList<TeachersDto> GetByDepartmentId(int departmentId)
        {
            return JsonConvert.DeserializeObject<List<TeachersDto>>(_httpService.GetList($"{getTeacherByDepratmentIdUrl}{departmentId}"));
        }

        public IList<TeachersDto> SearchTeacher(string search, string position, string department)
        {
            var lastName = string.Empty;
            var items = ParseSearchString(search);
            var firstName = items.First();
            if (items.Count == 2)
                lastName = items.Last();
            return JsonConvert.DeserializeObject<List<TeachersDto>>(_httpService.GetList(
                $"{searchTeacherUrl}{firstName}&secondName={lastName}&position={position}&department={department}"));
        }

        public IList<TeachersDto> GetByPosition(string position)
        {
            return JsonConvert.DeserializeObject<List<TeachersDto>>(_httpService.GetList($"{getTeacherByPositionUrl}{position}"));
        }

        public TeachersDto GetTeacherById(int idTeacher)
        {
            return JsonConvert.DeserializeObject<TeachersDto>(_httpService.GetItem($"{getTeacherByIdUrl}{idTeacher}"));
        }

        public IList<TeachersDto> GetAllTeachers()
        {
            return JsonConvert.DeserializeObject<List<TeachersDto>>(_httpService.GetList(getAllTeachersUrl));
        }
    }
}
