﻿using System.Web.Mvc;
using Entities.ModelsDTO;
using SharedLibrary.HttpServices;

namespace WebUI.Controllers
{
    public class SubjectController : Controller
    {
        private readonly SubjectHttpService _subjectService;

        public SubjectController()
        {
            _subjectService = new SubjectHttpService();
        }

        [HttpGet]
        public ActionResult CreateSubject()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult CreateSubject(SubjectsDto model)
        {
            if (ModelState.IsValid)
            {
                _subjectService.CreateSubject(model);
            }
            return RedirectToAction("Subjects","Subject");
        }

        [HttpGet]
        public ActionResult EditSubject(int id)
        {
            if (id > 0)
            {
                var subject = _subjectService.GetSubjectById(id);
                return PartialView(subject);
            }
            return RedirectToAction("Subjects", "Subject");
        }

        [HttpPost]
        public ActionResult EditSubject(SubjectsDto model)
        {
            if (ModelState.IsValid)
            {
                _subjectService.EditSubject(model);
            }
            return RedirectToAction("Subjects", "Subject");
        }

        [HttpGet]
        public ActionResult DeleteSubject(int id)
        {
            if (id > 0)
            {
                var subject = _subjectService.GetSubjectById(id);
                return PartialView(subject);
            }
            return RedirectToAction("Subjects", "Subject");
        }
        [HttpPost]
        public ActionResult DeleteSubjectById(int id)
        {
            if (id > 0)
            {
                _subjectService.DeleteSubject(id);
            }
            return RedirectToAction("Subjects", "Subject");
        }

        [HttpGet]
        public ActionResult Subjects(string name)
        {
            ViewBag.Subjects = _subjectService.GetAll();
            if (!string.IsNullOrEmpty(name))
            {
                ViewBag.Subjects = _subjectService.GetSubjectByName(name);
            }
            return View();
        }
    }
}