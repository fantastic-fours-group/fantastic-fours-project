﻿using System;
using System.Net;
using System.Web.Http;
using BusinessLogicLayer.Services;

namespace WebApi.Controllers
{
    public class GroupSearchController : ApiController
    {
        private readonly GroupService _groupService;

        public GroupSearchController()
        {
            _groupService = new GroupService();
        }

        // GET: api/GroupSearch/GetGroupByName?name=401
        [HttpGet]
        public IHttpActionResult GetGroupByName(string name)
        {
            try
            {
                var groups = _groupService.GetGroupByName(name);
                return Ok(groups);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // GET: api/GroupSearch/GetGroupByDepartmentId?departmentId=2
        [HttpGet]
        public IHttpActionResult GetGroupByDepartmentId(int departmentId)
        {
            try
            {
                var groups = _groupService.GetGroupByDepartmentId(departmentId);
                return Ok(groups);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // GET: api/GroupSearch/GetGroupByCourse?course=4
        [HttpGet]
        public IHttpActionResult GetGroupByCourse(string course)
        {
            try
            {
                var groups = _groupService.GetGroupByCourse(course);
                return Ok(groups);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
