﻿using System.Linq;
using System.Web.Mvc;
using Entities.ModelsDTO;
using SharedLibrary.HttpServices;

namespace WebUI.Controllers
{
    public class TeacherController : Controller
    {
        private readonly TeacherHttpService _teacherService;
        private readonly DepartmentHttpService _departmentService;
        private readonly UserHttpService _userService;

        public TeacherController()
        {
            _teacherService = new TeacherHttpService();
            _departmentService = new DepartmentHttpService();
            _userService = new UserHttpService();
        }

        [HttpGet]
        public ActionResult CreateTeacher()
        {
            ViewBag.Departments = _departmentService.GetAllDepartments();
            ViewBag.Users = _userService.GetAllUsers();
            return PartialView();
        }

        [HttpPost]
        public ActionResult CreateTeacher(TeachersDto model)
        {
            ViewBag.Success = false;
            if (ModelState.IsValid)
            {
                _teacherService.CreateTeacher(model);
                ViewBag.Success = true;
            }

            ViewBag.Departments = _departmentService.GetAllDepartments();
            ViewBag.Users = _userService.GetAllUsers();
            return RedirectToAction("Teachers","Teacher");
        }

        [HttpGet]
        public ActionResult EditTeacher(int id)
        {
            if (id > 0)
            {
                var teacher = _teacherService.GetTeacherById(id);
                ViewBag.Departments = _departmentService.GetAllDepartments();
                ViewBag.Users = _userService.GetAllUsers();
                return PartialView(teacher);
            }
            return RedirectToAction("Teachers", "Teacher");
        }

        [HttpPost]
        public ActionResult EditTeacher(TeachersDto model)
        {
            ViewBag.Success = false;
            if (ModelState.IsValid)
            {
                _teacherService.EditTeacher(model);
                ViewBag.Success = true;
            }
            ViewBag.Departments = _departmentService.GetAllDepartments();
            ViewBag.Users = _userService.GetAllUsers();
            return RedirectToAction("Teachers", "Teacher");
        }

        [HttpGet]
        public ActionResult DeleteTeacher(int id)
        {
            if (id > 0)
            {
                var teacher = _teacherService.GetTeacherById(id);
                ViewBag.Department = _departmentService.GetDepartmentById(teacher.IdDepartment);
                ViewBag.User = _userService.GetUserById(teacher.IdUser);
                return PartialView(teacher);
            }
            return RedirectToAction("Teachers","Teacher");
        }

        [HttpPost]
        public ActionResult DeleteTeacherById(int id)
        {
            if (id > 0)
            {
                _teacherService.DeleteTeacher(id);
            }
            return RedirectToAction("Teachers", "Teacher");
        }

        [HttpGet]
        public ActionResult Teachers()
        {
            ViewBag.Teachers = _teacherService.GetAllTeachers();
            ViewBag.Positions = _teacherService.GetAllTeachers().Select(teacher => teacher.Position).Distinct()
                .ToList();
            ViewBag.Departments = _departmentService.GetAllDepartments();
            return View();
        }

        [HttpPost]
        public ActionResult Teachers(string search, string position, int idDepartment)
        {
            ViewBag.Positions = _teacherService.GetAllTeachers().Select(teacher => teacher.Position).Distinct()
                .ToList();
            ViewBag.Departments = _departmentService.GetAllDepartments();
            if (!string.IsNullOrEmpty(search) && !string.IsNullOrEmpty(position) && idDepartment > 0)
            {
                var department = _departmentService.GetDepartmentById(idDepartment);
                ViewBag.Teachers = _teacherService.SearchTeacher(search, position, department.Name);
            }
            else if (string.IsNullOrEmpty(search) && !string.IsNullOrEmpty(position) && idDepartment > 0)
            {
                ViewBag.Teachers = _teacherService.GetByPosition(position)
                    .Where(teacher => teacher.IdDepartment.Equals(idDepartment)).ToList();
            }
            return View();
        }
    }
}