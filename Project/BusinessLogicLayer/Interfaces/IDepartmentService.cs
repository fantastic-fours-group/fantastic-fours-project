﻿using System.Collections.Generic;
using Entities.ModelsDTO;

namespace BusinessLogicLayer.Interfaces
{
    public interface IDepartmentService
    {
        void CreateDepartment(DepartmentsDto department);
        void EditDepartment(DepartmentsDto department);
        void DeleteDepartment(int idDepartment);
        IList<DepartmentsDto> GetDepartmentByName(string name);
        DepartmentsDto GetDepartmentById(int idDepartment);
        IList<DepartmentsDto> GetAllDepartments();
    }
}
