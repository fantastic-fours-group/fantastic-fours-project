﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Web.Http;
using BusinessLogicLayer.Services;
using Entities.ModelsDTO;

namespace WebApi.Controllers
{
    public class TransactionController : ApiController
    {
        private readonly TransactionService _transactionService;

        public TransactionController()
        {
            _transactionService = new TransactionService();
        }

        // GET: api/Transaction/GetAllTrancactions
        [HttpGet]
        public IHttpActionResult GetAllTransactions()
        {
            try
            {
                var transactions = _transactionService.GetAllTransactions();
                return Ok(transactions);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // GET: api/Transaction/GetTransactionById?transactionId=1
        [HttpGet]
        public IHttpActionResult GetTransactionById(int transactionId)
        {
            try
            {
                var transaction = _transactionService.GetTransactionsById(transactionId);
                return Ok(transaction);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // POST: api/Transaction/CreateTransaction
        [HttpPost]
        public IHttpActionResult CreateTransaction(TransactionsDto transaction)
        {
            try
            {
                _transactionService.CreateTransaction(transaction);
                return Content(HttpStatusCode.Created, transaction);
            }
            catch (ValidationException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // PUT: api/Transaction/EditTransaction
        [HttpPut]
        public IHttpActionResult EditTransaction(TransactionsDto transaction)
        {
            try
            {
                _transactionService.EditTransaction(transaction);
                return Content(HttpStatusCode.Created, transaction);
            }
            catch (ValidationException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // DELETE: api/Transaction/DeleteTransaction?transactionId=1
        [HttpDelete]
        public IHttpActionResult DeleteTransaction(int transactionId)
        {
            try
            {
                _transactionService.DeleteTransaction(transactionId);
                return Content(HttpStatusCode.OK, transactionId);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
