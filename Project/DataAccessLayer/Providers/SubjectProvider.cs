using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Mappers;
using DataAccessLayer.Predicates;
using Entities.ModelsDB;
using Entities.ModelsDTO;

namespace DataAccessLayer.Providers
{
    public class SubjectProvider : BaseProvider<Subjects>, ISubjectProvider
    {
	    private readonly SubjectMapper _subjectMapper;

	    public SubjectProvider()
	    {
		    _subjectMapper = new SubjectMapper();
	    }

        protected override DbSet<Subjects> GetDbSet()
        {
            return Context.Subjects;
        }

        public IList<SubjectsDto> GetSubjectByName(string name)
        {
            return GetDbSet().WithName(name).ToList().Select(subject =>_subjectMapper.MapFrom(subject)).ToList();

        }

	    public SubjectsDto GetSubjectById(int idSubject)
	    {
		    return _subjectMapper.MapFrom(GetDbSet().WithId(idSubject));
	    }
    }
}