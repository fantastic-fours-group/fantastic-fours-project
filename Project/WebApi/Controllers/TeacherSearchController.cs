﻿using System;
using System.Net;
using System.Web.Http;
using BusinessLogicLayer.Services;

namespace WebApi.Controllers
{
    public class TeacherSearchController : ApiController
    {
        private readonly TeacherService _teacherService;

        public TeacherSearchController()
        {
            _teacherService = new TeacherService();
        }

        // GET: api/TeacherSearch/GetTeacherByUserId?userId=2
        [HttpGet]
        public IHttpActionResult GetTeacherByUserId(int userId)
        {
            try
            {
                var teacher = _teacherService.GetByUserId(userId);
                return Ok(teacher);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // GET: api/TeacherSearch/GetTeacherByDepartmentId?departmentId=4
        [HttpGet]
        public IHttpActionResult GetTeacherByDepartmentId(int departmentId)
        {
            try
            {
                var teachers = _teacherService.GetByDepartmentId(departmentId);
                return Ok(teachers);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // Get: api/TeacherSearch/SearchTeacher?firstName=Igor&secondName=Scootar&position=Assistant&department=Applied Mathematics
        [HttpGet]
        public IHttpActionResult SearchTeacher(string firstName, string secondName, string position, string department)
        {
            try
            {
                var teachers = _teacherService.SearchTeacher(firstName, secondName, position, department);
                return Ok(teachers);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // Get: api/TeacherSearch/GetTeacherByPosition?position=Docent
        [HttpGet]
        public IHttpActionResult GetTeacherByPosition(string position)
        {
            try
            {
                var teachers = _teacherService.GetByPosition(position);
                return Ok(teachers);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

    }
}
