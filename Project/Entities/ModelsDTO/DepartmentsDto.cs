﻿using System.ComponentModel.DataAnnotations;

namespace Entities.ModelsDTO
{
    public class DepartmentsDto
    {
        [Required(ErrorMessage = "Id is a required field for entity Department")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is a required field for entity Department")]
        [StringLength(100, MinimumLength = 1, ErrorMessage = "Name size must be between 1 and 100 characters for entity Department")]
        public string Name { get; set; }
    }
}
