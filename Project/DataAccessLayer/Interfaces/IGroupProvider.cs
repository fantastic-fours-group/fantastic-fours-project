using System.Collections.Generic;
using Entities.ModelsDTO;

namespace DataAccessLayer.Interfaces
{
    public interface IGroupProvider
    {
        IList<GroupsDto> GetByDepartmentId(int departmentId);
        IList<GroupsDto> GetByName(string name);
        IList<GroupsDto> GetByCourse(string course);
        GroupsDto GetGroupById(int idGroup);
    }
}