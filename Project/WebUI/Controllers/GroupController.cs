﻿using System;
using System.Web.Mvc;
using Entities.ModelsDTO;
using SharedLibrary.HttpServices;

namespace WebUI.Controllers
{
    public class GroupController : Controller
    {
        private readonly GroupHttpService _groupService;
        private readonly DepartmentHttpService _departmentService;

        public GroupController()
        {
            _groupService = new GroupHttpService();
            _departmentService = new DepartmentHttpService();
        }

        [HttpGet]
        public ActionResult CreateGroup()
        {
            ViewBag.Groups = _groupService.GetAllGroups();
            ViewBag.Departments = _departmentService.GetAllDepartments();
            return PartialView();
        }

        [HttpPost]
        public ActionResult CreateGroup(GroupsDto group)
        {
            if (ModelState.IsValid)
            {
                _groupService.CreateGroup(group);
            }
            return RedirectToAction("Groups","Group");
        }

        [HttpGet]
        public ActionResult EditGroup(int id)
        {
            if (id > 0)
            {
                var group = _groupService.GetGroupById(id);
                ViewBag.Departments = _departmentService.GetAllDepartments();
                return PartialView(group);
            }
            return RedirectToAction("Groups", "Group");
        }

        [HttpPost]
        public ActionResult EditGroup(GroupsDto model)
        {
            if (ModelState.IsValid)
            {
                _groupService.EditGroup(model);
            }
            return RedirectToAction("Groups", "Group");
        }

        [HttpGet]
        public ActionResult DeleteGroup(int id)
        {
            if (id > 0)
            {
                var group = _groupService.GetGroupById(id);
                ViewBag.Department = _departmentService.GetDepartmentById(group.IdDepartment);
                return PartialView(group);
            }
            return RedirectToAction("Groups", "Group");
        }

        [HttpPost]
        public ActionResult DeleteGroupById(int id)
        {
           if (id > 0)
           { 
               _groupService.DeleteGroup(id);
           }
           return RedirectToAction("Groups", "Group");
        }

        [HttpGet]
        public ActionResult Groups(string name)
        {
            ViewBag.Groups = _groupService.GetAllGroups();
            ViewBag.Departments = _departmentService.GetAllDepartments();
            if (!String.IsNullOrEmpty(name))
            {
                ViewBag.Groups = _groupService.GetGroupByName(name);
            }
            return View();
        }
    }
}