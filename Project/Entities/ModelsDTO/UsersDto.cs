﻿using System.ComponentModel.DataAnnotations;
using Resourses.Enums;

namespace Entities.ModelsDTO
{
    public class UsersDto
    {
        [Required(ErrorMessage = "Id is a required field for entity User")]
        public int Id { get; set; }

        [Required(ErrorMessage = "UserEmail is a required field for entity User")]
        [StringLength(100, MinimumLength = 1, ErrorMessage = "Email size must be between 1 and 100 characters for entity User")]
        public string UserEmail { get; set; }

        [Required(ErrorMessage = "UserLogin is a required field for entity User")]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "Login size must be between 1 and 50 characters for entity User")]
        public string UserLogin { get; set; }

        [Required(ErrorMessage = "UserPassword is a required field for entity User")]
        [StringLength(300, MinimumLength = 1, ErrorMessage = "Password size must be between 1 and 50 characters for entity User")]
        public string UserPassword { get; set; }

        [Required(ErrorMessage = "UserRole is a required field for entity User")]
        public ScheduleEnums.UserRoles UserRole { get; set; }
    }

}
