﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Entities.ModelsDTO;
using Newtonsoft.Json;
using SharedLibrary.Interfaces;

namespace SharedLibrary.HttpServices
{
    public class SubjectHttpService:ISubjectHttpService
    {
        #region Rest API Urls
        private readonly string createSubjectUrl = "api/Subject/CreateSubject";
        private readonly string editSubjectUrl = "api/Subject/EditSubject";
        private readonly string deleteSubjectUrl = "api/Subject/DeleteSubject?subjectId=";
        private readonly string getAllSubjectsUrl = "api/Subject/GetAllSubjects";
        private readonly string getSubjectByIdUrl = "api/Subject/GetSubjectById?subjectId=";
        private readonly string getSubjectByNameUrl = "api/SubjectSearch/GetSubjectByName?name=";
        #endregion

        private readonly HttpService _httpService;

        public SubjectHttpService()
        {
            _httpService = new HttpService();
        }

        public void CreateSubject(SubjectsDto subject)
        {
            var _subject = JsonConvert.SerializeObject(subject);
            if (string.IsNullOrEmpty(_subject)) throw new JsonSerializationException("Object subject is empty");
            var buffer = Encoding.UTF8.GetBytes(_subject);
            var content = new ByteArrayContent(buffer);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            _httpService.Post(createSubjectUrl, content);
        }

        public void EditSubject(SubjectsDto subject)
        {
            var _subject = JsonConvert.SerializeObject(subject);
            if (string.IsNullOrEmpty(_subject)) throw new JsonSerializationException("Object subject is empty");
            var buffer = Encoding.UTF8.GetBytes(_subject);
            var content = new ByteArrayContent(buffer);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            _httpService.Put(editSubjectUrl, content);
        }

        public void DeleteSubject(int idSubject)
        {
            _httpService.Delete($"{deleteSubjectUrl}{idSubject}");
        }

        public List<SubjectsDto> GetSubjectByName(string name)
        {
            return JsonConvert.DeserializeObject<List<SubjectsDto>>(_httpService.GetList($"{getSubjectByNameUrl}{name}"));
        }

        public SubjectsDto GetSubjectById(int idSubject)
        {
            return JsonConvert.DeserializeObject<SubjectsDto>(_httpService.GetItem($"{getSubjectByIdUrl}{idSubject}"));
        }

        public IList<SubjectsDto> GetAll()
        {
            return JsonConvert.DeserializeObject<List<SubjectsDto>>(_httpService.GetList(getAllSubjectsUrl));
        }
    }
}
