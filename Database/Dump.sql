USE [Schedule]
GO
/****** Object:  Table [dbo].[Audiences]    Script Date: 21.12.2018 0:05:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Audiences](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[TypeAudience] [varchar](20) NULL,
	[Descriptions] [varchar](100) NULL,
 CONSTRAINT [PK_audience] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Departments]    Script Date: 21.12.2018 0:05:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Departments](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
 CONSTRAINT [PK_departmentID] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Groups]    Script Date: 21.12.2018 0:05:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Groups](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdDepartment] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Course] [nchar](1) NOT NULL,
 CONSTRAINT [PK_groupID] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Schedules]    Script Date: 21.12.2018 0:05:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Schedules](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdGroup] [int] NOT NULL,
	[IdTimeLesson] [int] NOT NULL,
	[DayOfTheWeek] [nchar](10) NOT NULL,
	[IdTransaction] [int] NOT NULL,
 CONSTRAINT [PK_sheduleID] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Students]    Script Date: 21.12.2018 0:05:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Students](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdUser] [int] NOT NULL,
	[IdGroup] [int] NOT NULL,
	[FirstName] [varchar](50) NOT NULL,
	[SecondName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_studentID] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Subjects]    Script Date: 21.12.2018 0:05:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Subjects](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
 CONSTRAINT [PK_subject] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Teachers]    Script Date: 21.12.2018 0:05:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Teachers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdUser] [int] NOT NULL,
	[IdDepartment] [int] NOT NULL,
	[FirstName] [varchar](50) NOT NULL,
	[SecondName] [varchar](50) NOT NULL,
	[Position] [nchar](25) NOT NULL,
 CONSTRAINT [PK_teacherID] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TimeLessons]    Script Date: 21.12.2018 0:05:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TimeLessons](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StartLessons] [time](7) NOT NULL,
	[FinishLessons] [time](7) NOT NULL,
 CONSTRAINT [PK_lesson] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Transactions]    Script Date: 21.12.2018 0:05:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Transactions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdTeacher] [int] NOT NULL,
	[IdSubject] [int] NOT NULL,
	[IdAudience] [int] NOT NULL,
	[TypeOfLesson] [nchar](20) NOT NULL,
	[NumberOfWeek] [varchar](10) NOT NULL,
 CONSTRAINT [PK_transactionID] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Users]    Script Date: 21.12.2018 0:05:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserEmail] [varchar](100) NOT NULL,
	[UserLogin] [varchar](50) NOT NULL,
	[UserPassword] [varchar](50) NOT NULL,
	[UserRole] [nchar](10) NOT NULL,
 CONSTRAINT [PK_userID] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Audiences] ON 

INSERT [dbo].[Audiences] ([Id], [Name], [TypeAudience], [Descriptions]) VALUES (1, N'3', N'Computer class', N'15 computers')
INSERT [dbo].[Audiences] ([Id], [Name], [TypeAudience], [Descriptions]) VALUES (2, N'15', N'Computer class', N'15 computers')
INSERT [dbo].[Audiences] ([Id], [Name], [TypeAudience], [Descriptions]) VALUES (3, N'18', N'Lecture class', N'15 places')
INSERT [dbo].[Audiences] ([Id], [Name], [TypeAudience], [Descriptions]) VALUES (4, N'19', N'Computer class', N'15 computers')
INSERT [dbo].[Audiences] ([Id], [Name], [TypeAudience], [Descriptions]) VALUES (5, N'21', N'Lecture class', N'30 places')
INSERT [dbo].[Audiences] ([Id], [Name], [TypeAudience], [Descriptions]) VALUES (6, N'24', N'Lecture class', N'15 places')
INSERT [dbo].[Audiences] ([Id], [Name], [TypeAudience], [Descriptions]) VALUES (7, N'31', N'Computer class', N'15 computers')
INSERT [dbo].[Audiences] ([Id], [Name], [TypeAudience], [Descriptions]) VALUES (8, N'33', N'Computer class', N'15 computers')
INSERT [dbo].[Audiences] ([Id], [Name], [TypeAudience], [Descriptions]) VALUES (9, N'39', N'Lecture class', N'16 places')
SET IDENTITY_INSERT [dbo].[Audiences] OFF
SET IDENTITY_INSERT [dbo].[Departments] ON 

INSERT [dbo].[Departments] ([Id], [Name]) VALUES (1, N'Algebra and computer science')
INSERT [dbo].[Departments] ([Id], [Name]) VALUES (4, N'Applied Mathematics')
INSERT [dbo].[Departments] ([Id], [Name]) VALUES (3, N'Differential equations')
INSERT [dbo].[Departments] ([Id], [Name]) VALUES (2, N'Mathematical analysis')
INSERT [dbo].[Departments] ([Id], [Name]) VALUES (5, N'Mathematical modeling')
SET IDENTITY_INSERT [dbo].[Departments] OFF
SET IDENTITY_INSERT [dbo].[Groups] ON 

INSERT [dbo].[Groups] ([Id], [IdDepartment], [Name], [Course]) VALUES (1, 5, N'401', N'4')
INSERT [dbo].[Groups] ([Id], [IdDepartment], [Name], [Course]) VALUES (2, 5, N'411', N'4')
INSERT [dbo].[Groups] ([Id], [IdDepartment], [Name], [Course]) VALUES (3, 4, N'402', N'4')
INSERT [dbo].[Groups] ([Id], [IdDepartment], [Name], [Course]) VALUES (4, 3, N'404', N'4')
INSERT [dbo].[Groups] ([Id], [IdDepartment], [Name], [Course]) VALUES (5, 2, N'405', N'4')
INSERT [dbo].[Groups] ([Id], [IdDepartment], [Name], [Course]) VALUES (6, 5, N'407', N'4')
SET IDENTITY_INSERT [dbo].[Groups] OFF
SET IDENTITY_INSERT [dbo].[Schedules] ON 

INSERT [dbo].[Schedules] ([Id], [IdGroup], [IdTimeLesson], [DayOfTheWeek], [IdTransaction]) VALUES (1, 1, 1, N'Monday    ', 1)
INSERT [dbo].[Schedules] ([Id], [IdGroup], [IdTimeLesson], [DayOfTheWeek], [IdTransaction]) VALUES (2, 2, 1, N'Monday    ', 1)
INSERT [dbo].[Schedules] ([Id], [IdGroup], [IdTimeLesson], [DayOfTheWeek], [IdTransaction]) VALUES (3, 2, 2, N'Monday    ', 2)
INSERT [dbo].[Schedules] ([Id], [IdGroup], [IdTimeLesson], [DayOfTheWeek], [IdTransaction]) VALUES (4, 1, 2, N'Monday    ', 3)
INSERT [dbo].[Schedules] ([Id], [IdGroup], [IdTimeLesson], [DayOfTheWeek], [IdTransaction]) VALUES (5, 1, 3, N'Monday    ', 5)
INSERT [dbo].[Schedules] ([Id], [IdGroup], [IdTimeLesson], [DayOfTheWeek], [IdTransaction]) VALUES (6, 2, 1, N'Tuesday   ', 4)
INSERT [dbo].[Schedules] ([Id], [IdGroup], [IdTimeLesson], [DayOfTheWeek], [IdTransaction]) VALUES (7, 2, 2, N'Tuesday   ', 6)
INSERT [dbo].[Schedules] ([Id], [IdGroup], [IdTimeLesson], [DayOfTheWeek], [IdTransaction]) VALUES (8, 1, 1, N'Wednesday ', 4)
INSERT [dbo].[Schedules] ([Id], [IdGroup], [IdTimeLesson], [DayOfTheWeek], [IdTransaction]) VALUES (9, 2, 1, N'Wednesday ', 8)
INSERT [dbo].[Schedules] ([Id], [IdGroup], [IdTimeLesson], [DayOfTheWeek], [IdTransaction]) VALUES (10, 2, 2, N'Wednesday ', 9)
INSERT [dbo].[Schedules] ([Id], [IdGroup], [IdTimeLesson], [DayOfTheWeek], [IdTransaction]) VALUES (11, 1, 1, N'Thursday  ', 7)
INSERT [dbo].[Schedules] ([Id], [IdGroup], [IdTimeLesson], [DayOfTheWeek], [IdTransaction]) VALUES (12, 2, 1, N'Thursday  ', 7)
INSERT [dbo].[Schedules] ([Id], [IdGroup], [IdTimeLesson], [DayOfTheWeek], [IdTransaction]) VALUES (13, 1, 2, N'Friday    ', 10)
INSERT [dbo].[Schedules] ([Id], [IdGroup], [IdTimeLesson], [DayOfTheWeek], [IdTransaction]) VALUES (14, 2, 2, N'Friday    ', 10)
INSERT [dbo].[Schedules] ([Id], [IdGroup], [IdTimeLesson], [DayOfTheWeek], [IdTransaction]) VALUES (15, 3, 2, N'Friday    ', 10)
INSERT [dbo].[Schedules] ([Id], [IdGroup], [IdTimeLesson], [DayOfTheWeek], [IdTransaction]) VALUES (16, 1, 2, N'Friday    ', 11)
INSERT [dbo].[Schedules] ([Id], [IdGroup], [IdTimeLesson], [DayOfTheWeek], [IdTransaction]) VALUES (17, 2, 2, N'Friday    ', 11)
INSERT [dbo].[Schedules] ([Id], [IdGroup], [IdTimeLesson], [DayOfTheWeek], [IdTransaction]) VALUES (18, 3, 2, N'Friday    ', 11)
SET IDENTITY_INSERT [dbo].[Schedules] OFF
SET IDENTITY_INSERT [dbo].[Students] ON 

INSERT [dbo].[Students] ([Id], [IdUser], [IdGroup], [FirstName], [SecondName]) VALUES (1, 7, 1, N'Alexey', N'Mitzkan')
INSERT [dbo].[Students] ([Id], [IdUser], [IdGroup], [FirstName], [SecondName]) VALUES (2, 8, 5, N'Mykhailo', N'Mytskan')
INSERT [dbo].[Students] ([Id], [IdUser], [IdGroup], [FirstName], [SecondName]) VALUES (3, 9, 3, N'Igor', N'Kosovich')
INSERT [dbo].[Students] ([Id], [IdUser], [IdGroup], [FirstName], [SecondName]) VALUES (4, 10, 6, N'Yaroslav', N'Vyshyvanyuk')
INSERT [dbo].[Students] ([Id], [IdUser], [IdGroup], [FirstName], [SecondName]) VALUES (5, 11, 5, N'Mykhailo', N'Lukan')
SET IDENTITY_INSERT [dbo].[Students] OFF
SET IDENTITY_INSERT [dbo].[Subjects] ON 

INSERT [dbo].[Subjects] ([Id], [Name]) VALUES (2, N'Designing programs.systems')
INSERT [dbo].[Subjects] ([Id], [Name]) VALUES (4, N'Information systems of accounting')
INSERT [dbo].[Subjects] ([Id], [Name]) VALUES (7, N'Methods of optimization and operations research')
INSERT [dbo].[Subjects] ([Id], [Name]) VALUES (5, N'Modern DBMS')
INSERT [dbo].[Subjects] ([Id], [Name]) VALUES (6, N'Platforms of corporate information systems')
INSERT [dbo].[Subjects] ([Id], [Name]) VALUES (3, N'Systems and methods of decision making')
INSERT [dbo].[Subjects] ([Id], [Name]) VALUES (1, N'Theory of programming')
SET IDENTITY_INSERT [dbo].[Subjects] OFF
SET IDENTITY_INSERT [dbo].[Teachers] ON 

INSERT [dbo].[Teachers] ([Id], [IdUser], [IdDepartment], [FirstName], [SecondName], [Position]) VALUES (1, 2, 5, N'Andrew', N'Pertsov', N'Assistant                ')
INSERT [dbo].[Teachers] ([Id], [IdUser], [IdDepartment], [FirstName], [SecondName], [Position]) VALUES (2, 3, 4, N'Natalia', N'Romanenko', N'Assistant                ')
INSERT [dbo].[Teachers] ([Id], [IdUser], [IdDepartment], [FirstName], [SecondName], [Position]) VALUES (3, 4, 4, N'Igor', N'Scootar', N'Assistant                ')
INSERT [dbo].[Teachers] ([Id], [IdUser], [IdDepartment], [FirstName], [SecondName], [Position]) VALUES (4, 5, 5, N'Vasyl', N'Kushnirchuk', N'Docent                   ')
INSERT [dbo].[Teachers] ([Id], [IdUser], [IdDepartment], [FirstName], [SecondName], [Position]) VALUES (5, 6, 5, N'Nikolai', N'Gorbatenko', N'Assistant                ')
SET IDENTITY_INSERT [dbo].[Teachers] OFF
SET IDENTITY_INSERT [dbo].[TimeLessons] ON 

INSERT [dbo].[TimeLessons] ([Id], [StartLessons], [FinishLessons]) VALUES (1, CAST(N'08:20:00' AS Time), CAST(N'09:40:00' AS Time))
INSERT [dbo].[TimeLessons] ([Id], [StartLessons], [FinishLessons]) VALUES (2, CAST(N'09:50:00' AS Time), CAST(N'11:10:00' AS Time))
INSERT [dbo].[TimeLessons] ([Id], [StartLessons], [FinishLessons]) VALUES (3, CAST(N'11:30:00' AS Time), CAST(N'12:50:00' AS Time))
INSERT [dbo].[TimeLessons] ([Id], [StartLessons], [FinishLessons]) VALUES (4, CAST(N'13:00:00' AS Time), CAST(N'14:20:00' AS Time))
INSERT [dbo].[TimeLessons] ([Id], [StartLessons], [FinishLessons]) VALUES (5, CAST(N'14:40:00' AS Time), CAST(N'16:00:00' AS Time))
INSERT [dbo].[TimeLessons] ([Id], [StartLessons], [FinishLessons]) VALUES (6, CAST(N'16:10:00' AS Time), CAST(N'17:30:00' AS Time))
INSERT [dbo].[TimeLessons] ([Id], [StartLessons], [FinishLessons]) VALUES (7, CAST(N'17:40:00' AS Time), CAST(N'19:00:00' AS Time))
INSERT [dbo].[TimeLessons] ([Id], [StartLessons], [FinishLessons]) VALUES (8, CAST(N'19:10:00' AS Time), CAST(N'20:30:00' AS Time))
SET IDENTITY_INSERT [dbo].[TimeLessons] OFF
SET IDENTITY_INSERT [dbo].[Transactions] ON 

INSERT [dbo].[Transactions] ([Id], [IdTeacher], [IdSubject], [IdAudience], [TypeOfLesson], [NumberOfWeek]) VALUES (1, 1, 1, 3, N'Lecture             ', N'First')
INSERT [dbo].[Transactions] ([Id], [IdTeacher], [IdSubject], [IdAudience], [TypeOfLesson], [NumberOfWeek]) VALUES (2, 2, 2, 4, N'Practice            ', N'First')
INSERT [dbo].[Transactions] ([Id], [IdTeacher], [IdSubject], [IdAudience], [TypeOfLesson], [NumberOfWeek]) VALUES (3, 3, 2, 2, N'Practice            ', N'First')
INSERT [dbo].[Transactions] ([Id], [IdTeacher], [IdSubject], [IdAudience], [TypeOfLesson], [NumberOfWeek]) VALUES (4, 1, 1, 4, N'Practice            ', N'All')
INSERT [dbo].[Transactions] ([Id], [IdTeacher], [IdSubject], [IdAudience], [TypeOfLesson], [NumberOfWeek]) VALUES (5, 4, 3, 1, N'Lecture             ', N'First')
INSERT [dbo].[Transactions] ([Id], [IdTeacher], [IdSubject], [IdAudience], [TypeOfLesson], [NumberOfWeek]) VALUES (6, 4, 3, 7, N'Practice            ', N'First')
INSERT [dbo].[Transactions] ([Id], [IdTeacher], [IdSubject], [IdAudience], [TypeOfLesson], [NumberOfWeek]) VALUES (7, 4, 7, 9, N'Lecture             ', N'Second')
INSERT [dbo].[Transactions] ([Id], [IdTeacher], [IdSubject], [IdAudience], [TypeOfLesson], [NumberOfWeek]) VALUES (8, 5, 6, 5, N'Lecture             ', N'First')
INSERT [dbo].[Transactions] ([Id], [IdTeacher], [IdSubject], [IdAudience], [TypeOfLesson], [NumberOfWeek]) VALUES (9, 5, 6, 9, N'Practice            ', N'All')
INSERT [dbo].[Transactions] ([Id], [IdTeacher], [IdSubject], [IdAudience], [TypeOfLesson], [NumberOfWeek]) VALUES (10, 2, 2, 8, N'Lecture             ', N'First')
INSERT [dbo].[Transactions] ([Id], [IdTeacher], [IdSubject], [IdAudience], [TypeOfLesson], [NumberOfWeek]) VALUES (11, 3, 2, 8, N'Lecture             ', N'Second')
SET IDENTITY_INSERT [dbo].[Transactions] OFF
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([Id], [UserEmail], [UserLogin], [UserPassword], [UserRole]) VALUES (1, N'admin@gmail.com', N'admin', N'1234', N'Admin     ')
INSERT [dbo].[Users] ([Id], [UserEmail], [UserLogin], [UserPassword], [UserRole]) VALUES (2, N'user1@gmail.com', N'User1', N'abc1234', N'Teacher   ')
INSERT [dbo].[Users] ([Id], [UserEmail], [UserLogin], [UserPassword], [UserRole]) VALUES (3, N'user2@gmail.com', N'User2', N'abc1234', N'Teacher   ')
INSERT [dbo].[Users] ([Id], [UserEmail], [UserLogin], [UserPassword], [UserRole]) VALUES (4, N'user3@gmail.com', N'User3', N'abc1234', N'Teacher   ')
INSERT [dbo].[Users] ([Id], [UserEmail], [UserLogin], [UserPassword], [UserRole]) VALUES (5, N'user4@gmail.com', N'User4', N'abc1234', N'Teacher   ')
INSERT [dbo].[Users] ([Id], [UserEmail], [UserLogin], [UserPassword], [UserRole]) VALUES (6, N'user5@gmail.com', N'User5', N'abc1234', N'Teacher   ')
INSERT [dbo].[Users] ([Id], [UserEmail], [UserLogin], [UserPassword], [UserRole]) VALUES (7, N'user6@gmail.com', N'User6', N'abc1234', N'Student   ')
INSERT [dbo].[Users] ([Id], [UserEmail], [UserLogin], [UserPassword], [UserRole]) VALUES (8, N'user7@gmail.com', N'User7', N'abc1234', N'Student   ')
INSERT [dbo].[Users] ([Id], [UserEmail], [UserLogin], [UserPassword], [UserRole]) VALUES (9, N'user8@gmail.com', N'User8', N'abc1234', N'Student   ')
INSERT [dbo].[Users] ([Id], [UserEmail], [UserLogin], [UserPassword], [UserRole]) VALUES (10, N'user9@gmail.com', N'User9', N'abc1234', N'Student   ')
INSERT [dbo].[Users] ([Id], [UserEmail], [UserLogin], [UserPassword], [UserRole]) VALUES (11, N'user10@gmail.com', N'User10', N'abc1234', N'Student   ')
INSERT [dbo].[Users] ([Id], [UserEmail], [UserLogin], [UserPassword], [UserRole]) VALUES (13, N'admin123@gmail.com', N'admin1123', N'550e1bafe077ff0b0b67f4e32f29d751', N'Admin     ')
SET IDENTITY_INSERT [dbo].[Users] OFF
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__Audience__737584F65041DCB3]    Script Date: 21.12.2018 0:05:29 ******/
ALTER TABLE [dbo].[Audiences] ADD UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__Departme__737584F67E11CA11]    Script Date: 21.12.2018 0:05:29 ******/
ALTER TABLE [dbo].[Departments] ADD UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__Subjects__737584F60FE11B7F]    Script Date: 21.12.2018 0:05:29 ******/
ALTER TABLE [dbo].[Subjects] ADD UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__Users__08638DF8C84A083B]    Script Date: 21.12.2018 0:05:29 ******/
ALTER TABLE [dbo].[Users] ADD UNIQUE NONCLUSTERED 
(
	[UserEmail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Groups]  WITH CHECK ADD  CONSTRAINT [FK_departmentID] FOREIGN KEY([IdDepartment])
REFERENCES [dbo].[Departments] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Groups] CHECK CONSTRAINT [FK_departmentID]
GO
ALTER TABLE [dbo].[Schedules]  WITH CHECK ADD  CONSTRAINT [fk_group] FOREIGN KEY([IdGroup])
REFERENCES [dbo].[Groups] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Schedules] CHECK CONSTRAINT [fk_group]
GO
ALTER TABLE [dbo].[Schedules]  WITH CHECK ADD  CONSTRAINT [fk_timelesson] FOREIGN KEY([IdTimeLesson])
REFERENCES [dbo].[TimeLessons] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Schedules] CHECK CONSTRAINT [fk_timelesson]
GO
ALTER TABLE [dbo].[Schedules]  WITH CHECK ADD  CONSTRAINT [fk_transactionId] FOREIGN KEY([IdTransaction])
REFERENCES [dbo].[Transactions] ([Id])
GO
ALTER TABLE [dbo].[Schedules] CHECK CONSTRAINT [fk_transactionId]
GO
ALTER TABLE [dbo].[Students]  WITH CHECK ADD  CONSTRAINT [FK_groupID] FOREIGN KEY([IdGroup])
REFERENCES [dbo].[Groups] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Students] CHECK CONSTRAINT [FK_groupID]
GO
ALTER TABLE [dbo].[Students]  WITH CHECK ADD  CONSTRAINT [FK_student] FOREIGN KEY([IdUser])
REFERENCES [dbo].[Users] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Students] CHECK CONSTRAINT [FK_student]
GO
ALTER TABLE [dbo].[Teachers]  WITH CHECK ADD  CONSTRAINT [FK_department] FOREIGN KEY([IdDepartment])
REFERENCES [dbo].[Departments] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Teachers] CHECK CONSTRAINT [FK_department]
GO
ALTER TABLE [dbo].[Teachers]  WITH CHECK ADD  CONSTRAINT [FK_teacher] FOREIGN KEY([IdUser])
REFERENCES [dbo].[Users] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Teachers] CHECK CONSTRAINT [FK_teacher]
GO
ALTER TABLE [dbo].[Transactions]  WITH CHECK ADD  CONSTRAINT [FK_audience] FOREIGN KEY([IdAudience])
REFERENCES [dbo].[Audiences] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Transactions] CHECK CONSTRAINT [FK_audience]
GO
ALTER TABLE [dbo].[Transactions]  WITH CHECK ADD  CONSTRAINT [fk_subject] FOREIGN KEY([IdSubject])
REFERENCES [dbo].[Subjects] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Transactions] CHECK CONSTRAINT [fk_subject]
GO
ALTER TABLE [dbo].[Transactions]  WITH CHECK ADD  CONSTRAINT [Teacher_fk] FOREIGN KEY([IdTeacher])
REFERENCES [dbo].[Teachers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Transactions] CHECK CONSTRAINT [Teacher_fk]
GO
ALTER TABLE [dbo].[Audiences]  WITH CHECK ADD CHECK  (([TypeAudience]='Computer class' OR [TypeAudience]='Lecture class'))
GO
ALTER TABLE [dbo].[Groups]  WITH CHECK ADD CHECK  (([Course]='1' OR [Course]='2' OR [Course]='3' OR [Course]='4' OR [Course]='5' OR [Course]='6'))
GO
ALTER TABLE [dbo].[Schedules]  WITH CHECK ADD CHECK  (([DayOfTheWeek]='Monday' OR [DayOfTheWeek]='Tuesday' OR [DayOfTheWeek]='Wednesday' OR [DayOfTheWeek]='Thursday' OR [DayOfTheWeek]='Friday' OR [DayOfTheWeek]='Saturday' OR [DayOfTheWeek]='Sunday'))
GO
ALTER TABLE [dbo].[Teachers]  WITH CHECK ADD CHECK  (([Position]='Professor' OR [Position]='Postgraduate' OR [Position]='Docent' OR [Position]='Assistant' OR [Position]='Senior Lecturer' OR [Position]='Trainee' OR [Position]='Teacher' OR [Position]='Doctoral student' OR [Position]='Researcher'))
GO
ALTER TABLE [dbo].[Transactions]  WITH CHECK ADD CHECK  (([NumberOfWeek]='First' OR [NumberOfWeek]='Second' OR [NumberOfWeek]='All'))
GO
ALTER TABLE [dbo].[Transactions]  WITH CHECK ADD CHECK  (([TypeOfLesson]='Lecture' OR [TypeOfLesson]='Practice' OR [TypeOfLesson]='Laboratory'))
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD CHECK  (([UserRole]='Student' OR [UserRole]='Admin' OR [UserRole]='Teacher'))
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [email_validator] CHECK  (([UserEmail] like '%___@___%.__%'))
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [email_validator]
GO
