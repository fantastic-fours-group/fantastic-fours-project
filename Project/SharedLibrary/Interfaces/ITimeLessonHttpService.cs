﻿using System;
using System.Collections.Generic;
using Entities.ModelsDTO;

namespace SharedLibrary.Interfaces
{
    public interface ITimeLessonHttpService
    {
        void CreateTimeLesson(TimeLessonsDto timeLesson);
        void EditTimeLesson(TimeLessonsDto timeLesson);
        void DeleteTimeLesson(int idTimeLesson);
        TimeLessonsDto SearchTimeLessons(TimeSpan start, TimeSpan finish);
        TimeLessonsDto GetTimeLessonsById(int idTimeLesson);
        IList<TimeLessonsDto> GetAllTimeLessons();
    }
}
