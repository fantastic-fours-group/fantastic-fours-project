﻿using System;

namespace Resourses.Attributes
{
    public class StringValue : Attribute
    {
        public string Value { get; }

        public StringValue(string value)
        {
            Value = value;
        }
    }
}
