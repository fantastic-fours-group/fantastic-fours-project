﻿using System;
using Resourses.Attributes;

namespace Resourses.Parsers
{
    public static class ScheduleParsers
    {
        public static string GetStringValue(this Enum value)
        {
            string stringValue = value.ToString();
            var type = value.GetType();
            var fieldInfo = type.GetField(value.ToString());
            var attrs = fieldInfo.
                GetCustomAttributes(typeof(StringValue), false) as StringValue[];
            if (attrs.Length > 0)
            {
                stringValue = attrs[0].Value;
            }
            return stringValue;
        }
    }
}
