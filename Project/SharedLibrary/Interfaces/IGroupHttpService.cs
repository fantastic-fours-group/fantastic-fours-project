﻿using Entities.ModelsDTO;
using System.Collections.Generic;

namespace SharedLibrary.Interfaces
{
    public interface IGroupHttpService
    {
        void CreateGroup(GroupsDto group);
        void EditGroup(GroupsDto group);
        void DeleteGroup(int idGroup);
        IList<GroupsDto> GetGroupByDepartmentId(int departmentId);
        List<GroupsDto> GetGroupByName(string name);
        IList<GroupsDto> GetGroupByCourse(string course);
        GroupsDto GetGroupById(int idGroup);
        IList<GroupsDto> GetAllGroups();
    }
}
