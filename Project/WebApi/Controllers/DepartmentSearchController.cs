﻿using System;
using System.Net;
using System.Web.Http;
using BusinessLogicLayer.Services;

namespace WebApi.Controllers
{
    public class DepartmentSearchController : ApiController
    {
        private readonly DepartmentService _departmentService;

        public DepartmentSearchController()
        {
            _departmentService = new DepartmentService();
        }

        // GET: api/DepartmentSearch/GetDepartmentByName?name=Differential equations
        [HttpGet]
        public IHttpActionResult GetDepartmentByName(string name)
        {
            try
            {
                var departments = _departmentService.GetDepartmentByName(name);
                return Ok(departments);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }
        
    }
}
