﻿using System.Security.Cryptography;

namespace BusinessLogicLayer.Interfaces
{
    public interface IHashingService
    {
        string GetHash(string value);
        bool VerifyHash(MD5 hash, string value, string hashValue);
    }
}
