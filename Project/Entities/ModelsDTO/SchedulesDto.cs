﻿using System.ComponentModel.DataAnnotations;
using Resourses.Enums;

namespace Entities.ModelsDTO
{
    public class SchedulesDto
    {
        [Required(ErrorMessage = "Id is a required field for entity Schedule")]
        public int Id { get; set; }

        [Required(ErrorMessage = "IdGroup is a required field for entity Schedule")]
        public int IdGroup { get; set; }

        [Required(ErrorMessage = "IdTimeLessons is a required field for entity Schedule")]
        public int IdTimeLessons { get; set; }

        [Required(ErrorMessage = "DayOfTheWeek is a required field for entity Schedule")]
        public ScheduleEnums.DayOfWeek DayOfTheWeek { get; set; }
        
        [Required(ErrorMessage = "IdTransaction is a required field for entity Schedule")]
        public int TransactionId { get; set; }
    }
}
