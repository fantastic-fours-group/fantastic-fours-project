﻿using System;
using Entities.ModelsDB;
using Entities.ModelsDTO;
using Resourses.Enums;
using Resourses.Parsers;

namespace DataAccessLayer.Mappers
{
    public class TransactionMapper
    {
        public TransactionsDto MapFrom(Transactions transactions)
        {
            return new TransactionsDto
            {
                Id = transactions.Id,
                IdTeacher = transactions.IdTeacher,
                IdSubject = transactions.IdSubject,
                IdAudience = transactions.IdAudience,
                TypeLessons = (ScheduleEnums.TypeOfLessons) Enum.Parse(typeof(ScheduleEnums.TypeOfLessons),
                    transactions.TypeOfLesson.Replace(" ", ""),
                    true),
                NumberOfWeek = (ScheduleEnums.NumberOfWeek) Enum.Parse(typeof(ScheduleEnums.NumberOfWeek),
                    transactions.NumberOfWeek.Replace(" ", ""),
                    true)
            };
        }

        public Transactions MapTo(TransactionsDto transactions)
        {
            return new Transactions
            {
                Id = transactions.Id,
                IdTeacher = transactions.IdTeacher,
                IdSubject = transactions.IdSubject,
                IdAudience = transactions.IdAudience,
                TypeOfLesson =  transactions.TypeLessons.GetStringValue(),
                NumberOfWeek = transactions.NumberOfWeek.GetStringValue()
            };
        }
    }
}
