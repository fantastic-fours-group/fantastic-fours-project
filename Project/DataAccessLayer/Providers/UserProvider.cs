﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Mappers;
using DataAccessLayer.Predicates;
using Entities.ModelsDB;
using Entities.ModelsDTO;

namespace DataAccessLayer.Providers
{
    public class UserProvider : BaseProvider<Users>, IUserProvider
    {
        private readonly UserMapper _userMapper;

        public UserProvider()
        {
            _userMapper = new UserMapper();
        }

        protected override DbSet<Users> GetDbSet()
        {
            return Context.Users;
        }

        public UsersDto GetByUserEmail(string userEmail)
        {
            return _userMapper.MapFrom(GetDbSet().WithUserEmail(userEmail));
        }

        public IList<UsersDto> GetByUserLogin(string userLogin)
        {
            return GetDbSet().WithUserLogin(userLogin).ToList().Select(user => _userMapper.MapFrom(user)).ToList();
        }

        public IList<UsersDto> GetByUserPassword(string userPassword)
        {
            return GetDbSet().WithUserPassword(userPassword).ToList().Select(user => _userMapper.MapFrom(user)).ToList();
        }

        public IList<UsersDto> GetByUserRole(string userRole)
        {
            return GetDbSet().WithUserRole(userRole).ToList().Select(user => _userMapper.MapFrom(user)).ToList();
        }

        public UsersDto GetUserById(int idUser)
        {
            return _userMapper.MapFrom(GetDbSet().WithId(idUser));
        }
    }
}
