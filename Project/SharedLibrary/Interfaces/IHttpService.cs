﻿using System.Net.Http;

namespace SharedLibrary.Interfaces
{
    public interface IHttpService
    {
        string Put(string urlResponce, HttpContent content);
        string Post(string urlResponce, HttpContent content);
        string Delete(string urlResponce);
        string GetItem(string urlResponce);
        string GetList(string urlResponce);

    }
}
