﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Entities.ModelsDTO;
using Newtonsoft.Json;
using SharedLibrary.Interfaces;

namespace SharedLibrary.HttpServices
{
    public class UserHttpService:IUserHttpService
    {
        #region Rest API Urls
        private readonly string createUserUrl = "api/User/CreateUser";
        private readonly string editUserUrl = "api/User/EditUser";
        private readonly string deleteUserUrl = "api/User/DeleteUser?userId=";
        private readonly string getAllUsersUrl = "api/User/GetAllUsers";
        private readonly string getUserByIdUrl = "api/User/GetUserById?userId=";
        private readonly string getUserByEmailUrl = "api/UserSearch/GetUserByEmail?userEmail=";
        private readonly string getUserByCredentialsUrl = "api/UserSearch/GetUserByCredentials?userLogin=";
        private readonly string getUserByRoleUrl = "api/UserSearch/GetUserByRole?userRole=";
        private readonly string updateUserPasswordUrl = "api/UserSearch/UpdateUserPassword?userLogin=";
        #endregion

        private readonly HttpService _httpService;

        public UserHttpService()
        {
            _httpService = new HttpService();
        }

        public void CreateUser(UsersDto user)
        {
            var _user = JsonConvert.SerializeObject(user);
            if (string.IsNullOrEmpty(_user)) throw new JsonSerializationException("Object user is empty");
            var buffer = Encoding.UTF8.GetBytes(_user);
            var content = new ByteArrayContent(buffer);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            _httpService.Post(createUserUrl, content);
        }

        public void EditUser(UsersDto user)
        {
            var _user = JsonConvert.SerializeObject(user);
            if (string.IsNullOrEmpty(_user)) throw new JsonSerializationException("Object user is empty");
            var buffer = Encoding.UTF8.GetBytes(_user);
            var content = new ByteArrayContent(buffer);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            _httpService.Put(editUserUrl, content);
        }

        public void DeleteUser(int idUser)
        {
            _httpService.Delete($"{deleteUserUrl}{idUser}");
        }

        public UsersDto GetUserByEmail(string userEmail)
        {
            return JsonConvert.DeserializeObject<UsersDto>(_httpService.GetItem($"{getUserByEmailUrl}{userEmail}"));
        }

        public UsersDto GetUserByCredentials(string userLogin, string userPassword)
        {
            return JsonConvert.DeserializeObject<UsersDto>(
                _httpService.GetItem($"{getUserByCredentialsUrl}{userLogin}&userPassword={userPassword}"));
        }

        public void UpdateUserPassword(string userLogin, string oldPassword, string newPassword)
        {
            JsonConvert.DeserializeObject<UsersDto>(
                _httpService.GetItem(
                    $"{updateUserPasswordUrl}{userLogin}&oldPassword={oldPassword}&newPassword={newPassword}"));
        }

        public IList<UsersDto> GetUserByRole(string userRole)
        {
            return JsonConvert.DeserializeObject<List<UsersDto>>(_httpService.GetList($"{getUserByRoleUrl}{userRole}"));
        }

        public UsersDto GetUserById(int idUser)
        {
            return JsonConvert.DeserializeObject<UsersDto>(_httpService.GetItem($"{getUserByIdUrl}{idUser}"));
        }

        public IList<UsersDto> GetAllUsers()
        {
            return JsonConvert.DeserializeObject<List<UsersDto>>(_httpService.GetList(getAllUsersUrl));
        }
    }
}
