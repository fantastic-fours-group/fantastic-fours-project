﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Web.Http;
using BusinessLogicLayer.Services;
using Entities.ModelsDTO;

namespace WebApi.Controllers
{
    public class AudienceController : ApiController
    {
        private readonly AudienceService _audienceService;

        public AudienceController()
        {
            _audienceService = new AudienceService();
        }

        // GET: api/Audience/GetAllAudiences
        [HttpGet]
        public IHttpActionResult GetAllAudiences()
        {
            try
            {
                var audiences = _audienceService.GetAllAudienceses();
                return Ok(audiences);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // GET: api/Audience/GetAudienceById?audienceId=2
        [HttpGet]
        public IHttpActionResult GetAudienceById(int audienceId)
        {
            try
            {
                var audience = _audienceService.GetAudienceById(audienceId);
                return Ok(audience);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // POST: api/Audience/CreateAudience
        [HttpPost]
        public IHttpActionResult CreateAudience(AudiencesDto audience)
        {
            try
            {
                _audienceService.CreateAudience(audience);
                return Content(HttpStatusCode.Created, audience);
            }
            catch (ValidationException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // PUT: api/Audience/EditAudience
        [HttpPut]
        public IHttpActionResult EditAudience(AudiencesDto audience)
        {
            try
            {
                _audienceService.EditAudience(audience);
                return Content(HttpStatusCode.Created, audience);
            }
            catch (ValidationException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // DELETE: api/Audience/DeleteAudience?audienceId=1
        [HttpDelete]
        public IHttpActionResult DeleteAudience(int audienceId)
        {
            try
            {
                _audienceService.DeleteAudience(audienceId);
                return Content(HttpStatusCode.OK, audienceId);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
