﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Validators;
using DataAccessLayer.Mappers;
using DataAccessLayer.Providers;
using Entities.ModelsDTO;

namespace BusinessLogicLayer.Services
{
    public class GroupService:IGroupService
    {
        private readonly GroupMapper _groupMapper;
        private readonly GroupProvider _groupProvider;
        private readonly AttributeValidator _validator;

        public GroupService()
        {
            _groupMapper = new GroupMapper();
            _groupProvider = new GroupProvider();
            _validator = new AttributeValidator();
        }

        private void ValidateId(int id)
        {
            if (id < 1)
                throw new ArgumentException("Id must be positive number");
            if (id <= 0) throw new ArgumentOutOfRangeException(nameof(id));
        }

        public void CreateGroup(GroupsDto @group)
        {
            _validator.Validate(group);
            _groupProvider.Create(_groupMapper.MapTo(group));
        }

        public void EditGroup(GroupsDto @group)
        {
            _validator.Validate(group);
            _groupProvider.Update(_groupMapper.MapTo(group));
        }

        public void DeleteGroup(int idGroup)
        {
            ValidateId(idGroup);
            _groupProvider.Delete(_groupMapper.MapTo(GetGroupById(idGroup)));
        }

        public IList<GroupsDto> GetGroupByDepartmentId(int departmentId)
        {
            ValidateId(departmentId);
            return _groupProvider.GetByDepartmentId(departmentId);
        }

        public IList<GroupsDto> GetGroupByName(string name)
        {
            return _groupProvider.GetByName(name);
        }

        public IList<GroupsDto> GetGroupByCourse(string course)
        {
            return _groupProvider.GetByCourse(course);
        }

        public GroupsDto GetGroupById(int idGroup)
        {
            ValidateId(idGroup);
            return _groupProvider.GetGroupById(idGroup);
        }

        public IList<GroupsDto> GetAll()
        {
            return _groupProvider.GetAll().ToList().Select(@group => _groupMapper.MapFrom(@group)).ToList();
        }
    }
}
