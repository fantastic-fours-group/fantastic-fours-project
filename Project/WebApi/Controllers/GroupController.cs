﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Web.Http;
using BusinessLogicLayer.Services;
using Entities.ModelsDTO;

namespace WebApi.Controllers
{
    public class GroupController : ApiController
    {
        private readonly GroupService _groupService;

        public GroupController()
        {
            _groupService = new GroupService();
        }

        // GET: api/Group/GetAllGroups
        [HttpGet]
        public IHttpActionResult GetAllGroups()
        {
            try
            {
                var groups = _groupService.GetAll();
                return Ok(groups);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // GET: api/Group/GetGroupById?groupId=1
        [HttpGet]
        public IHttpActionResult GetGroupById(int groupId)
        {
            try
            {
                var group = _groupService.GetGroupById(groupId);
                return Ok(group);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // POST: api/Group/CreateGroup
        [HttpPost]
        public IHttpActionResult CreateGroup(GroupsDto group)
        {
            try
            {
                _groupService.CreateGroup(group);
                return Content(HttpStatusCode.Created, group);
            }
            catch (ValidationException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // PUT: api/Group/EditGroup
        [HttpPut]
        public IHttpActionResult EditGroup(GroupsDto group)
        {
            try
            {
                _groupService.EditGroup(group);
                return Content(HttpStatusCode.Created, group);
            }
            catch (ValidationException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // DELETE: api/Group/DeleteGroup?groupId=7
        [HttpDelete]
        public IHttpActionResult DeleteGroup(int groupId)
        {
            try
            {
                _groupService.DeleteGroup(groupId);
                return Content(HttpStatusCode.OK, groupId);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
