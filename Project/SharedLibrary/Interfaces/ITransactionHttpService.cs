﻿using System.Collections.Generic;
using Entities.ModelsDTO;

namespace SharedLibrary.Interfaces
{
    public interface ITransactionHttpService
    {
        void CreateTransaction(TransactionsDto transaction);
        void EditTransaction(TransactionsDto transaction);
        void DeleteTransaction(int idTransaction);
        IList<TransactionsDto> GetByTeacherId(int teacherId);
        IList<TransactionsDto> GetBySubjectId(int subjectId);
        IList<TransactionsDto> GetByAudienceId(int audienceId);
        IList<TransactionsDto> GetByTypeOfLesson(string typeOfLesson);
        IList<TransactionsDto> GetByNumberOfWeek(string numberOfWeek);
        TransactionsDto GetTransactionsById(int idTransaction);
        IList<TransactionsDto> GetAllTransactions();
    }
}
