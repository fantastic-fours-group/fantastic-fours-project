using System.Collections.Generic;
using Entities.ModelsDTO;

namespace DataAccessLayer.Interfaces
{
    public interface IDepartmentProvider
    {
        IList<DepartmentsDto> GetDepartmentByName(string name);
        DepartmentsDto GetDepartmentById(int idDepartment);
    }
}