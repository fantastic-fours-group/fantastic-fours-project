﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Validators;
using DataAccessLayer.Mappers;
using DataAccessLayer.Providers;
using Entities.ModelsDTO;

namespace BusinessLogicLayer.Services
{
    public class SubjectService:ISubjectService
    {
        private readonly SubjectMapper _subjectMapper;
        private readonly SubjectProvider _subjectProvider;
        private readonly AttributeValidator _validator;

        public SubjectService()
        {
            _subjectMapper = new SubjectMapper();
            _subjectProvider = new SubjectProvider();
            _validator = new AttributeValidator();
        }

        private void ValidateId(int id)
        {
            if (id < 1)
                throw new ArgumentException("Id must be positive number");
            if (id <= 0) throw new ArgumentOutOfRangeException(nameof(id));
        }

        public void CreateSubject(SubjectsDto subject)
        {
            _validator.Validate(subject);
            _subjectProvider.Create(_subjectMapper.MapTo(subject));
        }

        public void EditSubject(SubjectsDto subject)
        {
            _validator.Validate(subject);
            _subjectProvider.Update(_subjectMapper.MapTo(subject));
        }

        public void DeleteSubject(int idSubject)
        {
            ValidateId(idSubject);
            _subjectProvider.Delete(_subjectMapper.MapTo(GetSubjectById(idSubject)));
        }

        public IList<SubjectsDto> GetSubjectByName(string name)
        {
            return _subjectProvider.GetSubjectByName(name);
        }

        public SubjectsDto GetSubjectById(int idSubject)
        {
            ValidateId(idSubject);
            return _subjectProvider.GetSubjectById(idSubject);
        }

        public IList<SubjectsDto> GetAll()
        {
            return _subjectProvider.GetAll().ToList().Select(subject => _subjectMapper.MapFrom(subject)).ToList();
        }
    }
}
