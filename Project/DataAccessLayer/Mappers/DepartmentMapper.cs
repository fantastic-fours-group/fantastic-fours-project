﻿using Entities.ModelsDB;
using Entities.ModelsDTO;

namespace DataAccessLayer.Mappers
{
    public class DepartmentMapper
    {
        public DepartmentsDto MapFrom(Departments departments)
        {
            return new DepartmentsDto
            {
                Id = departments.Id,
                Name = departments.Name
            };
        }
        public Departments MapTo(DepartmentsDto departments)
        {
            return new Departments
            {
                Id = departments.Id,
                Name = departments.Name
            };
        }
    }
}
