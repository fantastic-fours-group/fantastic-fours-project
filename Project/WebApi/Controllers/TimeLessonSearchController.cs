﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BusinessLogicLayer.Services;

namespace WebApi.Controllers
{
    public class TimeLessonSearchController : ApiController
    {
        private readonly TimeLessonService _timeLessonService;

        public TimeLessonSearchController()
        {
            _timeLessonService = new TimeLessonService();
        }

        // GET: api/TimeLessonSearch/SearchTimeLesson?start=10:50&finish=11:30
        [HttpGet]
        public IHttpActionResult SearchTimeLesson(TimeSpan start, TimeSpan finish)
        {
            try
            {
                var timeLesson = _timeLessonService.SearchTimeLessons(start, finish);
                return Ok(timeLesson);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
