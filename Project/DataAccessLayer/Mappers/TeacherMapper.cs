﻿using System;
using Entities.ModelsDB;
using Entities.ModelsDTO;
using Resourses.Enums;
using Resourses.Parsers;

namespace DataAccessLayer.Mappers
{
    public class TeacherMapper
    {
        public TeachersDto MapFrom(Teachers teachers)
        {
            return new TeachersDto
            {
                Id = teachers.Id,
                IdUser = teachers.IdUser,
                IdDepartment = teachers.IdDepartment,
                FirstName = teachers.FirstName,
                SecondName = teachers.SecondName,
                Position = (ScheduleEnums.TeacherPosition) Enum.Parse(typeof(ScheduleEnums.TeacherPosition),
                    teachers.Position.Replace(" ", ""),
                    true)
            };
        }

        public Teachers MapTo(TeachersDto teachers)
        {
            return new Teachers
            {
                Id = teachers.Id,
                IdUser = teachers.IdUser,
                IdDepartment = teachers.IdDepartment,
                FirstName = teachers.FirstName,
                SecondName = teachers.SecondName,
                Position = teachers.Position.GetStringValue()
            };
        }
    }
}
