﻿using System.ComponentModel.DataAnnotations;

namespace Entities.ModelsDTO
{
    public class SubjectsDto
    {
        [Required(ErrorMessage = "Id is a required field for entity Subject")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is a required field for entity Subject")]
        [StringLength(100, MinimumLength = 1, ErrorMessage = "Name size must be between 1 and 100 characters for entity Subject")]
        public string Name { get; set; }
    }
}
