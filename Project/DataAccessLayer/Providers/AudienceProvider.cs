using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Mappers;
using DataAccessLayer.Predicates;
using Entities.ModelsDB;
using Entities.ModelsDTO;

namespace DataAccessLayer.Providers
{
    public class AudienceProvider : BaseProvider<Audiences>, IAudienceProvider
    {
        private readonly AudienceMapper _audienceMapper;

        public AudienceProvider()
        {
            _audienceMapper = new AudienceMapper();
        }

        protected override DbSet<Audiences> GetDbSet()
        {
            return Context.Audiences;
        }

        public IList<AudiencesDto> GetAudienceByName(string name)
        {
            return GetDbSet().WithName(name).ToList().Select(audience => _audienceMapper.MapFrom(audience)).ToList();
        }

        public IList<AudiencesDto> GetByAudienceType(string audienceType)
        {
            return GetDbSet().WithTypeOfAudience(audienceType).ToList().Select(audience => _audienceMapper.MapFrom(audience)).ToList();
        }

        public IList<AudiencesDto> GetByDescription(string description)
        {
            return GetDbSet().WithDescription(description).ToList().Select(audience => _audienceMapper.MapFrom(audience)).ToList();
        }

        public AudiencesDto GetById(int audienceId)
        {
            return _audienceMapper.MapFrom(GetDbSet().WithId(audienceId));
        }
    }
}