﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Entities.ModelsDTO;
using SharedLibrary.HttpServices;

namespace WebUI.Controllers
{
    public class AudienceController : Controller
    {
        private readonly AudienceHttpService _audienceService;

        public AudienceController()
        {
            _audienceService = new AudienceHttpService();
        }

        [HttpGet]
        public ActionResult CreateAudience()
        {
           return PartialView();
        }

        [HttpPost]
        public ActionResult CreateAudience(AudiencesDto model)
        {
            ViewBag.Success = false;
            if (ModelState.IsValid)
            {
                _audienceService.CreateAudience(model);
                ViewBag.Success = true;
            }
            return PartialView(model);
        }

        [HttpGet]
        public ActionResult EditAudience(int id)
        {
            if (id > 0)
            {
                var audience = _audienceService.GetAudienceById(id);
                return PartialView(audience);
            }
            return RedirectToAction("Audiences", "Audience");
        }

        [HttpPost]
        public ActionResult EditAudience(AudiencesDto model)
        {
            if (ModelState.IsValid)
            {
                _audienceService.EditAudience(model);
            }
            return RedirectToAction("Audiences","Audience");
        }

        [HttpGet]
        public ActionResult DeleteAudience(int id)
        {
            if (id > 0)
            {
                var audience = _audienceService.GetAudienceById(id);
                return PartialView(audience);
            }
            return RedirectToAction("Audiences", "Audience");
        }

        [HttpPost]
        public ActionResult DeleteAudienceById(int id)
        {
            if (id > 0)
            {
                _audienceService.DeleteAudience(id);
            }
            return RedirectToAction("Audiences","Audience");
        }

        [HttpGet]
        public ActionResult Audiences(string name)
        {
            ViewBag.Audiences = _audienceService.GetAllAudienceses();
            if (!String.IsNullOrEmpty(name))
            {
                ViewBag.Audiences = _audienceService.GetAudienceByName(name);
            }
            return View();
        }
    }
}