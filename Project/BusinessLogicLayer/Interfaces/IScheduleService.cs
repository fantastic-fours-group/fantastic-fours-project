﻿using System.Collections.Generic;
using Entities.ModelsDTO;

namespace BusinessLogicLayer.Interfaces
{
    public interface IScheduleService
    {
        void CreateSchedule(SchedulesDto schedule);
        void EditSchedule(SchedulesDto schedule);
        void DeleteSchedule(int idSchedule);
        IList<SchedulesDto> GetByGroupId(int groupId);
        IList<SchedulesDto> GetByTimeLessonId(int timeLessonId);
        IList<SchedulesDto> GetByDayOfTheWeek(string dayOfTheWeek);
        IList<SchedulesDto> GetByTransactionId(int transactionId);
        SchedulesDto GetScheduleById(int idSchedule);
        IList<SchedulesDto> GetAll();
    }
}
