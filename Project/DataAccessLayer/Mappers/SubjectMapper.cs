﻿using Entities.ModelsDB;
using Entities.ModelsDTO;

namespace DataAccessLayer.Mappers
{
    public class SubjectMapper
    {
        public SubjectsDto MapFrom(Subjects subjects)
        {
            return new SubjectsDto
            {
                Id = subjects.Id,
                Name = subjects.Name
            };
        }
        
        public Subjects MapTo(SubjectsDto subjects)
        {
            return new Subjects
            {
                Id = subjects.Id,
                Name = subjects.Name
            };
        }
    }
}
