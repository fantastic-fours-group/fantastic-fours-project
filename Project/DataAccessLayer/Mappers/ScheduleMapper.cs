﻿using System;
using Entities.ModelsDB;
using Entities.ModelsDTO;
using Resourses.Enums;
using Resourses.Parsers;

namespace DataAccessLayer.Mappers
{
    public class ScheduleMapper
    {
        public SchedulesDto MapFrom(Schedules schedules)
        {
            return new SchedulesDto
            {
                Id = schedules.Id,
                IdGroup = schedules.IdGroup,
                IdTimeLessons = schedules.IdTimeLesson,
                DayOfTheWeek = (ScheduleEnums.DayOfWeek) Enum.Parse(typeof(ScheduleEnums.DayOfWeek),
                    schedules.DayOfTheWeek.Replace(" ", ""),
                    true),
                TransactionId = schedules.IdTransaction
            };
        }

        public Schedules MapTo(SchedulesDto schedules)
        {
            return new Schedules
            {
                Id = schedules.Id,
                IdGroup = schedules.IdGroup,
                IdTimeLesson = schedules.IdTimeLessons,
                DayOfTheWeek =  schedules.DayOfTheWeek.GetStringValue(),
                IdTransaction = schedules.TransactionId
            };
        }
    }
}
