using System.Linq;
using Entities.ModelsDB;

namespace DataAccessLayer.Predicates
{
    internal static class AudiencesPredicates
    {
        public static Audiences WithId(this IQueryable<Audiences> audiences , int id)
        {
            return audiences.First(a => a.Id == id);
        }

        public static IQueryable<Audiences> WithName(this IQueryable<Audiences> audiences, string name)
        {
            return audiences.Where(a => a.Name.Contains(name));
        }
        
        public static IQueryable<Audiences> WithTypeOfAudience(this IQueryable<Audiences> audiences, string audienceType)
        {
            return audiences.Where(a => a.TypeAudience == audienceType);
        }
        
        public static IQueryable<Audiences> WithDescription(this IQueryable<Audiences> audiences, string description)
        {
            return audiences.Where(a => a.Descriptions == description);
        }
    }
}