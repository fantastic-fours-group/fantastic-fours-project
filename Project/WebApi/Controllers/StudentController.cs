﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Web.Http;
using BusinessLogicLayer.Services;
using Entities.ModelsDTO;

namespace WebApi.Controllers
{
    public class StudentController : ApiController
    {
        private readonly StudentService _studentService;

        public StudentController()
        {
            _studentService = new StudentService();
        }

        // GET api/Student/GetAllStudents
        [HttpGet]
        public IHttpActionResult GetAllStudents()
        {
            try
            {
                var students = _studentService.GetAll();
                return Ok(students);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // GET: api/Student/GetByUserId?userId=1
        [HttpGet]
        public IHttpActionResult GetByUserId(int userId)
        {
            try
            {
                var student = _studentService.GetByUserId(userId);
                return Ok(student);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // POST api/Student/CreateStudent
        [HttpPost]
        public IHttpActionResult CreateStudent(StudentsDto student)
        {
            try
            {
                _studentService.CreateStudent(student);
                return Content(HttpStatusCode.Created, student);
            }
            catch (ValidationException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // PUT api/Student/EditStudent
        [HttpPut]
        public IHttpActionResult EditStudent(StudentsDto student)
        {
            try
            {
                _studentService.EditStudent(student);
                return Content(HttpStatusCode.Created, student);
            }
            catch (ValidationException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // DELETE api/Student/DeleteStudent?studentId=1
        [HttpDelete]
        public IHttpActionResult DeleteStudent(int studentId)
        {
            try
            {
                _studentService.DeleteStudent(studentId);
                return Content(HttpStatusCode.OK, studentId);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}