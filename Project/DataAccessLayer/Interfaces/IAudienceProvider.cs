using System.Collections.Generic;
using Entities.ModelsDTO;

namespace DataAccessLayer.Interfaces
{
    public interface IAudienceProvider
    {
        IList<AudiencesDto> GetAudienceByName(string name);
        IList<AudiencesDto> GetByAudienceType(string audienceType);
        IList<AudiencesDto> GetByDescription(string description);
        AudiencesDto GetById(int audienceId);
    }
}