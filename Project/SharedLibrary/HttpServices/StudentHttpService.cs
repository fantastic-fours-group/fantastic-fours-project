﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Entities.ModelsDTO;
using Newtonsoft.Json;
using SharedLibrary.Interfaces;

namespace SharedLibrary.HttpServices
{
    public class StudentHttpService : IStudentHttpService
    {
        #region Rest API Url
        private readonly string createStudentUrl = "api/Student/CreateStudent";
        private readonly string editStudentUrl = "api/Student/EditStudent";
        private readonly string deleteStudentUrl = "api/Student/DeleteStudent?studentId=";
        private readonly string getAllStudentsUrl = "api/Student/GetAllStudents";
        private readonly string getStudentByUserIdUrl = "api/Student/GetByUserId?userId=";
        private readonly string getStudentByGroupIdUrl = "api/StudentSearch/GetByGroupId?groupId=";
        private readonly string searchStudentUrl = "api/StudentSearch/SearchStudent?firstName=";
        private readonly string getStudentByIdUrl = "api/StudentSearch/GetStudentById?studentId=";
        #endregion

        private readonly HttpService _httpService;

        public StudentHttpService()
        {
            _httpService = new HttpService();
        }

        private List<string> ParseSearchString(string search)
        {
            var result = new List<string>();
            var sep = new[] { ' ' };
            string[] splited = search.Split(sep);
            if (search.Trim(sep).IndexOfAny(sep) < 0)
                result = new List<string> {splited.First()};
            else if (splited.Length == 2)
            {
                result = new List<string> {splited.First(), splited.Last()};
            }
            else if (splited.Length == 3)
            {
                result = new List<string> {splited.First(), splited[1], splited.Last()};
            }
            return result;
        }
        
        public void CreateStudent(StudentsDto student)
        {
            var _student = JsonConvert.SerializeObject(student);
            if (string.IsNullOrEmpty(_student)) throw new JsonSerializationException("Object student is empty");
            var buffer = Encoding.UTF8.GetBytes(_student);
            var content = new ByteArrayContent(buffer);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            _httpService.Post(createStudentUrl, content);
        }

        public void EditStudent(StudentsDto student)
        {
            var _student = JsonConvert.SerializeObject(student);
            if (string.IsNullOrEmpty(_student)) throw new JsonSerializationException("Object student is empty");
            var buffer = Encoding.UTF8.GetBytes(_student);
            var content = new ByteArrayContent(buffer);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            _httpService.Put(editStudentUrl, content);
        }

        public void DeleteStudent(int idStudent)
        {
            _httpService.Delete($"{deleteStudentUrl}{idStudent}");
        }

        public StudentsDto GetByUserId(int userId)
        {
            return JsonConvert.DeserializeObject<StudentsDto>(_httpService.GetItem($"{getStudentByUserIdUrl}{userId}"));
        }

        public IList<StudentsDto> GetByGroupId(int groupId)
        {
            return JsonConvert.DeserializeObject<List<StudentsDto>>(_httpService.GetList($"{getStudentByGroupIdUrl}{groupId}"));
        }

        public IList<StudentsDto> SearchStudent(string search)
        {
            var lastName = string.Empty;
            var group = string.Empty;
            var items = ParseSearchString(search);
            var firstName = items.First();
            if (items.Count == 2 || items.Count == 3)
                lastName = items[1];
            if (items.Count == 3)
                group = items.Last();
            return JsonConvert.DeserializeObject<List<StudentsDto>>(
                _httpService.GetList($"{searchStudentUrl}{firstName}&lastName={lastName}&group={group}"));
        }

        public StudentsDto GetStudentById(int studentId)
        {
            return JsonConvert.DeserializeObject<StudentsDto>(_httpService.GetItem($"{getStudentByIdUrl}{studentId}"));
        }

        public IList<StudentsDto> GetAllStudents()
        {
            return JsonConvert.DeserializeObject<List<StudentsDto>>(_httpService.GetList(getAllStudentsUrl));
        }
    }
}
