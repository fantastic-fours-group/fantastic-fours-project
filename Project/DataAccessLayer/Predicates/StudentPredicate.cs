using System.Linq;
using Entities.ModelsDB;

namespace DataAccessLayer.Predicates
{
    internal static class StudentPredicates
    {
        public static Students WithId(this IQueryable<Students> students, int id)
        {
            return students.First(s => s.Id == id);
        }

        public static IQueryable<Students> WithFirstName(this IQueryable<Students> students, string firstName)
        {
            return students.Where(s => s.FirstName.Contains(firstName));
        }

        public static IQueryable<Students> WithSecondName(this IQueryable<Students> students, string lastName)
        {
            return students.Where(s => s.SecondName.Contains(lastName));
        }

        // TODO: Decide the correct naming for this method.
        public static Students WithUserId(this IQueryable<Students> students, int userId)
        {
            return students.First(s => s.IdUser == userId);
        }

        // TODO: Decide the correct naming for this method.
        public static IQueryable<Students> WithGroupId(this IQueryable<Students> students, int groupid)
        {
            return students.Where(s => s.IdGroup == groupid);
        }
    }
}