﻿using System;
using System.Net;
using System.Web.Http;
using BusinessLogicLayer.Services;

namespace WebApi.Controllers
{
    public class StudentSearchController : ApiController
    {
        private readonly StudentService _studentService;

        public StudentSearchController()
        {
            _studentService = new StudentService();
        }

        // GET: api/StudentSearch/GetByGroupId?groupId=1
        [HttpGet]
        public IHttpActionResult GetByGroupId(int groupId)
        {
            try
            {
                var students = _studentService.GetByGroupId(groupId);
                return Ok(students);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // GET: api/StudentSearch/SearchStudent?firstName=Alexey&lastName=Mitzkan&group=401
        [HttpGet]
        public IHttpActionResult SearchStudent(string firstName, string lastName, string group)
        {
            try
            {
                var students = _studentService.SearchStudent(firstName, lastName, @group);
                return Ok(students);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }

        // GET: api/StudentSearch/GetStudentById?studentId=1
        [HttpGet]
        public IHttpActionResult GetStudentById(int studentId)
        {
            try
            {
                var students = _studentService.GetStudentById(studentId);
                return Ok(students);
            }
            catch (ArgumentException ex)
            {
                return Content(HttpStatusCode.BadRequest, ex);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
