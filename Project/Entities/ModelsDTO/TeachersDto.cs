﻿using System.ComponentModel.DataAnnotations;
using Resourses.Enums;

namespace Entities.ModelsDTO
{
    public class TeachersDto
    {
        [Required(ErrorMessage = "Id is a required field for entity Teacher")]
        public int Id { get; set; }

        [Required(ErrorMessage = "IdUser is a required field for entity Teacher")]
        public int IdUser { get; set; }

        [Required(ErrorMessage = "IdDepartment is a required field for entity Teacher")]
        public int IdDepartment { get; set; }

        [Required(ErrorMessage = "FirstName is a required field for entity Teacher")]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "FirstName size must be between 1 and 15 characters for entity Teacher")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "SecondName is a required field for entity Teacher")]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "SecondName size must be between 1 and 30 characters for entity Teacher")]
        public string SecondName { get; set; }

        [Required(ErrorMessage = "Position is a required field for entity Teacher")]
        public ScheduleEnums.TeacherPosition Position { get; set; }
    }
}
