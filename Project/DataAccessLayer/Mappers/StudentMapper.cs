﻿using Entities.ModelsDB;
using Entities.ModelsDTO;

namespace DataAccessLayer.Mappers
{
    public class StudentMapper
    {
        public StudentsDto MapFrom(Students students)
        {
            return new StudentsDto
            {
                Id = students.Id,
                FirstName = students.FirstName,
                SecondName = students.SecondName,
                IdGroup = students.IdGroup,
                IdUser = students.IdUser
            };
        }

        public Students MapTo(StudentsDto students)
        {
            return new Students
            {
                Id = students.Id,
                FirstName = students.FirstName,
                SecondName = students.SecondName,
                IdGroup = students.IdGroup,
                IdUser = students.IdUser
            };
        }
    }
}
