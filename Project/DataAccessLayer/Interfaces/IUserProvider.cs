using System.Collections.Generic;
using Entities.ModelsDTO;

namespace DataAccessLayer.Interfaces
{
    public interface IUserProvider
    {
        UsersDto GetByUserEmail(string userEmail);
        IList<UsersDto> GetByUserLogin(string userLogin);
        IList<UsersDto> GetByUserPassword(string userPassword);
        IList<UsersDto> GetByUserRole(string userRole);
        UsersDto GetUserById(int idUser);
    }
}