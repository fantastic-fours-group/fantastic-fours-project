using System.Collections.Generic;
using Entities.ModelsDTO;

namespace DataAccessLayer.Interfaces
{
    public interface ISubjectProvider
    {
        IList<SubjectsDto> GetSubjectByName(string name);
	    SubjectsDto GetSubjectById(int idSubject);
    }
}