namespace Entities.ModelsDB
{
    using System;
    using System.Collections.Generic;
    
    public partial class Transactions
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Transactions()
        {
            this.Schedules = new HashSet<Schedules>();
        }
    
        public int Id { get; set; }
        public int IdTeacher { get; set; }
        public int IdSubject { get; set; }
        public int IdAudience { get; set; }
        public string TypeOfLesson { get; set; }
        public string NumberOfWeek { get; set; }
    
        public virtual Audiences Audiences { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Schedules> Schedules { get; set; }
        public virtual Subjects Subjects { get; set; }
        public virtual Teachers Teachers { get; set; }
    }
}
